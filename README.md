# README #

Punti generali della versione 2.0.0 del Webappengine

### Come eseguire il debugin sulle Variabili nei controller.###

** Top::print_d è un funzione pensata per debbugare e per rendere visibile il risultato, si deve aggiungere al link di interesse “?debug=1”

* Top::print_d($variabile);     	In questo caso accoderemo la variabile senza interrompere il Flusso.
* Top::print_d($variabile, true)	In questo caso accoderemo e imporremo l’interruzione del Flusso. 

** Solitamente per avere un mapping pulito delle variabili accodate, consigliamo di accodare e interrompere solo l’ultima variabile del flusso che volete controllare, cosicché possiate fare i test necessari.  

### Come eseguire un login forzato su un utenza da Email o ID ###

* Per effetuare questo tipo di azione usiamo il seguete Rest
* debug/login?id={ID}
* debug/login?email={EMAIL}

### Webitart.Notify ###


##Schema##
** Notify
** [
**     0=>[
**         title: string,
**         icon: url_icon,
**         message: string,
**         system: boolean, 'true: usa il sistema di notifiche di interno del Browser'
**     ],
**     1=>[
**         title: string,
**         icon: url_icon,
**         message: string,
**         system: boolean,
**     ],
**     2=>[
**         title: string,
**         icon: url_icon,
**         message: string,
**         system: boolean,
**     ]
** ]

##USARE MODEL NEL BOOSTRAP##

$this->bootstrap(array('db', 'modules'));

###Query per creare un oggetto Base###

CREATE TABLE `tubooster`.`new_table` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `resource_id` INT NULL,
  `resource_name` VARCHAR(20) NULL,
  `user_id` INT NULL,
  `create_date` DATETIME NULL,
  `modified_date` DATETIME NULL,
  PRIMARY KEY (`id`));

### A che cosa serve? ###

* Migliorare lo sviluppo
* Gestione di websites Multilingua

### Punti forti? ###

* MVC
* Object-oriented

### Contributori? ###

* Sanchez Jean Paul