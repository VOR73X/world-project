// Gruntfile.js
module.exports = function (grunt) {
    var gruntReact = require('grunt-react');
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify: {
            options: {
                transform: [gruntReact.browserify],
            },
            default: {
                src: ['application/modules/default/jsx/app.jsx'],
                dest: 'lib/custom/themes/default/react/react-app.js',
            },
            backoffice: {
                src: ['application/modules/backoffice/jsx/app.jsx'],
                dest: 'lib/custom/themes/backoffice/react/react-app.js',
            },
        },
        uglify: {
            my_target: {
                files: {
                    'lib/custom/themes/default/react/react-app.min.js': ['lib/custom/themes/default/react/react-app.js']
                }
            },
            backoffice: {
                files: {
                    'lib/custom/themes/backoffice/react/react-app.min.js': ['lib/custom/themes/backoffice/react/react-app.js']
                }
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    sourceMap: false,
                    loadPath: ['lib/dependencies/foundation-sites/scss']
                },
                files: {
                    'lib/custom/themes/default/css/foundation.css': 'application/modules/default/scss/foundation-init.scss',
                    'lib/custom/themes/default/css/style.css': 'application/modules/default/scss/theme-init.scss',
                    'lib/custom/themes/backoffice/css/foundation.css': 'application/modules/backoffice/scss/foundation-init.scss',
                    'lib/custom/themes/backoffice/css/style.css': 'application/modules/backoffice/scss/theme-init.scss',
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'lib/custom/themes/default/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'lib/custom/themes/default/css/',
                    ext: '.min.css'
                },{
                    expand: true,
                    cwd: 'lib/custom/themes/backoffice/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'lib/custom/themes/backoffice/css/',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            options: {
                livereload: true,
                dateFormat: function(time) {
                    grunt.log.writeln('\n\n');
                    grunt.log.writeln('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*');
                    grunt.log.writeln('Waiting for more changes...');
                }
            },
            gruntfile:{
                files: 'gruntfile.js'
            },
            src: {
                files: ['application/modules/default/jsx/**/*.jsx','application/modules/backoffice/jsx/**/*.jsx'],
                tasks: ['browserify','uglify'],
            },
            sass: {
                files: [
                    'application/modules/default/scss/**/*.scss',
                    'application/modules/backoffice/scss/**/*.scss',
                ],
                tasks: ['sass','cssmin'],
            }

        },
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', [
        'watch'
    ]);
};