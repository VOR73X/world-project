(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Home = require('./pages/home/home.jsx');
Editor = require('./pages/editor/editor.jsx');

$(document).ready(function () {
	$('.react-home').each(function(i){
		var elemento = this;
		var params = $(this).data('params') || {};
		if(elemento){
			ReactDOM.render(
				React.createElement(Home, {params: params}), elemento
			)
		}
	})

	$('.react-editor').each(function(i){
		var elemento = this;
		var params = $(this).data('params') || {};
		if(elemento){
			ReactDOM.render(
				React.createElement(Editor, {params: params}), elemento
			)
		}
	})

})
},{"./pages/editor/editor.jsx":4,"./pages/home/home.jsx":8}],2:[function(require,module,exports){

Compass = React.createClass({displayName: "Compass",

	componentDidMount: function(){
		var self = this;
		if(window.DeviceOrientationEvent){
			window.addEventListener('deviceorientation', function(event) {
			  self.setState({
			  	alpha: event.alpha
			  })
			}, false);
		} else {
			console.log('Browser not support DeviceOrientationEvent')
		}
	},
	getInitialState: function(){
		return {
			alpha: 0
		}
	},
	render: function(){
		var compass = this.state;
		return React.createElement("div", null, 
			React.createElement("b", null, "Compass:"), " ", getDirectionString(compass.alpha), " | ", compass.alpha
		)
	}
})

module.exports = Compass;


},{}],3:[function(require,module,exports){
Gps = React.createClass({displayName: "Gps",
	gpsId: null,
	componentDidMount: function(){
		options = {
		  enableHighAccuracy: true,
		  timeout: 500,
		  maximumAge: 1000
		};
		this.gpsId = setInterval(function(){
			this.updateGPS();
		}.bind(this), 1000)
	},
	componentWillUnmount: function(){
		clearInterval(this.gpsId);
	},
	getInitialState: function(){
		return {
			latitude: 0,
			longitude: 0,	
			altitude: 0,
			accuracy: 0,
			altitudeAccuracy: 0,
			heading: 0,
			headingString: "",
			speed: 0,
			status: ""
		}
	},
	updateGPS: function(){
		this.setState({
			latitude: World.gps.latitude,
			longitude: World.gps.longitude,
			altitude: World.gps.altitude,
			accuracy: World.gps.accuracy,
			altitudeAccuracy: World.gps.altitudeAccuracy,
			heading: World.gps.heading,
			headingString: World.gps.headingString,
			speed: World.gps.speed,
			status: World.gps.status,
			timestamp: World.gps.timestamp
		});
	},
	render: function(){
		var gps = this.state;
		return React.createElement("div", null, 
			React.createElement("div", null, "latitude: ", gps.latitude), 
			React.createElement("div", null, "longitude: ", gps.longitude), 
			React.createElement("div", null, "altitude: ", gps.altitude), 
			React.createElement("div", null, "accuracy: ", gps.accuracy), 
			React.createElement("div", null, "altitudeAccuracy: ", gps.altitudeAccuracy), 
			React.createElement("div", null, "heading: ", gps.heading), 
			React.createElement("div", null, "headingString: ", gps.headingString), 
			React.createElement("div", null, "speed: ", gps.speed), 
			React.createElement("div", null, "status: ", gps.status), 
			React.createElement("div", null, "timestamp: ", gps.timestamp)
		)
	}
})

module.exports = Gps;
},{}],4:[function(require,module,exports){
Event = require('./event/event.jsx');
Script = require('./script/script.jsx');


Editor = React.createClass({displayName: "Editor",
	getDefaultProps: function(){
		console.log('INIT: getDefaultProps');
		return {}
	},
	getInitialState: function(){
		console.log('MOUNTING: getInitialState');
		return {
			show : false
		}
	},
	componentWillMount: function(){
		console.log('MOUNTING: componentWillMount');
	},
	render: function(){
		console.log('MOUNTING : render');
		return React.createElement("div", {className: "row"}, 
			React.createElement("div", {className: "column larg-16"}, 
				"BASE COMPONENT"
			)
		)
	},
	componentDidMount: function(){
		console.log('MOUNTING: componentDidMount');
	},
	componentWillReceiveProps: function(nextProps){
		console.log('UPDATE: componentWillReceiveProps');
	},
	shouldComponentUpdate: function(){
		console.log('UPDATE: shouldComponentUpdate');
		return true;
	},
	componentWillUpdate: function(nextProps,nextState){
		console.log('UPDATE: componentWillUpdate');
	},
	componentDidUpdate: function(prevProps,prevState){
		console.log('UPDATE: componentDidUpdate');
	},
	componentWillUnmount: function(){
		console.log('UNMOUNTING: componentWillUnmount');
	}
})

module.exports = Editor;


},{"./event/event.jsx":5,"./script/script.jsx":6}],5:[function(require,module,exports){
Event = React.createClass({displayName: "Event",
	getDefaultProps: function(){
		console.log('INIT: getDefaultProps');
		return {}
	},
	getInitialState: function(){
		console.log('MOUNTING: getInitialState');
		return {
			show : false
		}
	},
	componentWillMount: function(){
		console.log('MOUNTING: componentWillMount');
	},
	render: function(){
		console.log('MOUNTING : render');
		return React.createElement("div", {props: props}, 
			"BASE COMPONENT"
		)
	},
	componentDidMount: function(){
		console.log('MOUNTING: componentDidMount');
	},
	componentWillReceiveProps: function(nextProps){
		console.log('UPDATE: componentWillReceiveProps');
	},
	shouldComponentUpdate: function(){
		console.log('UPDATE: shouldComponentUpdate');
		return true;
	},
	componentWillUpdate: function(nextProps,nextState){
		console.log('UPDATE: componentWillUpdate');
	},
	componentDidUpdate: function(prevProps,prevState){
		console.log('UPDATE: componentDidUpdate');
	},
	componentWillUnmount: function(){
		console.log('UNMOUNTING: componentWillUnmount');
	}
})

module.exports = Event;


},{}],6:[function(require,module,exports){
Script = React.createClass({displayName: "Script",
	getDefaultProps: function(){
		console.log('INIT: getDefaultProps');
		return {}
	},
	getInitialState: function(){
		console.log('MOUNTING: getInitialState');
		return {
			show : false
		}
	},
	componentWillMount: function(){
		console.log('MOUNTING: componentWillMount');
	},
	render: function(){
		console.log('MOUNTING : render');
		return React.createElement("div", {props: props}, 
			"BASE COMPONENT"
		)
	},
	componentDidMount: function(){
		console.log('MOUNTING: componentDidMount');
	},
	componentWillReceiveProps: function(nextProps){
		console.log('UPDATE: componentWillReceiveProps');
	},
	shouldComponentUpdate: function(){
		console.log('UPDATE: shouldComponentUpdate');
		return true;
	},
	componentWillUpdate: function(nextProps,nextState){
		console.log('UPDATE: componentWillUpdate');
	},
	componentDidUpdate: function(prevProps,prevState){
		console.log('UPDATE: componentDidUpdate');
	},
	componentWillUnmount: function(){
		console.log('UNMOUNTING: componentWillUnmount');
	}
})

module.exports = Script;


},{}],7:[function(require,module,exports){
Events = React.createClass({displayName: "Events",
	componentWillMount: function(){

	},
	render: function(){
		var tplEvents = [];
		
		for(var x in this.props.data.events){
			var event = this.props.data.events[x];
			tplEvents.push(React.createElement("div", {key: x, className: "callout"}, 
				event.name, " - ", event.directionString, " - ", event.distance
			));
		}

		return React.createElement("div", null, 
			tplEvents
		)
	}
})

module.exports = Events;
},{}],8:[function(require,module,exports){
Gps= require('./../../components/gps/gps.jsx');
Compass = require('./../../components/compass/compass.jsx');
Events = require("./events.jsx");

Home = React.createClass({displayName: "Home",
	getInitalState:function () {
		return{
			data: {}
		}
	},
	onCheckStatus: function(){
		var formData = new FormData();
		for(var a in World.gps){
			formData.append(a,World.gps[a]);
		}

		$.ajax({
		    type: "post",
		    url: "api/scan",
		    data: formData,
		    cache: false,
		    contentType: false,
		    processData: false,
		    async: true,
		    success: function (responde) {	        
		        console.log(responde);
		        this.setState(function () {
		        	return{
		        		data:responde
		        	}
		        });

		        if(responde.events){
		        	for(var i in responde.events){
		        		var element = responde.events[i];

		        		if(element.responde){
		        			if(element.responde.messages){
		        				for(var x in element.responde.messages){
		        					var message = element.responde.messages[x];
		        					alert(message);
		        				}
		        			}
		        		}

		        	}
	        		console.log(responde.env);
		        }
		    }.bind(this)
		});
	},
	render: function(){
		var tplEvents = React.createElement("div", null);
		var state = this.state || {};

			console.log(this.state);
		if(state.data){
			tplEvents = React.createElement(Events, {data: state.data})
		}
		return React.createElement("div", null, 
			React.createElement(Compass, null), 
			React.createElement(Gps, null), 
			tplEvents, 
			React.createElement("button", {className: "button", onClick: this.onCheckStatus}, "Check")

		)
	}
})

module.exports = Home;

},{"./../../components/compass/compass.jsx":2,"./../../components/gps/gps.jsx":3,"./events.jsx":7}]},{},[1]);
