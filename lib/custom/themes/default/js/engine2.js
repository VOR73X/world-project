var coords = {
	"latitude": 0,
	"longitude": 0,
	"accuracy": 0,
	"altitude": 0,
	"altitudeAccuracy": 0,
	"heading":0,
	"speed": 0
}; 

navigator.geolocation.watchPosition(refreshInput);

function refreshInput(){
	navigator.geolocation.getCurrentPosition(function(data){
		coords = data.coords;
	});
}


function sendRequest(){
	var domainName = "";
	var playload = coords;

	var formData = new FormData();
	for(var a in playload){
		formData.append(a,playload[a]);
	}

	$.ajax({
	    type: "post",
	    url: "api/scan",
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false,
	    async: true,
	    success: function (data) {
	        //responde = JSON.stringify(responde);
	    	if(data.events){
	    		for(var i in data.events){
	    			if(data.events[i].responde){
	    				for(var k in data.events[i].responde.messages){
	    					alert(data.events[i].responde.messages[k]);
	    				}
	    			}
	    		}
	    	}    
	    }
	});
}