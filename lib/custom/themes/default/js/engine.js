var World = {};
World.gps = {};

function getDirectionString(bearing){
	   tmp = Math.round(bearing / 22.5);
	   switch(tmp) {
	      case 1:
	         direction = "NNE";
	         break;
	      case 2:
	         direction = "NE";
	         break;
	      case 3:
	         direction = "ENE";
	         break;
	      case 4:
	         direction = "E";
	         break;
	      case 5:
	         direction = "ESE";
	         break;
	      case 6:
	         direction = "SE";
	         break;
	      case 7:
	         direction = "SSE";
	         break;
	      case 8:
	         direction = "S";
	         break;
	      case 9:
	         direction = "SSW";
	         break;
	      case 10:
	         direction = "SW";
	         break;
	      case 11:
	         direction = "WSW";
	         break;
	      case 12:
	         direction = "W";
	         break;
	      case 13:
	         direction = "WNW";
	         break;
	      case 14:
	         direction = "NW";
	         break;
	      case 15:
	         direction = "NNW";
	         break;
	      default:
	         direction = "N";
	   }
	   return direction;
}

/**
 * Gps Translate convert gps Html5 format to simple format
 * @param  {Geolocation object} position Object GPS
 * @return {Simple GPS}          Simple Gps Translate
 */
function gpsTranslate(position){
	var accuracy = 20;
	var gps = {
		latitude: position.coords.latitude,
		longitude: position.coords.longitude,
		altitude: position.coords.altitude,
		accuracy: position.coords.accuracy,
		altitudeAccuracy: position.coords.altitudeAccuracy,
		heading: position.coords.heading,
		speed: position.coords.speed,
		timestamp: position.timestamp,
		status: "Offline"
	}

	gps.latitude = gps.latitude || 0;
	gps.longitude = gps.longitude || 0;
	gps.altitude = gps.altitude || -9999;
	gps.accuracy = gps.accuracy || 9999;
	gps.altitudeAccuracy = gps.altitudeAccuracy || 0;
	gps.heading = gps.heading || 0;
	gps.headingString = getDirectionString(gps.heading) || "";
	gps.speed = gps.speed || 0;
	gps.timestamp = gps.timestamp || 0;
	gps.change = false;

	if(gps.accuracy<=accuracy){
		gps.status = "Online";
	}

	gps.latitude = gps.latitude.toFixed(6);
	gps.longitude = gps.longitude.toFixed(6);
	gps.altitude = gps.altitude.toFixed(0);
	gps.heading = gps.heading.toFixed(0);
	gps.speed = gps.speed.toFixed(0);
	gps.speedString = gps.speed + " m/s";

	return gps;
}

/**
 * Watch GPS
 * @param  {Geolocation Object} position Objet GPS Html5
 */
function watchGps(position){
	var gps = gpsTranslate(position);
	World.gps = gps;
}

options = {
  enableHighAccuracy: true,
  timeout: 500,
  maximumAge: 1000
};

var gpsFunction = navigator.geolocation.watchPosition(watchGps, null, options);

