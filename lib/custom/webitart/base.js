//WEBITART-CONFIG
(function () {
    var Webitart = {
        init: $wbt_init,
        notify: $wbt_notify,
        submit: $wbt_submit,
        log: $wbt_log,
        top: new $wbt_top,
        upload: $wbt_upload,
        callback: new $wbt_callback,
        cache: new $wbt_cache,
        rest: $wbt_rest,
        popup: $wbt_popup
    }

    window.Webitart = Webitart;
})();

Webitart.init();
$(document).ready(function () {
Webitart.notify(Wbt.notify);
})
Webitart.submit();  
Webitart.upload();

Webitart.log("Webitart Init:",Wbt);
