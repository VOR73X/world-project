<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Top
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Top {

    public static $token;
    public static $profile;
    public static $acl;
    public static $smarty;
    public static $jsEngine;
    public static $executor;
    public static $notifyOffline = [];
    public static $system;
    public static $world;
    public static $multiplayer;
    public static $debug= [];
    public static $debugStop= false;
    public static $debugTime= 0;

    public static function reatriveFile(&$ref){
        $elemento = self::getModel('file')->load($ref);
        if($elemento){
            $ref = $elemento->getData();
        }
    }

    public static function setRoleString() {
        switch (Top::$profile->session['role']) {
            case 1:
                Top::$profile->session['roleString'] = "guest";
                break;
            case 2:
                Top::$profile->session['roleString'] = "user";
                break;
            case 3:
                Top::$profile->session['roleString'] = "partner";
                break;
            case 4:
                Top::$profile->session['roleString'] = "moderator";
                break;
            case 5:
                Top::$profile->session['roleString'] = "admin";
                break;
        }
    }

    public static function getUrl($url){
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $isCache = $config->website->cache->rest;
        $writabledir = Top::getAbsoluteUrl() . '/var/cache';
        $nameFile = hash("sha256", $url);
        $urlFile =$writabledir . "/" . $nameFile;

        $cacheManager =  Zend_Registry::get('cacheGeneric');
        $cache = $cacheManager->getCache('generic');

        $cacheID = 'json_' . $nameFile;
        if (false === ($data = $cache->load($cacheID)) && $isCache) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            $data = curl_exec($ch);
            curl_close($ch);
            $cache->save($data, $cacheID);
        }

        return $data;
    }

    public static function getUrlObject($url){
        return json_decode(self::getUrlJson($url),true);
    }

    public static function getModel($_model) {
        $_className = self::_getClassName($_model);
        return new $_className();
    }

    private static function _getClassName($_model) {
        $_modelExploded = explode("/", $_model);
        if (count($_modelExploded) == 1) {
            $_className = "Default_Model_" . ucfirst($_modelExploded[0]);
        } elseif (count($_modelExploded) == 2) {
            $_hasModule = explode("_", $_modelExploded[0]);
            if (count($_hasModule) == 1) {
                $_className = "Default_Model_" . ucfirst($_modelExploded[0]) . "_" . ucfirst($_modelExploded[1]);
            } else {
                $_className = ucfirst($_hasModule[0]) . "_Model_" . ucfirst($_hasModule[1]) . "_" . ucfirst($_modelExploded[1]);
            }
        } elseif (count($_modelExploded) == 3) {
            $_hasModule = explode("_", $_modelExploded[0]);
            if (count($_hasModule) == 1) {
                $_className = "Default_Model_" . ucfirst($_modelExploded[0]) . "_" . ucfirst($_modelExploded[1]) . "_" . ucfirst($_modelExploded[2]);
            } else {
                $_className = ucfirst($_hasModule[0]) . "_Model_" . ucfirst($_hasModule[1]) . "_" . ucfirst($_modelExploded[1]) . "_" . ucfirst($_modelExploded[2]);
            }
        }
        return $_className;
    }

    public static function getBaseUrl() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        return $config->website->address . "/";
    }

    public static function getBaseThemeUrl($module) {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        if(!$module){
            $module="default";
        }
        return $config->website->address . "/lib/custom/themes/{$module}/";
    }


    public static function getAbsoluteUrl() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $subdir = $config->website->subdir;
        if(substr($_SERVER['DOCUMENT_ROOT'] , strlen($_SERVER['DOCUMENT_ROOT'] )-1,strlen($_SERVER['DOCUMENT_ROOT']))=="/"){
            return $_SERVER['DOCUMENT_ROOT'] . $subdir;
        }else{
            return $_SERVER['DOCUMENT_ROOT'] . "/" . $subdir;
        }
        
    }

    public static function getEnv()
    {
        return APPLICATION_ENV;
    }

    public static function getLangDebug() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        return $config->lang->debug . "/";
    }

    public static function getCollectionArray($value,$statistics=false,$refModels=[]) {
        $obj['tab'] = $value['tab'];
        $obj['paginator'] = $value['paginator'];
        foreach ($value['data'] as $index => $elemento) {
            $obj['data'][$index] = $elemento->getData();
            foreach ($refModels as $keyRef => $valueRef) {
                $ref = $elemento->getReferenceModel($valueRef);
                if($ref){
                    $obj['data'][$index][$valueRef]=$ref->getData();
                }
            }
            if($statistics){
                $obj['data'][$index]['statistics'] = $elemento->getStatistics();
            }
        }
        return $obj;
    }

    public static function sendMail($emails,$subject,$template,$data){
        $name_server = str_replace("www", "", $_SERVER['SERVER_NAME']);

        foreach ($data as $key=>$value) {
            self::$smarty->assign($key,$value);
        }

        $email = new Zend_Mail();
        $email->setFrom("noreply@{$name_server}");
        $email->setBodyHtml(self::$smarty->outputString($template));
        foreach ($emails as $email) {
            $email->addTo($email);
        }
       
        $email->setSubject($subject);
        $email->send();
    }

    public static function getKey($data,$key){
        $explode = explode(".", $key) ;
        $v=$data;
        foreach ($explode as $element) {
            $v = $v[$element];
        }
        if(!$v){
            return $key;
        }
        return $v;
    }

    public static function SetNotify($_resource_name, $_resource_id) {
        $my_id = Top::$profile->session['id'];
        $resource = Top::getModel($_resource_name)->load($_resource_id);

        $datecreate = date('Y-m-d H:i:s');

        $notify = Top::getModel("notify")->loadByAttribute([
            'resource_id' => $_resource_id,
            'resource_name' => $_resource_name,
            'user_id' => $resource->getUser_id(),
            'open' => "0"
        ]);

        if (!$notify) {
            $notify = Top::getModel("notify");
            $notify->setResource_id($_resource_id);
            $notify->setResource_name($_resource_name);
            $notify->setUser_id($resource->getUser_id());
            $notify->setCreate_date($datecreate);
            $notify->setModified_date($datecreate);
            $notify->setParams(json_encode([$my_id]));
            $notify->setOpen(0);
        } else {
            $tmp_data = json_decode($notify->getParams());

            if (!in_array($my_id, $tmp_data)) {
                array_push($tmp_data, $my_id);

                $notify->setModified_date($datecreate);
                $notify->setParams(json_encode($tmp_data));
            }
        }
        $notify->save();
        top::print_d($notify->getData(), true);
    }

    public static function SetNotifyOffline($message,$options){
        $obj['system'] = !$options['system'] ? false : True;
        $obj['icon'] = !$options['system'] ? "" : $options['system'];
        $obj['onclick'] = !$options['onclick'] ? Self::getBaseUrl() : $options['onclick'];
        $obj['onsound'] = !$options['onsound'] ? false : true;
        $obj['type'] = !$options['type'] ? 0 : $options['type'];

        if (trim($message)==""){
            return false;
        }

        $obj['message'] = $message;

        array_push(Self::$notifyOffline, $obj);
        
    }

    public static function SetNotifySimple($message,$options){

        if(!is_array(Self::$jsEngine->object['notify'])){
            Self::$jsEngine->object['notify'] = [];
        }

        $obj['system'] = !$options['system'] ? false : True;
        $obj['icon'] = !$options['system'] ? "" : $options['system'];
        $obj['onclick'] = !$options['onclick'] ? Self::getBaseUrl() : $options['onclick'];
        $obj['onsound'] = !$options['onsound'] ? false : true;
        $obj['type'] = !$options['type'] ? 0 : $options['type'];


        if (trim($message)==""){
            return false;
        }

        $obj['message'] = $message;
        array_push(Self::$jsEngine->object['notify'], $obj);
    }

    public static function returnJson($obj){
        header("Content-Type: application/json; charset='utf-8'");
        $obj['notify'] = Self::$notifyOffline;  
        echo json_encode($obj,true);
    }

    public static function clearCache(){
        $files = glob(self::getAbsoluteUrl() . '/var/db/*');
        foreach($files as $file){ 
          if(is_file($file))
            unlink($file);
        }

        $files = glob(self::getAbsoluteUrl() . '/var/media/*');
        foreach($files as $file){ 
          if(is_file($file))
            unlink($file);
        }

        $files = glob(self::getAbsoluteUrl() . '/var/media/*');
        foreach($files as $file){ 
          if(is_file($file))
            unlink($file);
        }
    }

    public static function print_d($elemento, $die = false, $clear = false) {
        if(!$GLOBALS['timexe']){
            $GLOBALS['timexe'] = microtime(true);
            $GLOBALS['timecheck'] = 0;
        }

        $actual = microtime(true) - $GLOBALS['timexe'];
        self::$debugTime = $actual;
        if(!self::$debugStop){        
            if($clear){
                ob_end_clean();
            }

            $GLOBALS['timecheck'] = $actual - $GLOBALS['timecheck'] ;

            $check = $GLOBALS['timecheck'];
            $GLOBALS['timecheck'] = $actual;

            $objectDebug['object'] = $elemento;
            $objectDebug['level']="0";

            if($check>5 && $check<=10){
                $objectDebug['level']="1";
            }elseif($check>10){
                $objectDebug['level']="2";
            }

            $objectDebug['work']=$check;
            $objectDebug['workGeneral']=$actual;

            self::$debug[] = $objectDebug;
            if($_GET['debug']){
                print_r($objectDebug);
            }

            if ($die) {
                die();
            }
        }
    }

}
