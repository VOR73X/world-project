<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Google Helper
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class GoogleHelper {

    protected $query;
    protected $limit;
    protected $data;
    protected $client;

    public function __construct(){
    	$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);

    	$OAUTH2_CLIENT_ID = $config->google->appId;
    	$OAUTH2_CLIENT_SECRET = $config->google->appSecret;

    	$this->client = new Google_Client();
    	$this->client->setClientId($OAUTH2_CLIENT_ID);
    	$this->client->setClientSecret($OAUTH2_CLIENT_SECRET);
    }

    public function getInstance(){
        return $this->client;
    }

    public function getUrlLogin($callback=null,$permissions=[]){
    	//['https://www.googleapis.com/auth/youtube','https://www.googleapis.com/auth/youtube.force-ssl','https://www.googleapis.com/auth/youtube.readonly','https://www.googleapis.com/auth/youtubepartner']
    	if($callback){
			$redirect = filter_var($callback ,FILTER_SANITIZE_URL);
    	}else{
    		$redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'],FILTER_SANITIZE_URL);
    	}

    	$_SESSION['google_redirect'] = $redirect;

        if(!$permissions){
            $permissions = ['https://www.googleapis.com/auth/youtube']; // Optional permissions
        }
    	
    	$this->client->setRedirectUri($redirect);
        $this->client->setScopes($permissions);
        return $this->client->createAuthUrl();
    }

    public function callbackPoint(){
        if (isset($_GET['code'])) {
            if (strval($_SESSION['state']) !== strval($_GET['state'])) {
                die('The session state did not match.');
            }

            try {
				$redirect = $_SESSION['google_redirect'];

				$this->client->setRedirectUri($redirect);
                $this->client->authenticate($_GET['code']);
                
                $_SESSION['Google_token'] = $this->client->getAccessToken();
            }
            catch(Exception $e)
            {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
    }	

    public function authenticate(){
   		$this->client->setAccessToken($_SESSION['Google_token']);

        $oauth2 = new Google_Service_Oauth2($this->client);

     	$user = $oauth2->userinfo->get(); //get user info 
     	$obj = [];
     	foreach ($user as $key => $value) {
     		$obj[$key]=$value;
     	}

        return $obj;


    }

   	public function allListVideos(){
   		$this->client->setAccessToken($_SESSION['Google_token']);

   		$youtube = new Google_Service_YouTube($this->client);

   		$obj = [];

   		if ($this->client->getAccessToken()) {
   		    try {
   		        $channelsResponse = $youtube->channels->listChannels('contentDetails', array(
   		            'mine' => 'true',
   		        ));

   		        foreach ($channelsResponse['items'] as $channel) {
   		            $uploadsListId = $channel['contentDetails']['relatedPlaylists']['uploads'];
   		            $i = 0;

   		            do{
	   		            $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
	   		                'playlistId' => $uploadsListId,
	   		                'maxResults' => 50,
	   		                'pageToken' => $pageToken
	   		            ));

	   		            $pageToken = $playlistItemsResponse['nextPageToken'];

	   		            foreach ($playlistItemsResponse['items'] as $playlistItem) {
	   		            	$objChild['id'] = $playlistItem['snippet']['resourceId']['videoId'];
	   		            	foreach ($playlistItem['snippet'] as $key => $value) {
	   		                	$objChild[$key] = $value;   		                
	   		            	}
	   		            	$obj[] = $objChild;
	   		            }
	   		            $i++;
   		            }  while($pageToken);   		            
   		        }

   		    } catch (Google_Service_Exception $e) {

   		    } catch (Google_Exception $e) {

   		    }
   		}

   		return $obj;
   }
}
