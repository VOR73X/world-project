<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Field
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Base {

	public function __construct($param){
		$this->initPreset($param);
	}

    public function __call($methodName, $params = null) {
        $methodPrefix = substr($methodName, 0, 3);
        $key = strtolower(substr($methodName, 3));
        $_isUppercase = ctype_upper(substr($methodName, 3, 1));
        if ($methodPrefix == 'set' && count($params) == 1 && $_isUppercase) {
            $this->{$key} = $params[0];
            return $this;
        } elseif ($methodPrefix == 'get') {
         	return $this->{$key};
        }
    }

	public function getData(){
		$obj = [];
		foreach ($this as $key => $value) {
			$obj[$key] = $value;
		}
		return $obj;
    }	

    public function initPreset($param){
		if($param['presetJson']){
			$param = json_decode($param['presetJson'],true);
		}

    	foreach ($param as $key => $value) {
			$chiave = "set" . ucfirst($key);
			$this->{$chiave}($value);
		}
    }
}
