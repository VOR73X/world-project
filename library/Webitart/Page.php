<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Page extends Webitart_Base{

    protected $rows = [];
    protected $title;
    protected $description;
    protected $model;
    protected $action;

    public function __construct(){

    }

    public function addRow($params){
        array_push($this->rows, new Webitart_Row($params));
    }

    public function getData(){
        $obj = [];
        $obj['title'] = $this->title;
        $obj['description'] = $this->description;
        $obj['rows'] = [];
        foreach ($this->rows as $key => $value) {
            $obj['rows'][$key] = $value->getData();
        }
        return $obj;
    }

    public function json(){
        header("Content-Type: application/json; charset='utf-8'");
        echo json_encode($this->getData());
    }


}

Class Webitart_Row extends Webitart_Base{

    protected $columns= [];

    public function __construct($params){
        foreach ($params as $key => $value) {
            $this->addColum($value);
        }
    }

    public function addColum($params){
        array_push($this->columns, new Webitart_Column($params));
    }

    public function getData(){
        $obj = [];
        foreach ($this->columns as $key => $value) {
            $obj[$key] = $value->getData();
        }
        return $obj;
    }


}


Class Webitart_Column extends Webitart_Base{

    protected $title;
    protected $description;
    protected $large;
    protected $list = [];

    public function __construct($params){
        $this->title = $params['title'];
        $this->description = $params['description'];
        $this->large = $params['large'];
        $this->list = $params['list'];
    }

    public function addColum($params){
        array_push($this->rows, new Webitart_Row($params));
    }

    public function getData(){
        $obj = [];
        foreach ($this as $key => $value) {
            $obj[$key] = $value;
        }
        return $obj;
    }


}

