
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Webitart_Abstract
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Abstract extends Zend_Db_Table_Abstract {

    protected $_primaryKey;
    protected static $_instance;
    protected $_tableName;
    protected $calledClass;
    protected $_attributes = array();
    protected $_referencedClass = array();
    protected $_domain = "";
    protected $_notify = false;
    protected $_fields = [];

    /**
     * @author Jean Paul Sanchez <j.sanchez@webitart.com>
     */
    public function __construct($_data = array()) {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        try {
            $this->_domain = $this->_primaryKey = ucfirst($this->retrievePrimaryKey());
            $this->_tableName = $this->retrieveTableName();
            $this->retrieveFields();
            $this->provideInformationsForRelatedTables();
            $this->_notify = $this->retrieveNotify();
            if ($config->website->domain) {
                $this->domain = $this->_tableName . "_";
            }
            $config = array('name' => $this->_tableName);
            $this->calledClass = get_called_class();
            parent::__construct($config);
            if (count($_data) != 0) {
                $this->_setData($_data);
            } else {
                $this->_setData(array_flip($this->_getCols()), true);
            }
            
        } catch (Exception $e) {
            
        }
        self::$_instance = $this;
        return $this;
    }

    public function __call($methodName, $params = null) {
        $methodPrefix = substr($methodName, 0, 3);
        $key = strtolower(substr($methodName, 3));
        $_isUppercase = ctype_upper(substr($methodName, 3, 1));
        if ($methodPrefix == 'set' && count($params) == 1 && $_isUppercase) {
            $key = $this->domain . $key;
            if (!in_array($key, $this->_getCols())) {
                return $this;
            } else {
                $value = $params[0];

                $this->_attributes[$key] = $value;
                return $this;
            }
        } elseif ($methodPrefix == 'get') {
            $key = $this->domain . $key;
            if (in_array($key, array_keys($this->_attributes))) {
                return $this->_attributes[$key];
            } else {
                return;
            }
        } elseif (!in_array($methodName, array_flip(get_class_methods($this)))) {
            throw new Exception("Method \"" . $methodName . "\" doesn't exist in " . get_called_class(), 500);
        }
    }

    private function _cleanNotNullableAttributes($_attributes) {
        $_metadata = $this->info("metadata");
        foreach ($_attributes as $_key => $_value) {
            if ($_metadata[$_key]['NULLABLE'] === false && $_value === NULL) {
                unset($_attributes[$_key]);
            }
        }
        return $_attributes;
    }

    public static function getInstance() {
        if (self::$_instance === NULL || self::$_instance->calledClass !== get_called_class()) {
            $_className = get_called_class();
            self::$_instance = new $_className();
        }
        return self::$_instance;
    }

    public function getAttributes() {
        return $this->_getCols();
    }

    public function getData() {
        $data = $this->_attributes;
        if($this->domain){
            foreach ($data as $key => $value) {
                $name = str_replace($this->domain, "", $key);
                $new[$name] = $value;
            }
            $data = $new;
        }
        return $data;
    }

    public function getPrimaryKey() {
        return $this->_primaryKey;
    }

    public function getReferenceModel($model = null ) {
        if ($this->_referencedClass) {
            $_colsToRef = array_keys($this->_referencedClass);
            if($model){
                $_tableSelect=$this->_referencedClass[$model][0];
                $_colSelect = $model;
            }else{
                $_tmp = array_keys($this->_referencedClass);
                $_tableSelect = $this->_referencedClass[$_tmp[0]][0];
                $_colSelect = $_tmp[0];
            }
            if($this->_referencedClass[$_colSelect]){
                $_tableRef = $this->_referencedClass[$_tableSelect];
                $_primaryKey = "get" . ucfirst($_colSelect);
                return Top::getModel($_tableSelect)->load($this->{$_primaryKey}());
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function getReferenceList(){
        $listRef = [];
        foreach ($this->_referencedClass as $key => $value) {
            $listRef[] = $key;
        }
        return $listRef;
    }

    public function isReference($ref){
        $listRef = $this->getReferenceList();
        return in_array($ref, $listRef);
    }

    public function setDataToJson($json) {
        $elementi = json_decode($json);
        if (is_array($elementi)) {
            foreach ($elementi as $key => $value) {
                $_primaryKey = "set" . ucfirst($key);

                $this->{$_primaryKey}($value);
            }
        }
    }

    public function setDataToArray($elementi) {
        if (is_array($elementi)) {
            foreach ($elementi as $key => $value) {
                $_primaryKey = "set" . ucfirst($key);
                $this->{$_primaryKey}($value);
            }
        }
    }

    public function setData($_data) {
        if (count($_data) != 0) {
            $this->_setData($_data);
        } else {
            return false;
        }
        return $this;
    }

    protected function _setData($_data, $_init = false) {
        foreach ($_data as $_key => $_value) {
            $this->_attributes[$_key] = ($_init) ? NULL : $_value;
        }
    }

    public function countRows() {
        $_count = $this->getAdapter()->fetchOne('SELECT COUNT(*) AS count FROM ' . $this->_tableName);
        return $_count;
    }

    protected function provideInformationsForRelatedTables() {
        $this->_referencedClass = $this->retrieveTableReference();
    }

    private function parseDocComment($str, $tag = '') {
        if (empty($tag)) {
            return $str;
        }
        $matches = array();
        preg_match("/" . $tag . ":(.*)(\\r\\n|\\r|\\n)/U", $str, $matches);
        if (isset($matches[1])) {
            return trim($matches[1]);
        }

        return '';
    }

    private function retrievePrimaryKey() {
        $annotations = new ReflectionClass($this);
        return $this->parseDocComment($annotations->getDocComment(), "@PrimaryKey");
    }

    private function retrieveTableReference() {
        $annotations = new ReflectionClass($this);
        return json_decode($this->parseDocComment($annotations->getDocComment(), "@ClassReferenced"),true);
    }

    private function retrieveTableName() {
        $annotations = new ReflectionClass($this);
        return $this->parseDocComment($annotations->getDocComment(), "@Table");
    }

    private function retrieveNotify() {
        $annotations = new ReflectionClass($this);
        if($this->parseDocComment($annotations->getDocComment(), "@Notify")=='true'){
            return true;
        }elseif($this->parseDocComment($annotations->getDocComment(), "@Notify")=='false'){
            return false;
        }
        return false;
    }

    private function retrieveFields() {
        $annotations = new ReflectionClass($this);
        $re = "/@Field: (.*)(\\r\\n|\\r|\\n)/U"; 
        preg_match_all($re, $annotations->getDocComment(), $matches);

        foreach ($matches[1] as $key => $value) {
            $array = explode(" ", $value);
            $obj['name'] = $array[0];
            $obj['type'] = $array[1];
            $obj['default'] = $array[2];
            $this->_fields[] =$obj;
        }

        return $matches[1];
    }


    public function load($_id) {
        try {
            $cacheManager = Zend_Registry::get('cacheDatabase');
            $cache = $cacheManager->getCache('database');
            $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
            $cacheID =$this->_tableName . "_" . $_id;
            if (false === ($_objectData = $cache->load($cacheID))) {
                $_objectData = $this->select()->where(strtolower($this->_primaryKey) . " = ?", $_id);
                Top::print_d($_objectData->__toString());

                $_objectData = $_objectData->query()->fetch();

                if ($config->website->cache->db) {
                    $cache->save($_objectData, $cacheID);
                }
            }
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            } else {
                $object = get_called_class();
                return new $object($_objectData);
            }
        } catch (Exception $e) {
            $_logger = Webitart_Log::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage() . " (PK:: {$_id})");
            return false;
        }
    }

    public function getCollection($_filters = [], $options = []) {
        try {
            $_primaryKey = $this->domain . $this->getPrimaryKey();
            $_objectData = $this->select()->from($this->_tableName)->columns($_primaryKey);

            foreach ($_filters as $_attributeName => $_value) {
                if (is_array($_value)) {
                    $_matchingValue = $_value[1];
                    $_operand = $_value[0];
                    if ($_value[2]) {
                        if (strtolower($_value[2]) == "or") {
                            $_objectData->orWhere($this->domain . $_attributeName . " {$_operand} ?", $_matchingValue);
                        } else {
                            $_objectData->where($this->domain . $_attributeName . " {$_operand} ?", $_matchingValue);
                        }
                    } else {
                        $_objectData->where($this->domain . $_attributeName . " {$_operand} ?", $_matchingValue);
                    }
                } else {
                    $_objectData->where($_value);
                }
            }

            if ($options['order']) {
                $_objectData->order($options['order']);
            }
            if ($options['group']) {
                $_objectData->group($options['group']);
            }
            if ($options['limit']) {
                $_objectData->limitPage($options['limit'][0], $options['limit'][1]);
            }

            Top::print_d($_objectData->__toString());

            $adapter = new Zend_Paginator_Adapter_DbSelect($_objectData);
            $_objectData = new Zend_Paginator($adapter);

            if ($options['resultPage']) {
                $_objectData->setItemCountPerPage($options['resultPage']);
            }
            
            if ($options['page'] && $options['target'] == $this->_tableName) {
                $_objectData->setCurrentPageNumber($options['page']);
            }

            if (!$_objectData && count($_objectData) == 0) {

            } else {
                $_toReturn = array();
                $objectName = get_called_class();
                foreach ($_objectData as $_singleObject) {
                    $_idPk = $_singleObject[$_primaryKey];
                    $object = new $objectName();
                    $element = $object->loadByAttribute(strtolower($_primaryKey), $_idPk);
                    array_push($_toReturn, $element);
                }
                $final_obj['tab'] = $this->_tableName;
                $final_obj['paginator'] = (array) $_objectData->getPages();
                $final_obj['data'] = $_toReturn;

                return $final_obj;
            }
        } catch (Exception $e) {
            Top::print_d($e->getMessage());
            return false;
        }
    }

    public function loadByAttribute($_attributeName, $_value = NULL) {
        if (!in_array($_attributeName, $this->_getCols()) && !is_array($_attributeName)) {
            throw new Exception("Attribute \"" . $_attributeName . "\" is not part of object's attribute set", 500, NULL);
        }
        try {
            if (!is_array($_attributeName)) {
                $_attributes = array($_attributeName => $_value);
            } else {
                $_attributes = $_attributeName;
            }
            $_objectData = $this->select();
            foreach ($_attributes as $_key => $_value) {
                $_objectData->where($_key . " = ?", $_value);
            }
            Top::print_d($_objectData->__toString());

                $_objectDataIstance = $_objectData->query()->fetch();

            if (!$_objectDataIstance) {
                throw new Exception("Object not found", 501);
            } else {
                $this->_setData($_objectDataIstance);
                return $this;
            }
        } catch (Exception $e) {
            Top::print_d($e->getMessage());
            return false;
        }
    }

    public function getStatistics() {
        $view = Top::getModel("view");
        $obj['views'] = $view->getCount($this->_tableName, $this->getId());

        $likes = Top::getModel("likes");
        $obj['likes'] = $likes->getCount($this->_tableName, $this->getId());
        $obj['is_like'] = $likes->statusResource($this->_tableName, $this->getId());

        $dislikes = Top::getModel("dislikes");
        $obj['dislikes'] = $dislikes->getCount($this->_tableName, $this->getId());
        $obj['is_dislike'] = $dislikes->statusResource($this->_tableName, $this->getId());

        $comment = Top::getModel("comment");
        $obj['comments'] = $comment->getCount($this->_tableName, $this->getId());
        return $obj;
    }

    public function checkView() {
        $view = Top::getModel("view");
        $view->checkResource($this->_tableName, $this->getId());
    }

    public function save() {
        $_primaryKey = "get" . $this->_primaryKey;
        $_primaryKeySet = "set" . $this->_primaryKey;

        $now = date('Y-m-d H:i:s');
        if ($this->{$_primaryKey}() === NULL || $this->{$_primaryKey}() == 0) {
            if(!$this->getCreate_date()){
                $this->setCreate_date($now);
            }
        }
        if(!$this->getModified_date()){
            $this->setModified_date($now);
        }
        $_data = $this->_cleanNotNullableAttributes($this->_attributes);

        if ($this->{$_primaryKey}() === NULL || $this->{$_primaryKey}() == 0) {
            $this->{$_primaryKeySet}($this->insert($_data));
            if($this->_notify){
                $this->notify($this->getResource_name(),$this->getResource_id());
            }
        } else {
            $cacheManager = Zend_Registry::get('cacheDatabase');
            $cache = $cacheManager->getCache('database');
            $cacheID = $this->_tableName . "_" . $this->{$_primaryKey}();
            $cache->remove($cacheID);
            $this->update($_data, strtolower($this->_primaryKey) . " = " . $this->{$_primaryKey}());

        }
        $this->_setData($this->_attributes);
        return $this;
    }

    public function delete() {
        try {
            $_primaryKey = "get" . $this->_primaryKey;
            parent::delete(strtolower($this->_primaryKey) . " = " . $this->{$_primaryKey}());
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function setup(){
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $db = $this->_db;
        $dbName = $config->resources->db->params->dbname;
        $table = $this->_tableName;
        $fieldsTmp = $this->_fields;

        $create_date['type'] = "DATETIME";
        $create_date['name'] = "create_date";
        $fieldsTmp[] = $create_date;

        $modified_date['type'] = "DATETIME";
        $modified_date['name'] = "modified_date";
        $fieldsTmp[]  = $modified_date;
      
        try {
            foreach ($fieldsTmp as $key => $value) {
                $column = $value['name'];
                $default = $value['default'];
                if(!$default){
                    $default = "";
                }

                if(is_string($default)){
                    $default = "'" . $default . "'";
                }

                $type = $value['type'];

                //CHECK EXIST TABLE
                $sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ?  AND TABLE_NAME = ?";
                $checkExistTable = new Zend_Db_Statement_Mysqli($db, $sql);
                $checkExistTable->execute([$dbName,$table]);


                if($checkExistTable->rowCount()==0){
                    //NO EXIST TABLE
                    echo "CREATE TABLE $table" . "<hr>";
                    $sql ="CREATE TABLE $table (`id` INT NOT NULL AUTO_INCREMENT,`name` VARCHAR(45) NULL,`create_date` DATETIME NULL, `modified_date` DATETIME NULL,PRIMARY KEY (`id`));";
                    $createTable = new Zend_Db_Statement_Mysqli($db, $sql);
                    $createTable->execute();

                    $sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ?  AND TABLE_NAME = ?";
                    $checkExistTable = new Zend_Db_Statement_Mysqli($db, $sql);
                    $checkExistTable->execute([$dbName,$table]);
                }

                $fetchAll = $checkExistTable->fetchAll();
                $columns = [];
                foreach ($fetchAll as $element) {
                    $columns[] = $element['COLUMN_NAME'];
                }

                if($column!="id"){            
                    //CHECK EXIST COLUMN
                    $sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ?  AND TABLE_NAME = ? AND COLUMN_NAME = ? ";
                    $checkExistColumn = new Zend_Db_Statement_Mysqli($db, $sql);
                    $checkExistColumn->execute([$dbName,$table,$column]);

                    if(!$checkExistColumn->rowCount()){
                       //NO EXIST COLUMN
                        echo "ADD TO $table >> $column >> $type" . "<hr>";
                        $sql = "ALTER TABLE `$table` ADD `$column` $type";
                        $createColumn = new Zend_Db_Statement_Mysqli($db, $sql);
                        $createColumn->execute();
                        
                    }else{
                        //EXIST COLUMN
                        echo "MODIFY TO $table >> $column >> $type" . "<hr>";
                        $sql = "ALTER TABLE `$table` MODIFY `$column` $type";
                        $modifyColumn = new Zend_Db_Statement_Mysqli($db, $sql);
                        $modifyColumn->execute();
                    }
                }
            }

            $confColumns = [];
            foreach ($this->_fields as $key => $value) {
                $confColumns[] = $value['name'];
            }


            $confColumns[] = 'id';
            $confColumns[] = 'create_date';
            $confColumns[] = 'modified_date';

            foreach ($columns as $key => $value) {
                if(!in_array($value, $confColumns)){
                   echo "DELETE TO $table >> $value" . "<hr>";
                   $sql = "ALTER TABLE $table DROP COLUMN `$value`;";
                   $modifyColumn = new Zend_Db_Statement_Mysqli($db, $sql);
                   $modifyColumn->execute();
                }
            }
  
        } catch (Exception $e) {
            echo $e->getMessage() . "<hr>";
        }
    }

    public function notify($_resource_name, $_resource_id){
        $user_id = Top::$profile->session['id'];
        if($this->_notify && $user_id>0){
            $now = date('Y-m-d H:i:s');

            if(!$_resource_name || !$_resource_id){
                $_resource_id = $this->getId();
                $_resource_name = $this->_tableName;
                $resource = $this;
            }
            else{
                $resource = Top::getModel($_resource_name)->load($_resource_id);
            }

            $notify = Top::getModel("notify")->loadByAttribute([
                'resource_id'=>$_resource_id,
                'resource_name'=>$_resource_name,
                'user_id'=>$resource->getUser_id(),
                'type'=>$this->_tableName
            ]);

            $resource_user_id=$resource->getUser_id();

            if(!$notify){
                $notify = Top::getModel("notify");
                $notify->setResource_id($_resource_id);
                $notify->setResource_name($_resource_name);
                $notify->setUser_id($resource_user_id);
                $notify->setRead("0");
                $notify->setModified_date($now);
                $notify->setType($this->_tableName);
                $notify->save();

                $notify_element = Top::getModel("notifyElement")->loadByAttribute([
                    'user_id'=>$user_id,
                    'group_id'=>$notify->getId()
                ]);
                if(!$notify_element){
                    $notify_element = Top::getModel("notifyElement");
                    $notify_element->setUser_id($user_id);
                    $notify_element->setGroup_id($notify->getId());
                    $notify_element->save();
                }
            }  
        }
    }
}
