<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AclModel
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_AclModel {
    public $_model;
    public $_action;
    public $_prefix;
    
    public function __construct($model,$action,$prefix="") {
        $this->_model = $model;
        $this->_action = $action;
        $this->_prefix = $prefix;
    }
}
