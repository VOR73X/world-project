<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Executor
 *
 * @author Jean
 */
class Webitart_Executor {

    public $begin;
    public $count = 0;

    public function __construct() {
        $now = date('Y-m-d H:i:s');
        $executor = Top::getModel("executor")->getCollection(['close' => ['=', '0'], 'begin' => ["<=", $now], 'avaiable' => ['<=', $now]], ['order' => 'begin ASC', 'limit' => '1']);
        $start = $this->microtime_float();

        $this->begin = $this->microtime_float();

        foreach ($executor['data'] as $key => $value) {
            if (isset($executor['data'][$key])) {
                $value = $executor['data'][$key];
                if ($value->getRecurrence() > 0 || $value->getRecurrence()==-1) {
                    if(!$value->getRecursive()){

                        //Lancio JOB
                        $model = Top::getModel("job");
                        $jobName = $value->getAction()."Job";
                        $model->{$jobName}(json_decode($value->getParams(), true));

                        $begin = new DateTime($now);
                        $begin->add(new DateInterval('PT' . $value->getInterval() . "S"));
                        $value->setBegin($begin->format('Y-m-d H:i:s'));

                        $valore = $value->getRecurrence();
                        if ($value->getRecurrence() > 0) {
                            $value->setRecurrence($valore - 1);
                        }
                        if ($value->getRecurrence() == 0) {
                            $value->setClose(1);
                        }

                        $value->save();
                    }else{
                        while ( ($value->getRecurrence() > 0 && $value->getBegin() <= $now) ||  $value->getRecurrence()==-1 && $value->getBegin() <= $now) {
                            if ($this->microtime_float() - $start > 20 ) {
                                die("SYSTEM PENDING!!!");
                                break;
                            }

                            //Lancio JOB
                            $model = Top::getModel("job");
                            $jobName = $value->getAction()."Job";
                            $model->{$jobName}(json_decode($value->getParams(), true));
                            
                            $begin = new DateTime($value->getBegin());
                            $begin->add(new DateInterval('PT' . $value->getInterval() . "S"));
                            $value->setBegin($begin->format('Y-m-d H:i:s'));

                            $read = new DateTime();
                            $read->add(new DateInterval('PT20S'));
                            $value->setAvaiable($read->format('Y-m-d H:i:s'));

                            $valore = $value->getRecurrence();
                            if ($value->getRecurrence() > 0) {
                                $value->setRecurrence($valore - 1);
                            }
                            if ($value->getRecurrence() == 0) {
                                $value->setClose(1);
                            }

                            $value->save();
                        }
                    }

                } else {
                    $value->setClose(1);
                    $value->save();
                }
            } else {
                break;
            }
        }
    }

    function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);  
    }

}
