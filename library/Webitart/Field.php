<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Field
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Field extends Webitart_Base {

	protected $name = "";
	protected $type = "text";
	protected $regex = "";
	protected $min = 0;
	protected $max = 0;
	protected $help_message = "";
	protected $warning_message = "";
	protected $error_message = "";
	protected $label = "";
	protected $required = false;
	protected $readonly = false;
	protected $value = "";
	protected $style = false;
	protected $class = "";
	protected $preview = "";
	protected $presave = "";
	protected $template = "";
	protected $maxlength = 0;
	protected $placeholder = "";
	protected $step;
	protected $accept;
	protected $checked = false;
	protected $disabled = false;
	protected $pathTemplate = "system/webitart/form";
	protected $option = [];

	protected $ids = [];
	protected $classes = [];
	protected $datafields = [];
    protected $styles = [];


	protected $system = [];

	public function __construct($params){
		parent::__construct($params);
		$rand_id = "wbt" . rand(100000,999999);
		$this->system['id'] = $rand_id;
	}

    public function getHtml(){
    	Top::$smarty->assign("bean", $this->getData());
    	if($_GET['debug']){
    		Top::$smarty->assign("debug",1);
    	}

    	$template = $this->type;

    	if ($this->template){
			$template = $this->template;
    	}

    	$url_template = Top::getAbsoluteUrl() . "application/modules/default/views/templates/" . $this->pathTemplate . "/fields/" . $template . ".tpl";

    	if(!file_exists($url_template)){
    		$template = "generic";
    		$html = Top::$smarty->outputString($this->pathTemplate . "/fields/" . $template . ".tpl");
    		return $html;
    	}
    	$html = Top::$smarty->outputString($this->pathTemplate . "/fields/" . $template . ".tpl");
    	return $html;
    }

    public function addId($id){
    	array_push($this->ids, $id);
        return $this;
    }

    public function addClass($class){
    	array_push($this->classes, $class);
        return $this;
    }

    public function addStyle($style){
        array_push($this->styles, $style);
        return $this;
    }

    public function addData($name, $value){
       $this->datafields[$name]=$value;
       return $this;
    }

    public function addOption($name, $value){
       $this->option[$value]=$name;
       return $this;
    }

    public function getData(){
    	$obj = parent::getData();
    	$obj['ids'] = $this->ids;
    	$obj['id'] = $obj['system']['id'] . " ";

    	foreach ($this->ids as $key => $value) {
    		$obj['id'] .= $value . " ";
    	}

		$obj['datafield'] = "";
        foreach ($this->datafields as $key => $value) {
            $obj['datafield'] .= "data-" . $key . "=\"$value\" ";
        }

		$obj['class'] = "";
        foreach ($this->classes as $key => $value) {
            $obj['class'] .= $value . " ";
        }

        $obj['style'] = "";
        foreach ($this->styles as $key => $value) {
            $obj['style'] .= $value . ";";
        }   

		$obj['id'] = trim($obj['id']);
		$obj['class'] = trim($obj['class']);
    	$obj['system'] = $this->system;
    	return $obj;
    }

}
