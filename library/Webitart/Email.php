<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Field
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Email  {

    public function sendToSystem($subject,$template, $recipients, $params, $debug=false)
    {
        $name_server=str_replace("www.","",$_SERVER['SERVER_NAME']);
        Top::$smarty->assign('template',$template);
        Top::$smarty->assign('data',$params);

        $mail = new Zend_Mail();
        $mail->setFrom("noreply@{$name_server}");
        $mail->setBodyHtml(Top::$smarty->outputString("email/base.tpl"));

        if($debug){
            echo $subject."<hr>";
            foreach ($recipients as $key => $value) {
                echo "To: " . $value . "<hr>";
            }
            echo Top::$smarty->outputString("email/base.tpl");
            die();
        }

        foreach ($recipients as $key => $value) {
            $mail->addTo($value);
        }

        $mail->setSubject($subject);
        $mail->send();
    }
}
