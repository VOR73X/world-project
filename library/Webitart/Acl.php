<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Acl
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Acl extends Zend_Acl {

    function allow($roles = null, $resources = null, $privileges = null, \Zend_Acl_Assert_Interface $assert = null) {
        if (is_a($resources, "Webitart_AclModel")) {
            $prefix = $this->prefixCeck($resources);

            parent::allow($roles, "{$resources->_model}_{$prefix}{$resources->_action}", $privileges, $assert);
        } else {
            parent::allow($roles, $resources, $privileges, $assert);
        }
    }

    function deny($roles = null, $resources = null, $privileges = null, \Zend_Acl_Assert_Interface $assert = null) {
        if (is_a($resources, "Webitart_AclModel")) {
            $prefix = $this->prefixCeck($resources);

            parent::deny($roles, "{$resources->_model}_{$prefix}{$resources->_action}", $privileges, $assert);
        } else {
            parent::deny($roles, $resources, $privileges, $assert);
        }
    }

    function isAllowedMe( $resource = null, $privilege = null) {
        return parent::isAllowed(Top::$profile->session['roleString'], $resource, $privilege);
    }

    function isAllowedArrayMe($config, $flusso = []) {
        $prefix = $this->prefixCeck($config);
        $this->isAllowedArray(Top::$profile->session['roleString'], $config->_model, $prefix, $config->_action, $flusso);
    }

    function isAllowedKeys($role, $config, $flusso = []) {
        $prefix = $this->prefixCeck($config);
        $privilege = array_keys($flusso);
        foreach ($privilege as $key => $value) {
            if (!$this->isAllowed($role, "{$config->_model}_{$prefix}{$config->_action}", $value)) {
                return false;
            }
        }
        return true;
    }

    function isAllowedArray($role, $config, $privilege = []) {
        $prefix = $this->prefixCeck($config);
        foreach ($privilege as $key => $value) {
            if (!$this->isAllowed($role, "{$config->_model}_{$prefix}{$config->_action}", $value)) {
                return false;
            }
        }
        return true;
    }

    function isAllowedFiltreArray($role, $config, $header = []) {
        $prefix = $this->prefixCeck($config);
        foreach ($header as $key => $value) {
            if (!$this->isAllowed($role, "{$config->_model}_{$prefix}{$config->_action}", $value)) {
                unset($header[$key]);
            }
        }
        return $header;
    }

    function addResources($models, $prefix, $privilege = ['insert', 'update', 'update_all', 'read', 'read_all', 'delete']) {
        if ($prefix != "") {
            $prefix = $prefix . "_";
        }

        foreach ($models as $model) {
            foreach ($privilege as $key => $value) {
                $this->add(new Zend_Acl_Resource("{$model}_{$prefix}{$value}"));
            }
        }
    }

    function prefixCeck($config) {
        if ($config->_prefix != "") {
            return $config->_prefix . "_";
        } else {
            return $config->_prefix;
        }
    }

}
