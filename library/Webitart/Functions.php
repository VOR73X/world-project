<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of static functions
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_functions {

    function __construct() {
        
    }

    static function database_array_escape($array) {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $a[$key] = database_escape($value);
            }
            return $a;
        }
    }

    static function database_array_return_escape($array, $br = 0) {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = self::database_return_escape($value, $br);
            }
            return $array;
        }
    }

    static function database_escape($testo) {
        $testo = str_replace("\\x00", "[char1_235711]", $testo);
        $testo = str_replace("\n", "[char2_235711]", $testo);
        $testo = str_replace("\\r", "[char3_235711]", $testo);
        $testo = str_replace("\\x1a", "[char7_235711]", $testo);
        $testo = str_replace("\\", "[char4_235711]", $testo);
        $testo = str_replace("'", "[char5_235711]", $testo);
        $testo = str_replace("\"", "[char6_235711]", $testo);

        return $testo;
    }

    static function database_return_escape($testo, $br = 0) {
        if ($br == 1) {
            $testo = str_replace("[char2_235711]", "<br>", $testo);
            $testo = str_replace("\n", "<br>", $testo);
        } else {
            $testo = str_replace("[char2_235711]", "\n", $testo);
        }

        $testo = str_replace("[char1_235711]", "\\x00", $testo);
        $testo = str_replace("[char3_235711]", "\\r", $testo);
        $testo = str_replace("[char7_235711]", "\\x1a", $testo);
        $testo = str_replace("[char4_235711]", "\\", $testo);
        $testo = str_replace("[char5_235711]", "'", $testo);
        $testo = str_replace("[char6_235711]", "\"", $testo);

        return $testo;
    }

    static function mtime() {
        $calcolo_micro = explode(" ", microtime());
        return $calcolo_micro[1] + $calcolo_micro[0];
    }

    static function openFile($np) {
        $f = fopen($np, "r");
        $r = fread($f, filesize($np));
        fclose($f);
        return $r;
    }

    static function inviaMail($destinatario, $titolo_mail, $subtitolo_mail, $indirizzo_mail, $messaggio) {
        $mail_Da = "From: $titolo_mail <$indirizzo_mail>\nMIME-Version: 1.0\nContent-Type: text/html; charset=\"iso-8859-1\"\nContent-Transfer-Encoding: 7bit\n\n";
        $inviaMail = mail($destinatario, $subtitolo_mail, $messaggio, $mail_Da);
        return $inviaMail;
    }

    static function stringCalculate($equation) {
        $result = 0;
        $equation = preg_replace('/\s+/', '', $equation);
        $number = '((?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?|pi|π)';
        $functions = '(?:sinh?|cosh?|tanh?|acosh?|asinh?|atanh?|exp|log(10)?|deg2rad|rad2deg|sqrt|pow|abs|intval|ceil|floor|round|(mt_)?rand|gmp_fact)';
        $operators = '[\/*\^\+-,]';
        $regexp = '/^([+-]?(' . $number . '|' . $functions . '\s*\((?1)+\)|\((?1)+\))(?:' . $operators . '(?1))?)+$/';

        if (preg_match($regexp, $equation)) {
            $equation = preg_replace('!pi|π!', 'pi()', $equation);
            eval('$result = ' . $equation . ';');
        } else {
            $result = false;
        }
        return $result;
    }

    static function setRsaKey($key) {
        global $rsa;
        $rsa->loadKey($key);
    }

    static function CriptRsa($testo) {
        global $rsa;
        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $ciphertext = $rsa->encrypt($testo);

        return base64_encode($ciphertext);
    }

    static function DecriptRsa($testo) {
        global $rsa;
        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $ciphertext = base64_decode($testo);
        return $rsa->decrypt($ciphertext);
    }

    static function curPageURL() {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    static function toJSon($e) {
        if (isset($e)) {
            $_SESSION['json']['msg'] = $e;
        }

        $_SESSION['json']['tick'] = date('d/m/Y H:i:s', time());
        $r = json_encode($_SESSION['json']);
        unset($_SESSION['json']);
        return $r;
    }

    static function hook($hookA, $hookB, $array = false) {
        global $mysql;

        $latoA = explode("::", $hookA);
        $latoB = explode("::", $hookB);

        if ($array == false) {
            $where = "";
        } else {
            $where["{$latoA[0]}"] = [];
            $where["{$latoB[0]}"] = [];
        }

        $w = [];
        if ($latoA[0])
            $w[0] = "hook_nameA='{$latoA[0]}'";
        if ($latoA[1])
            $w[1] = "hook_valueA={$latoA[1]}";
        if ($latoB[0])
            $w[2] = "hook_nameB='{$latoB[0]}'";
        if ($latoB[1])
            $w[3] = "hook_valueB={$latoB[1]}";

        $filtro = "";
        foreach ($w as $key => $value) {
            if ($key > 0)
                $filtro.=" AND ";
            $filtro.=" {$value} ";
        }

        $c = $mysql->vQuery("SELECT * FROM hook WHERE (hook_holder={$_SESSION['piattaforma']} OR hook_holder=0) AND $filtro AND hook_visible=1");
        $m = $mysql->vQuery_count($c);
        for ($a = 0; $a < $m; $a++) {
            $l = $mysql->vQuery_read_key($c);
            if ($latoB[1]) {
                if ($a > 0 and $array == false) {
                    $where.=" OR ";
                }
                if ($array == false) {
                    $where.=" {$latoA[0]}_id={$l['hook_valueA']} ";
                } else {
                    array_push($where["{$latoA[0]}"], "{$l['hook_valueA']}");
                }
            }
            if ($latoA[1]) {
                if ($a > 0 and $array == false) {
                    $where.=" OR ";
                }
                if ($array == false) {
                    $where.=" {$latoB[0]}_id={$l['hook_valueB']} ";
                } else {
                    array_push($where["{$latoB[0]}"], "{$l['hook_valueB']}");
                }
            }
        }
        if ($m == 0) {
            if ($array == false) {
                $where.=" 0=1 ";
            }
        }
        return $where;
    }

    static function lang_extract($sigla, $in) {
        if (count($in) > 0) {
            $in = self::database_array_return_escape($in);
            foreach ($in as $key1 => $value1) {
                if (strpos($value1, "<is-array>") === 0) {
                    $value1 = str_replace("<is-array>", "", $in[$key1]);
                    $in[$key1] = json_decode($value1, true);
                }
            }

            foreach ($in as $key => $value) {
                if (!is_numeric($value) && !is_array($value)) {
                    preg_match_all("/<{$sigla}>(.*)<\/{$sigla}>/s", $value, $output_array);
                    preg_match_all("/<it>(.*)<\/it>/s", $value, $output_ita);

                    if (count($output_array[1][0]) == 0) {

                        if (isset($output_ita[1][0])) {
                            $data_lang[$key] = $output_ita[1][0];
                        } else {
                            $data_lang[$key] = $value;
                        }
                    } else {
                        if (isset($output_array[1][0])) {
                            $data_lang[$key] = $output_array[1][0];
                        } else {
                            $data_lang[$key] = $output_ita[1][0];
                        }
                    }
                } else {
                    $data_lang[$key] = $value;
                }
            }
            //print_r($data_lang);
            return $data_lang;
        } else {
            return $in;
        }
    }

    static function simpleRead($elemento, $tab) {
        $return = [];
        if (count($elemento) > 0) {
            foreach ($elemento as $key => $value) {
                $nK = str_replace("{$tab}_", "", $key);
                $return[$nK] = $value;
            }
        }
        return $return;
    }

    static function simpleOpenChild($flusso, $tab, $image = []) {
        foreach ($flusso as $key => $value) {
            $flusso[$key][$tab] = simpleImage(['flusso' => $value[$tab], 'image' => $image]);
        }
        return $flusso;
    }

    static function simpleOpen($fields = [], $vars_image = [], $vars_hook = []) {
        global $mysql;
        global $tpl;
        global $lang;

        if (!isset($fields['draw'])) {
            $fields['draw'] = 1;
        }

        $sigla = $_SESSION['lang'];
        $valore = [];

        $query = "SELECT * FROM {$fields['tab']}";
        if ($fields['where']) {
            $query.=" WHERE ({$fields['tab']}_holder={$_SESSION['piattaforma']} OR {$fields['tab']}_holder=0) AND {$fields['where']}";
        } else {
            $query.=" WHERE ({$fields['tab']}_holder={$_SESSION['piattaforma']} OR {$fields['tab']}_holder=0)";
        }
        if ($fields['order']) {
            $query.=" ORDER BY {$fields['order']}";
        }
        if ($fields['limit']) {
            $query.=" LIMIT {$fields['limit']}";
        }

        $c = $mysql->vQuery($query);
        for ($a = 0; $a < $mysql->vQuery_count($c); $a++) {
            $l[$a] = $mysql->vQuery_read_key($c);
            $l[$a] = database_array_return_escape($l[$a], $fields['br']);
            $l[$a] = self::lang_extract($sigla, $l[$a]);
            $valore[$a] = simpleRead($l[$a], $fields['tab']);

            if (count($vars_image) > 0) {
                foreach ($vars_image as $key => $value) {
                    if (is_array($valore[$a]["{$value}"])) {
                        foreach ($valore[$a]["{$value}"] as $k => $v) {
                            $valore[$a]["{$value}_img"][$k] = simpleImage($v);
                        }
                    } else {
                        $valore[$a]["{$value}_img"] = simpleImage($valore[$a]["{$value}"]);
                    }
                }
            }

            if (count($vars_hook) > 0) {
                foreach ($vars_hook as $key => $value) {
                    $valore[$a][$value] = simpleOpen(['tab' => $value, 'draw' => "0", 'where' => "{$value}_id=" . $valore[$a]["{$value}_id"]])[0];
                }
            }
        }
        if ($fields['draw'] == 1) {
            $tpl->assign($fields['tab'], $valore);
        }

        return $valore;
    }

    static function simpleReplace($var, $words = []) {
        foreach ($words as $key => $value) {
            $var = str_replace($key, $value, $var);
        }

        return $var;
    }

    static function simpleImage($dato) {
        if (is_array($dato)) {
            foreach ($dato['image'] as $key => $value) {
                if (is_array($dato['flusso']["{$value}"])) {
                    foreach ($dato['flusso']["{$value}"] as $k => $v) {
                        $dato['flusso']["{$value}_img"][$k] = simpleImage($v);
                    }
                } else {
                    $dato['flusso']["{$value}_img"] = simpleImage($dato['flusso']["{$value}"]);
                }
            }
            return $dato['flusso'];
        } else {
            $f = new file();
            return $f->file_info($dato)['file_fullname'];
        }
    }

    static function simpleModule($var, $destinatario) {
        unset($var['invia']);

        $msg = "";
        foreach ($var as $key => $value) {
            $value = htmlentities($value);
            $msg.="<b>{$key}</b>: {$value}<hr>";
        }

        inviaMail($destinatario, $var['name'], $var['oggetto'], $var['mail'], $msg);

        notify("Messaggio Inviato con successo!", 2);
    }

    /**
     * Social Engine Generator
     * @param $social [facebook, google-plus, twitter..]
     * @param $options[url_share] Link to share If Null Actual url
     * @param $options[url_photo] Piterest Require image If Null og_image.png
     * @param $options[size] [0-100]
     * @param $options[pages] {facebook=>URL,Twitter=>URL...}
     *
     * @author Jean Paul Sanchez <j.sanchez@webitart.com>
     * @return string $sharebox
     */
    function SocialEngine($social, $options = []) {
        global $global_path;
        if (!isset($options['size'])) {
            $options['size'] = 15;
        }

        $options['oggetto'] = rawurlencode($options['oggetto']);
        $options['messaggio'] = rawurlencode($options['messaggio']);


        if (!isset($options['url_share'])) {
            $options['url_share'] = self::curPageURL();
        }

        if (!isset($options['url_photo'])) {
            $options['url_photo'] = $global_path . "img/og_image.png";
        }

        if (!isset($options['pages'])) {
            $sharebox = "<ul class='menu'>";
            foreach ($social as $value) {
                switch ($value) {
                    case "facebook":
                        $sharebox .= "<li><a><span style=\"font-size: {$options['size']}px;color:#3b5998\" onclick=\"shareFunction('https://www.facebook.com/sharer/sharer.php?u={$options['url_share']}');\" class=\"fi-social-$value\"></span></a></li>";
                        break;
                    case "twitter":
                        $sharebox .= "<li><a><span style=\"font-size: {$options['size']}px;color:#4099FF;\" onclick=\"shareFunction('https://twitter.com/home?status={$options['url_share']}');\" class=\"fi-social-$value\"></span></a></li>";
                        break;
                    case "google-plus":
                        $sharebox .= "<li><a><span style=\"font-size: {$options['size']}px;color:#dd4b39;\" onclick=\"shareFunction('https://plus.google.com/share?url={$options['url_share']}');\" class=\"fi-social-$value\"></span></a></li>";
                        break;
                    case "linkedin":
                        $sharebox .= "<li><a><span style=\"font-size: {$options['size']}px;color:#007bb5;\" onclick=\"shareFunction('https://www.linkedin.com/shareArticle?mini=true&amp;url={$options['url_share']}');\" class=\"fi-social-$value\"></span></a></li>";
                        break;
                    case "pinterest":
                        $sharebox .= "<li><a><span style=\"font-size: {$options['size']}px;color:#cb2027;\" onclick=\"shareFunction('https://pinterest.com/pin/create/button/?url={$options['url_share']}&amp;media={$options['url_photo']}');\" class=\"fi-social-$value\"></span></a></li>";
                        break;
                    case "email":
                        $sharebox .= "<li><a href=\"mailto:?subject={$options['oggetto']}&amp;body={$options['messaggio']}\"><span style=\"font-size: {$options['size']}px;color:#ffbe00;\" class=\"fi-mail\"></span></a></li>";
                        break;
                }
            }
            $sharebox .= "</ul>";
        } elseif (is_array($options['pages'])) {
            $sharebox = "<ul class='inline-list'>";
            foreach ($social as $value) {
                $sharebox .= "<li><a href='{$options['pages'][$value]}' target='_blank'><span style=\"font-size: {$options['size']}px\" class=\"fi-social-$value\"></span></a></li>";
            }
            $sharebox .= "</ul>";
        }

        return $sharebox;
    }

    static function accessLevel($tab = "", $valore, $filtro) {
        if ($tab != "") {
            $tab.="_";
        }
        foreach ($filtro as $val) {
            if (isset($valore["{$tab}{$val}"])) {
                unset($valore["{$tab}{$val}"]);
            }
        }
        return $valore;
    }

    static function notify($msg, $tipo = 1) {
        if (!is_array($_SESSION['messaggio']))
            $_SESSION['messaggio'] = [];
        array_push($_SESSION['messaggio'], ['contenuto' => $msg, 'tipo' => $tipo]);
    }

    static function Debug($var, $block = true) {
        print_r($var);
        if ($block)
            die();
    }

    static function page404() {
        global $tpl;
        global $global_require_path;
        header('HTTP/1.0 404 Not Found');
        $tpl->display($global_require_path . "static functions/system/templates/404.tpl");
        exit();
    }

}
