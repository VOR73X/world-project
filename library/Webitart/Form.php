<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Form extends Webitart_Base {

    protected $fields = [];
    protected $template = "standard";
    protected $pathTemplate = "system/webitart/form";
    protected $action = "";
    protected $method = "post";
    protected $class = "";
    protected $uploadFile = false;
    protected $dataforms = [];

    protected $ids = [];
    protected $system = [];

    public function __construct($params){
        if($params['presetJsonToFile']){
            $params['presetJson'] = file_get_contents(Top::getAbsoluteUrl() . "application/modules/default/preset/" . $params['presetJsonToFile'] . ".json");
        }

        $obj = json_decode($params['presetJson'],true);

        foreach ($obj['fields'] as $key => $value) {
            $this->addField(new Webitart_Field([
                'presetJson'=>json_encode($value)
            ]),$key);
        }

        unset($obj['fields']);

        if($obj){
            $params['presetJson'] = json_encode($obj);
        }

        parent::__construct($params);

        $rand_id = "wbt" . rand(100000,999999);
        $this->system['id'] = $rand_id;
    }

    public function addField($field, $key=""){
        $preset = json_encode($field->getData());
        
        $element = new Webitart_Field([
            'presetJson' => $preset
        ]);

        if($key){
            $this->fields[$key] = $element;
        } else {
            array_push($this->fields, $element);
        }
        return $this;
    }

    public function addData($name, $value){
       $this->dataforms[$name]=$value;
       return $this;
    }

    public function getHtml(){
        $fielsHtml = "";
        foreach ($this->fields as $key => $value) {
            $fielsHtml .= $value->getHtml();
        }

        if ($this->template){
            $template = $this->template;
        }

        Top::$smarty->assign("bean", $this->getData());
        Top::$smarty->assign("fields", $fielsHtml);
        return Top::$smarty->outputString($this->pathTemplate . "/layout/" . $template . ".tpl");
    }

    public function getData(){
        $obj = parent::getData();
        unset($obj['fields']);

        $obj['ids'] = $this->ids;
        $obj['id'] = $obj['system']['id'] . " ";

        foreach ($this->ids as $key => $value) {
            $obj['id'] .= $value . " ";
        }
        $obj['id'] = trim($obj['id']);

        $obj['dataform']="";
        foreach ($this->dataforms as $key => $value) {
            $obj['dataform'] .= "data-" . $key . "=\"$value\" ";
        }

        foreach ($this->fields as $key => $value) {
            $obj['fields'][$key] = $value->getData();
        }

        return $obj;
    }

    public function get($name){
        return $this->fields[$name];
    }

    public function remove($name){
        unset($this->fields[$name]);
        return $this;
    }

    public function initModel($model,$label=false){
        foreach ($model as $key => $value) {
            if($this->get($key)){
                $this->get($key)->setValue($value);
            } else {
                $this->addField(new Webitart_Field(['name'=>$key,'value'=>$value]),$key);                
            }
            
            if($label){
                $this->get($key)->setLabel(ucfirst($key));
            }
        }
        return $this;
    }

    public function json(){
        header("Content-Type: application/json; charset='utf-8'");
        echo json_encode($this->getData());
    }

    public function preset(){
        return $this->getData();
    }
}
