<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Token
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Token {

    public function clearToken() {
        Top::$token->token = [];
    }

    public function checkToken($model, $action, $id, $token) {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        if (!$config->website->token){
            return true;
        }

        if (Top::$token->token["$model-$action-$id"]) {
            if (Top::$token->token["$model-$action-$id"]['count'] > 0) {
                Top::$token->token["$model-$action-$id"]['count'] --;
            }

            $check_token = $this->getToken($model, $action, $id);

            if ($check_token['life'] < 0) {
                unset(Top::$token->token["$model-$action-$id"]);
                return false;
            } elseif ($token != $check_token['token']) {
                return false;
            } else {
                Top::$token->token["$model-$action-$id"]['use'] = time();
                if ($check_token['count'] == 0) {
//                    unset(Top::$token->token["$model-$action-$id"]);
                    return false;
                }

                if ($check_token['wait'] >= $check_token['interval']) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function getToken($model, $action, $id) {
        $token = Top::$token->token["$model-$action-$id"];
        if (isset($token)) {
            $token['wait'] = time() - $token['use'];
            $token['remaining'] = $token['interval'] - $token['wait'];
            $token['life'] = $token['duration'] - (time() - $token['create']);
            $token['timestamp'] = time();
            return $token;
        } else {
            return false;
        }
    }

    function setToken($model, $action, $id, $options = []) {
        if (is_array($options)) {
            if (!$options['duration'])
                $options['duration'] = 3600;
            if (!$options['count'])
                $options['count'] = 1;
            if (!$options['interval'])
                $options['interval'] = 0;

            $oggetto = $options;
            $oggetto['token'] = $this->generateToken();
            $oggetto['create'] = time();
            $oggetto['use'] = time();
            Top::$token->token["$model-$action-$id"] = $oggetto;
        }
    }

    function generateToken() {
        $microTime = microtime();
        list($a_dec, $a_sec) = explode(" ", $microTime);

        $dec_hex = dechex($a_dec * 1000000);
        $sec_hex = dechex($a_sec);

        $this->ensure_length($dec_hex, 5);
        $this->ensure_length($sec_hex, 6);

        $guid = "";
        $guid .= $dec_hex;
        $guid .= $this->create_guid_section(3);
        $guid .= '-';
        $guid .= $this->create_guid_section(4);
        $guid .= '-';
        $guid .= $this->create_guid_section(4);
        $guid .= '-';
        $guid .= $this->create_guid_section(4);
        $guid .= '-';
        $guid .= $sec_hex;
        $guid .= $this->create_guid_section(6);

        return base64_encode(hash("sha256", $guid));
    }

    function create_guid_section($characters) {
        $return = "";
        for ($i = 0; $i < $characters; $i++) {
            $return .= dechex(mt_rand(0, 15));
        }
        return $return;
    }

    function ensure_length(&$string, $length) {
        $strlen = strlen($string);
        if ($strlen < $length) {
            $string = str_pad($string, $length, "0");
        } else if ($strlen > $length) {
            $string = substr($string, 0, $length);
        }
    }

}
