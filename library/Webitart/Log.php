<?php

/**
 * Description of Logger
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Webitart_Log extends Zend_Log {

    /**
     * @var \Zend_Log 
     */
    private static $instance;
    private static $date;
    private $_logMinLevel = NULL;

    public function __construct() {
        $writer = new Zend_Log_Writer_Stream(ROOT_PATH . "/var/log/" . date("Y-m-d") . ".log");
        parent::__construct($writer);
        $this->_logMinLevel = 3;
    }

    /**
     * @author Jean Paul Sanchez <j.sanchez@webitart.com>
     */
    public static function getInstance() {
        if (self::$instance === NULL || self::$date != date("Y-m-d")) {
            self::$instance = new Webitart_Log();
        }
        return self::$instance;
    }

    public function log($_namespace, $_level, $_message) {
        if (is_array($_message) || is_object($_message)) {
            $_message = json_encode($_message);
        }
        if ($_level <= $this->_logMinLevel) {
            parent::log(strtoupper($_namespace) . "\t" . $_message, $_level);
        }
    }

}
