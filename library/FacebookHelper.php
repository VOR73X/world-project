<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Top
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class FacebookHelper {

    protected $query;
    protected $limit;
    protected $data;
    protected $client;

    public function __construct(){
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $this->client =  new Facebook\Facebook([
          'app_id' => $config->facebook->appId,
          'app_secret' => $config->facebook->appSecret,
          'default_graph_version' => $config->facebook->version,
        ]);
    }

    public function getInstance(){
        return $this->client;
    }

    public function getUrlLogin($callback=null,$permissions=null){
        if(!$callback){
            $callback = Top::getBaseUrl() . "test/facebookcallback/";
        }
        if(!$permissions){
            $permissions = ['email']; // Optional permissions
        }

        //$fb = $this->client->getInstance();

        $helper = $this->client->getRedirectLoginHelper();

        $loginUrl = $helper->getLoginUrl($callback, $permissions);

        return htmlspecialchars($loginUrl);
    }

    public function callbackPoint($longToken=false){
        //$fb = $this->getInstance();

        $helper = $this->client->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        if($longToken){
            $oAuth2Client = $this->client->getOAuth2Client();
            if (! $accessToken->isLongLived()) {
              try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
              } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
              }
            }

        }

        $_SESSION['Facebook_token'] = $accessToken;

        return $accessToken;
    }

    public function initData($query, $limit = 10){
        $this->query = $query;
        $this->limit = $limit;
        $this->data = $this->callQuery();
    }

    public function getData(){
        return $this->data;
    }

    public function getQuery($query){
        $obj = [];
        if($_SESSION['Facebook_token']){
            $accessToken =$_SESSION['Facebook_token'];

            $response = $this->client->get($query, $accessToken);
            
            $obj['responde'] = $response;
            $obj['array'] = $response->getDecodedBody();

        }

        return $obj;
    }

    public function authenticate(){
        $obj = $this->getQuery('me?fields=id,name,email');
        return $obj['array'];
    }

    protected function callQuery(){
        //$fb = $this->getInstance();

        $obj = [];
        if($_SESSION['Facebook_token']){
            $accessToken =$_SESSION['Facebook_token'];

            $response = $this->client->get($this->query . "&limit=". $this->limit, $accessToken );
            
            $obj['responde'] = $response;
            $obj['array'] = $response->getDecodedBody();

        }

        return $obj;
    }

    public function nextData(){
        //$fb = $this->getInstance();

        $obj = [];
        $helper = $this->client->getRedirectLoginHelper();
        if($_SESSION['Facebook_token']){
            $accessToken =$_SESSION['Facebook_token'];
            $pageToken = $this->data['array']['paging']['cursors']['after'];

            if($pageToken){
                $response = $this->client->get($this->query . "&limit=". $this->limit . "&after=" . $pageToken , $accessToken );
                $obj['responde'] = $response;
                $obj['array'] = $response->getDecodedBody();
                $this->data = $obj;
            }
        }
    }

    public function prevData(){
        //$fb = $this->getInstance();

        $obj = [];
        $helper = $this->client->getRedirectLoginHelper();
        if($_SESSION['Facebook_token']){
            $accessToken =$_SESSION['Facebook_token'];
            $pageToken = $this->data['array']['paging']['cursors']['before'];

            if($pageToken){
                $response = $this->client->get($this->query . "&limit=". $this->limit . "&before=" . $pageToken , $accessToken );
            
                $obj['responde'] = $response;
                $obj['array'] = $response->getDecodedBody();
                $this->data = $obj;
            }

        }
    }

    public function allListVideos($pageId){
        $obj = [];

        $cacheManager = Zend_Registry::get('cacheGeneric');
        $cache = $cacheManager->getCache('generic');
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $cacheID = "{$pageId}allListVideosFacebook";

        if (false === ($obj = $cache->load($cacheID))) {
            $this->initData("$pageId/videos?fields=title,description,created_time,id,from",10);

            foreach ($this->getData()['array']['data'] as $key => $value) {
                $obj[] = $value;
            }   
            
            $i=0;
            do {
                $this->nextData();
                $elements = $this->getData()['array']['data'];
                foreach ($elements as $key => $value) {
                    $obj[] = $value;
                }
                $i++;
            } while (count($elements)>0);

            if ($config->website->cache->db) {
                $cache->save($obj, $cacheID);
            }
        }
        
        
        return $obj;
    }
}