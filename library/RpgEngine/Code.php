<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RpgEngine_Code
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class RpgEngine_Code {
	public static $_env = [];
	public static $_temp = [];
	public static $_private = [];
	public static $_multiplayer = [];
	public static $_system = [];
	public static $_traking = 0;
	public static $_pending = 0;

	Private $xml;
	private $firstLevel = false;
	private $traking = 0;

	public function __construct($xml){
		$this->xml = $xml;
	}

	public function encode($system=null){		
		if($system){
			if($system['env']){
				self::$_env = $system['env'];
			}

			if($system['temp']){
				self::$_temp = $system['temp'];
			}

			if($system['private']){
				self::$_private = $system['private'];
			}

			if($system['multiplayer']){
				self::$_multiplayer = $system['multiplayer'];
			}

			self::$_system['env'] = &self::$_env;
			self::$_system['temp'] = &self::$_temp;
			self::$_system['private'] = &self::$_private;
			self::$_system['multiplayer'] = &self::$_multiplayer;

			$this->firstLevel = true;

		}

		foreach ($this->xml as $attribute => $xmlBody) {
			if(!self::$_pending){
				self::$_traking ++;
				$this->traking ++;
				if(!$index[$attribute]){
					$index[$attribute]=0;
				}

				$xmlElement = $this->xml->{$attribute}[$index[$attribute]];

				$returnInclude = include "library/RpgEngine/Properties/{$attribute}.php";
				if($returnInclude){
					if($returnInclude['result']){
						return $returnInclude['result'];
					}
				}
				$index[$attribute]++;
			}
		}

		if($this->firstLevel){
			self::$_traking = 0;
		}

		$return = [];
		$return['status'] = "complete";
		$return['traking'] = self::$_traking;
		$return['env'] = self::$_env;
		$return['private'] = self::$_private;
		$return['multiplayer'] = self::$_multiplayer;

		return $return;
	}

}
