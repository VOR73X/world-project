<?php

$name = (string)$xmlElement['name'];

Top::print_d("Include: " . $name);

$script = self::$_env['script'][$name];
if($script){
	$xmlInclude = simplexml_load_string($script['script']) or self::$_private['errors'][] = "Error: Cannot create object AbsoluteLine: " . self::$_traking . " CurrentLine: " . $this->traking . " Script: " . $script['name'];
	$includeInstance = new RpgEngine_Code($xmlInclude);
	$returnChild = $includeInstance->encode();
	
	if($returnChild['status']=="pending"){
		$return = [];
		$return['status']="pending";
		$return['traking'] = $returnChild['traking'];
		$return['env'] = $returnChild['env'];
		$return['private'] = $returnChild['private'];
		$return['multiplayer'] = $returnChild['multiplayer'];
		$return['temp'] = $returnChild['temp'];
		return ['result'=>$return];
	}
}