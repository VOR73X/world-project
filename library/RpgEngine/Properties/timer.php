<?php
Top::print_d("Timer");

$name  = RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['name']);
$object = RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['object'],true);
$action = (string)$xmlElement['action'];
$start = (int)RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['start']);
$end = (int)RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['end']);

$var = $object;

//se non impostato action di default è Play
if(!$action){
	$action="play";
}

//se non esistente viene creata la base dell'oggetto
if(!self::$_private['timers'][$name]){
	self::$_private['timers'][$name]=[
		'status'=> 'pause',
		'time'=> Time(),
		'start'=>$start,
		'end'=>$end
	];
}

//Retrieve referenza Timer
$timer = &self::$_private['timers'][$name];
$start = $timer['start'];
$end = $timer['end'];

//Calcolo direzione timer
$directionTimer = "";
if($start>$end){
	$directionTimer = "-";
}elseif($start<$end){
	$directionTimer = "+";
}


//Retrieve variabile attuale
$actual = RpgEngine_Helper::getKeyBalancer(self::$_system,$var);
if(!$actual){
	RpgEngine_Helper::setKeyBalancer(self::$_system, $var, $start);
	$actual = $start;
}

//Da Pause a Play
if($timer['status']=='pause' && $action=='play'){
	$timer['time'] = Time();
	$timer['status']=$action;
}

//Calcolo Delta
$delta = Time() - $timer['time'];

//Calcola nuovo stato
if($directionTimer=="+"){
	$actualNew = $actual + $delta;
	if($actualNew>=$end){
		$actualNew=$end;
		$timer['status']='pause';
		RpgEngine_Helper::setKeyBalancer(self::$_system, $var, $actualNew);
		$callback=true;
	}
}elseif($directionTimer=="-"){
	$actualNew = $actual - $delta;
	if($actualNew<=$end){
		$actualNew=$end;
		$timer['status']='pause';
		RpgEngine_Helper::setKeyBalancer(self::$_system, $var, $actualNew);
		$callback=true;
	}
}

if($timer['status']=='play' && $action=='pause'){
	//Da Play a Pause Aggiorna Var
	RpgEngine_Helper::setKeyBalancer(self::$_system, $var, $actualNew);
	$timer['status']=$action;
}elseif($action=='stop'){
	//Da Aggiorna Var a Start
	RpgEngine_Helper::setKeyBalancer(self::$_system, $var, $start);
	$timer['status']='pause';
}elseif($timer['status']=='play' && $action=='play'){
	//Update
	RpgEngine_Helper::setKeyBalancer(self::$_system, $var, $actualNew);
}

$timer['time'] = Time();

if($callback){
	if($xmlElement->code){
		$timerInstance = new RpgEngine_Code($xmlElement->code->children());
		$returnChild = $timerInstance->encode();
		if($returnChild['type']=="pending"){
			$return = [];
			$return['status']="pending";
			$return['traking'] = $returnChild['traking'];
			$return['env'] = $returnChild['env'];
			$return['temp'] = $returnChild['temp'];
			$return['private'] = $returnChild['private'];
			$return['multiplayer'] = $returnChild['multiplayer'];
			return ['result'=>$return];
		}
	}
}

$actualRefresh=RpgEngine_Helper::getKeyBalancer(self::$_system,$var);
self::$_env['timers'][$name]=[
	'status'=>$timer['status'],
	'value'=>$actualRefresh
];