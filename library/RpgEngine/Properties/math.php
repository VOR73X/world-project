<?php 
$variable = RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['object'], true);
$value = (string)$xmlElement['value'];
$formulaString = RpgEngine_Helper::extractVariables(self::$_system, $value);			

Top::print_d("Math: variable => " . $variable . " formulaString => " . $formulaString . " value => " . $value);

RpgEngine_Helper::setKeyBalancer(self::$_system, $variable,RpgEngine_Helper::formula($formulaString));
