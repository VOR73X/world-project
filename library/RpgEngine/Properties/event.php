<?php
$name = (string)$xmlElement['name'];

if(substr($name,0,3) =="\$me"){
	$name = str_replace("\$me", self::$_temp['me'], $name);
}else{
	$name = RpgEngine_Helper::extractVariables(self::$_system,(string)$xmlElement['name']);
}

$action = (string)$xmlElement['action'];
Top::print_d("Event: " . $name . " action: " . $action);


$event = &self::$_env['events'][$name];
$eventPrivate = &self::$_env['events'][$name];

switch ($action) {
	case 'wait':
		$event['timestamp'] = time();
		break;
	case 'teleportToRand':
		$min = (int)$xmlElement['min'];
		$max = (int)$xmlElement['max'];
		$latitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['latitude']);
		$longitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['longitude']);

		$newPoint = RpgEngine_Helper::randomPointGps([$latitude, $longitude], $min, $max);

		$event['latitude'] = $newPoint['coords']['latitude'];
		$event['longitude'] = $newPoint['coords']['longitude'];
		$event['timestamp'] = -1;

		break;
	case 'teleportTo':
		$latitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['latitude']);
		$longitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['longitude']);	

		$event['latitude'] = $latitude;
		$event['longitude'] = $longitude;
		$event['timestamp'] = -1;

		break;
	case 'moveTo':
		$latitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['latitude']);
		$longitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['longitude']);
		$speed =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['speed']);

		$coordsTo = ['latitude'=>$latitude, 'longitude'=>$longitude];
		$coordsFrom = ['latitude'=>$event['latitude'], 'longitude'=>$event['longitude']];

		$newPositon = RpgEngine_Helper::movePointGps($speed,$event,$coordsTo);

		$event['latitude'] = $newPositon['latitude'];
		$event['longitude'] =  $newPositon['longitude'];
		$event['timestamp'] = time();
		break;
	case 'moveToRand':
		$latitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['latitude']);
		$longitude =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['longitude']);
		$speed =  RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['speed']);

		if($eventPrivate){
			$newPoint = $eventPrivate;
		}else{
			$newPoint = RpgEngine_Helper::randomPointGps([$latitude, $longitude], $distance);
			$eventPrivate = $newPoint;
		}

		$coordsTo = $newPoint;
		$coordsFrom = ['latitude'=>$event['latitude'], 'longitude'=>$event['longitude']];

		$newPositon = RpgEngine_Helper::movePointGps(1,$event,$coordsTo);

		$event['latitude'] = $newPositon['latitude'];
		$event['longitude'] =  $newPositon['longitude'];
		$event['timestamp'] = time();
		break;
	default:
		# code...
		break;
}