<?php

/*
	il tag cmd permette al client di invokare azioni on demand

	chiama un azione di default quando non viene passata nessun'altra action dal client
	<cmd name="custom-name" passive="true">
		{xmlBody}  		
	</cmd>

	include uno script
	<cmd name="custom-name" include="script-name"/>

	esegue il code contenuto dentro il cmd
	<cmd name="custom-name">
	</cmd>

*/

if(!$name = RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['name'])){
	$name="unknow";
}
Top::print_d("Cmd: {$name}");

$isCmdPassive = (string)$xmlElement['passive'];
if($requestedAction  = self::$_temp['req_cmd'] && self::$_temp['req_cmd'] != ''){
	$requestedAction = self::$_temp['req_cmd'];
}

//se il cmd di riferimento non è passivo viene aggiunto in risposta all'elenco delle cmd attivabili
if(!$isCmdPassive){
	if(!$type = (string)$xmlElement['type']){
		$type = 'core';
	}
	if(!in_array(self::$_traking, self::$_temp['trakingTrace'])){
		self::$_private['cmd'][$name] = [
		'action' => $name,
		'type' => $type
		];
	}
}

//se non è stata richiesta nessun action da parte del client carico il contenuto del cmd passivo
//altrimenti controllo se l'azione richiesta corrisponde con il cmd iterato e carico lo script incluso
$script = null;
$innerCode = $xmlElement->asXml();

if($isCmdPassive && !$requestedAction){
	$script = $innerCode;
} else if($requestedAction == $name){
	$scriptName = (string)$xmlElement['include'];
	if($script = self::$_env['script'][$scriptName]){
		$script = $script['script'];
	} else {
		$script = $innerCode;
	}
}

//se esiste uno script incluso o del contenuto dentro cmd lo invoco
if($script){
	$xmlInclude = simplexml_load_string($script) or self::$_private['errors'][] = "Error: Cannot create object AbsoluteLine: " . self::$_traking . " CurrentLine: " . $this->traking . " Script: " . $script['name'];

	$includeInstance = new RpgEngine_Code($xmlInclude);
	$returnChild = $includeInstance->encode();

	if($returnChild['status']=="pending"){
		$return = [];
		$return['status']="pending";
		$return['traking'] = $returnChild['traking'];
		$return['env'] = $returnChild['env'];
		$return['private'] = $returnChild['private'];
		$return['multiplayer'] = $returnChild['multiplayer'];
		$return['temp'] = $returnChild['temp'];
		return ['result'=>$return];
	}
}

