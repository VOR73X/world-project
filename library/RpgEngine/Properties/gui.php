<?php
	
$name = RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['name']);
$object = (string)$xmlElement['object'];
$label = RpgEngine_Helper::extractVariables(self::$_system, (string)$xmlElement['label']);
$keys = (string)$xmlElement['keys'];

if(trim($name)==""){
	$name=$object;
}

$exlodeKey = [];
if(trim($keys)!=""){
	$exlodeKey = explode(",", $keys);
}

$objectKey = RpgEngine_Helper::extractVariables(self::$_system, $object,true);
$value = RpgEngine_Helper::getKeyBalancer(self::$_system, $objectKey);

Top::print_d("Gui: name => " . $name . " label => " . $label . " object => " . $object . " value: " . $value . " keys: " . $keys);

self::$_private['gui'][$name] = [
	'type'=>"gui",
	'label'=>$label,
	'value'=>$value,
	'keys'=>$exlodeKey,
	'name'=>$name
];