<?php 

$object = (string)$xmlElement['object'];
$name = RpgEngine_Helper::extractVariables(self::$_system,(string)$xmlElement['name']);
$to = RpgEngine_Helper::extractVariables(self::$_system,(string)$xmlElement['to'], true);
Top::print_d("Clone: " . $object . " name: " . $name . " to: " . $to);

if(strpos($object,"$")>-1){	
	if(substr($object,0,3) =="\$me"){
		$object = str_replace("\$me", "\$events." . self::$_temp['me'], $object);
	}

	$var = RpgEngine_Helper::extractVariables(self::$_system,$object,true);

	$newName = $name;
	if(trim($to)==""){
		$newVar = "";

		$explode = explode(".", $var);

		if(count($explode)>1){
			foreach ($explode as $key => $value) {
				if(count($explode)-1>$key){
					$newVar.=$value.".";
				}	
			}
		}


		$newVar = $newVar . $newName;
	}else{
		$newVar=$to;
	}

	$target = RpgEngine_Helper::getKeyBalancer(self::$_system, $var);

	if($target['name']){
		$target['name']=$newName;
	}

	RpgEngine_Helper::setKeyBalancer(self::$_system, $newVar,$target);
}
