<?php 

$clause = (string)$xmlElement['clause'];

$re = "/([\\w.\\$\\{\\}]+)( )*(==|>>|<<|>=|<=|!=)( )*([[\\w.\\$\\{\\}]+)/"; 
preg_match($re, $clause, $matches);

$param = RpgEngine_Helper::extractVariables(self::$_system, $matches[1]);
$operator = $matches[3];
$matches[5] = str_replace("null", null, $matches[5]);
$comparator = RpgEngine_Helper::extractVariables(self::$_system, $matches[5]);

Top::print_d("if: " . $clause . " param: " . $param . " operator: " . $operator . " comparator: " . $comparator);

$resultOperation=false;
switch ($operator) {
	case '==':
		if($param==$comparator){
			$resultOperation=true;
		}
		break;
	case '!=':
		if($param!=$comparator){
			$resultOperation=true;
		}
		break;
	case '>=':
		if($param>=$comparator){
			$resultOperation=true;
		}
		break;
	case '<=':
		if($param<=$comparator){
			$resultOperation=true;
		}
		break;
	case '>':
		if($param>$comparator){
			$resultOperation=true;
		}
		break;
	case '<':
		if($param<$comparator){
			$resultOperation=true;
		}
		break;
	default:
		# code...	
	break;
}

if($resultOperation){
	if($xmlElement->true){
		Top::print_d("If true");
		$conditionInstance = new RpgEngine_Code($xmlElement->true->children());
		$returnChild = $conditionInstance->encode();
		Top::print_d("If execute true");
	}
}else{
	if($xmlElement->false){
		Top::print_d("If false");
		$conditionInstance = new RpgEngine_Code($xmlElement->false->children());
		$returnChild = $conditionInstance->encode();
		Top::print_d("If execute false");
	}
}



if($returnChild['status']=="pending"){
	$return = [];
	$return['status']="pending";
	$return['traking'] = $returnChild['traking'];
	$return['env'] = $returnChild['env'];
	$return['temp'] = $returnChild['temp'];
	$return['private'] = $returnChild['private'];
	$return['multiplayer'] = $returnChild['multiplayer'];

	Top::print_d("If Pending");
	return ['result'=>$return];
}