<?php 

$from = RpgEngine_Helper::getKeyBalancer(self::$_system, (string)$xmlElement['from']);
$keyName = (string)$xmlElement['key'];
$valueName = (string)$xmlElement['value'];
Top::print_d("Foreach: from");
Top::print_d($from);

foreach ($from as $key => $value) {
	RpgEngine_Helper::setKeyBalancer(self::$_system,$keyName, $key);
	RpgEngine_Helper::setKeyBalancer(self::$_system,$valueName, $value);

	$forEachInstance = new RpgEngine_Code($xmlElement->code->children());
	$forEachInstance->encode();
}
