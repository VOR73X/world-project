<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RpgEngine_Helper
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class RpgEngine_Helper {

    /**
     * Open File to path
     * @param  String $path Url Abosolute to open file
     * @return String       Content file
     */
    public static function openFile($path){
        return file_get_contents($path);
    }

    /**
     * Save File to path
     * @param  String $path Absolute path to save file
     * @param  String $text Content to save file
     */
    public static function saveFile($path,$text){
        file_put_contents($path, $text);
    }

	/**
	 * Get the string var tree
	 * @param  Object $var Insert the object to read
	 * @param  String $key Insert a string Key
	 * @return object      Return the value to string key
	 */
	public static function getKey($var,$key){
        $explode = explode(".", $key) ;
        $v=$var;
        foreach ($explode as $element) {
            $element = self::extractVariables($var['env'],$element);

        	try {
				if($v->{$element}){
					$v = $v->{$element};
				}elseif(is_array($v)){
					$v = $v[$element];
				}else{
					return false;
				}
        	} catch (Exception $e) {
        	    Top::print_d('Caught exception: ',  $e->getMessage(), "\n");
        	}
        }

        if(!$v){
            return false;
        }
        return $v;
    }

    public static function setKeyBalancer(&$var,$key,$value){
        $workEnv = null;
        if(substr($key, 0,2)=="$$"){
            $workEnv=&$var['multiplayer'];
        }elseif(substr($key, 0,1)=="$"){
            $workEnv=&$var['env'];
        }

        $keyParser = str_replace("$", "", $key);
        self::setKey($workEnv,$keyParser,$value);
    }

    public static function getKeyBalancer($var,$key){
        $workEnv = null;
        if(substr($key, 0,2)=="$$"){
            $workEnv=$var['multiplayer'];
            Top::print_d("getKeyBalancer: {$key} $$");
        }elseif(substr($key, 0,1)=="$"){
            $workEnv=$var['env'];
            Top::print_d("getKeyBalancer: {$key} $");
        }

        $keyParser = str_replace("$", "", $key);

        return self::getKey($workEnv,$keyParser);
    }

    /**
     * Set to tree structure object a value
     * @param Object &$var  Reference to object source
     * @param String $key   Key tree object
     * @param Object $value Object to set to tree object
     */
    public static function setKey(&$var, $key, $value){
        $explode = explode(".", $key) ;
        $v=&$var;
        foreach ($explode as $element) {
        	try {
				if($v->{$element}){
					$v = &$v->{$element};
				}elseif(is_array($v)){
					$v = &$v[$element];
				}else{
					$v=&$v[$element];
				}
        	} catch (Exception $e) {
        	    Top::print_d('Caught exception: ',  $e->getMessage(), "\n");
        	}
        }
        $v = $value;
    }

    /**
     * Extract Variables to String
     * @param  Object $var Object to scan
     * @param  String $str Strint to extrack key
     * @return String      Return string with values
     */
    public static function extractVariables($var, $str,$onlyInner=false){
        $workEnv = $var['env'];
        if($onlyInner){
            $re = "/\\{\\$\\$?[\\w\\.]+\\}/";
        }else{
            $re = "/\\{?\\$\\$?[\\w\\.]+\\}?/"; 
            $str = self::extractVariables($var,$str,true);  
        }
        preg_match_all($re, $str, $matches);

        foreach ($matches[0] as $key => $value) {
            if(substr($value, 0,2)=="$$"){
                $workEnv=$var['multiplayer'];
            }

            $value = str_replace("{", "", $value);
            $value = str_replace("}", "", $value);
            $valueOrigin =  $value;

            $value = str_replace("$", "", $value);
            $value = trim($value);

            if(substr($value,0,3) =="me." || $value=="me"){
                $value = "events." . $var['temp']['me'] . substr($value,2,999999);
            }

            $returnValue = self::getKey($workEnv,$value);
            if($returnValue){
                $str = str_replace($valueOrigin, self::getKey($workEnv,$value), $str);
            }else{
                $str = str_replace($valueOrigin, "", $str);
            }
        }
        $str = str_replace("{", "", $str);
        $str = str_replace("}", "", $str);
    	return $str;
    }

    /**
     * Execute a formula string
     * @param  String $equation Formula string
     * @return Value           Return Result equation
     */
    public static function formula($equation){
        $result = 0;
        $equation = preg_replace('/\s+/', '', $equation);
        $number = '((?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?|pi|π)';
        $functions = '(?:sinh?|cosh?|tanh?|acosh?|asinh?|atanh?|exp|log(10)?|deg2rad|rad2deg|sqrt|pow|abs|intval|ceil|floor|round|(mt_)?rand|gmp_fact)';
        $operators = '[\/*\^\+-,]';
        $regexp = '/^([+-]?(' . $number . '|' . $functions . '\s*\((?1)+\)|\((?1)+\))(?:' . $operators . '(?1))?)+$/';

        if (preg_match($regexp, $equation)) {
            $equation = preg_replace('!pi|π!', 'pi()', $equation);
            eval('$result = ' . $equation . ';');
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * Calculate distasce between two points GPS
     * @param  Double $latitudeFrom  Latitude From
     * @param  Double $longitudeFrom Longitude From
     * @param  Double $latitudeTo    Latitude To
     * @param  Dobule $longitudeTo   Longitude To
     * @return Double                Return distance in Meter
     */
    public static function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo){
    	$earthRadius = 6371000;
    	$latFrom = deg2rad($latitudeFrom);
    	$lonFrom = deg2rad($longitudeFrom);
    	$latTo = deg2rad($latitudeTo);
    	$lonTo = deg2rad($longitudeTo);

    	$lonDelta = $lonTo - $lonFrom;
    	$a = pow(cos($latTo) * sin($lonDelta), 2) +
    		pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    	$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

    	$angle = atan2(sqrt($a), $b);
    	return $angle * $earthRadius;
    }

    /**
     * Calculate direction between two points GPS
     * @param  Double $latitudeFrom  Latitude From
     * @param  Double $longitudeFrom Longitude From
     * @param  Double $latitudeTo    Latitude To
     * @param  Dobule $longitudeTo   Longitude To
     * @return Double                Return Direnction in degrees
     */
    public static function direction($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo) {
    	 $dLon = deg2rad($longitudeTo) - deg2rad($longitudeFrom);
    	 $dPhi = log(tan(deg2rad($latitudeTo) / 2 + pi() / 4) / tan(deg2rad($latitudeFrom) / 2 + pi() / 4));
     
    	 if(abs($dLon) > pi()  ) {
    			if($dLon > 0) {
    				 $dLon = (2 * pi() - $dLon) * -1;
    			}
    			else {
    				 $dLon = 2 * pi() + $dLon;
    			}
    	 }
    	 return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
    }

    /**
     * Convert Degrees in Cardinal Points
     * @param  Double $bearing Degrees direction
     * @return String          String direction
     */
    public static function directionString($bearing) {
    	 $tmp = round($bearing / 22.5);
    	 switch($tmp) {
    			case 1:
    				 $direction = "NNE";
    				 break;
    			case 2:
    				 $direction = "NE";
    				 break;
    			case 3:
    				 $direction = "ENE";
    				 break;
    			case 4:
    				 $direction = "E";
    				 break;
    			case 5:
    				 $direction = "ESE";
    				 break;
    			case 6:
    				 $direction = "SE";
    				 break;
    			case 7:
    				 $direction = "SSE";
    				 break;
    			case 8:
    				 $direction = "S";
    				 break;
    			case 9:
    				 $direction = "SSW";
    				 break;
    			case 10:
    				 $direction = "SW";
    				 break;
    			case 11:
    				 $direction = "WSW";
    				 break;
    			case 12:
    				 $direction = "W";
    				 break;
    			case 13:
    				 $direction = "WNW";
    				 break;
    			case 14:
    				 $direction = "NW";
    				 break;
    			case 15:
    				 $direction = "NNW";
    				 break;
    			default:
    				 $direction = "N";
    	 }
    	 return $direction;
    }

    /* @param  array $centre Numeric array of floats. First element is 
     *                       latitude, second is longitude.
     * @param  float $radius The radius (in miles).
     * @return array         Numeric array of floats (lat/lng). First 
     *                       element is latitude, second is longitude.
     */
 	public static function randomPointGps($coords, $min, $max ){
		$radius_earth = 3959; //miles

        //Pick random distance within $distance;
        $min = ($min / 1000) * 0.621371;
        $max = ($max / 1000) * 0.621371;

		$distance = (lcg_value()*($max-$min))+$min;

		//Convert degrees to radians.
		$centre_rads = array_map( 'deg2rad', $coords );

		//First suppose our point is the north pole.
		//Find a random point $distance miles away
		$lat_rads = (pi()/2) -  $distance/$radius_earth;
		$lng_rads = lcg_value()*2*pi();

		//($lat_rads,$lng_rads) is a point on the circle which is
		//$distance miles from the north pole. Convert to Cartesian
		$x1 = cos( $lat_rads ) * sin( $lng_rads );
		$y1 = cos( $lat_rads ) * cos( $lng_rads );
		$z1 = sin( $lat_rads );


		//Rotate that sphere so that the north pole is now at $centre.

		//Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
		$rot = (pi()/2) - $centre_rads[0];
		$x2 = $x1;
		$y2 = $y1 * cos( $rot ) + $z1 * sin( $rot );
		$z2 = -$y1 * sin( $rot ) + $z1 * cos( $rot );

		//Rotate in z axis by $rot = $centre_rads[1]
		$rot = $centre_rads[1];
		$x3 = $x2 * cos( $rot ) + $y2 * sin( $rot );
		$y3 = -$x2 * sin( $rot ) + $y2 * cos( $rot );
		$z3 = $z2;


		//Finally convert this point to polar co-ords
		$lng_rads = atan2( $x3, $y3 );
		$lat_rads = asin( $z3 );

		$temp = array_map( 'rad2deg', array( $lat_rads, $lng_rads ));

		$return['coords']['latitude'] = $temp[0];
		$return['coords']['longitude'] = $temp[1];
		return $return;
	}

 	public static function movePointGps($speed, $coordsFrom, $coordsTo){
        if($coordsFrom['timestamp']<=0 || !$coordsFrom['timestamp']){
            $coordsFrom['timestamp']=time();
        }
		$delta = time() - $coordsFrom['timestamp'];

		$speed = $speed * $delta;

        $distance = self::distance($coordsTo['latitude'],$coordsTo['longitude'],$coordsFrom['latitude'],$coordsFrom['longitude']);
        if($speed>$distance){
            $ratio=$distance;
            $newLat = $coordsTo['latitude'];
            $newLng = $coordsTo['longitude'];
        }else{
            $ratio = $speed / $distance;            
    		$newLat = $coordsFrom['latitude'] + (($coordsTo['latitude'] - $coordsFrom['latitude']) * $ratio);
            $newLng = $coordsFrom['longitude'] + (($coordsTo['longitude'] - $coordsFrom['longitude']) * $ratio);
        }


		$return['latitude'] = $newLat;
		$return['longitude'] = $newLng;

		return $return;
	}

}