<?php

/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.lang.php
 * Type:        function
 * Name:        lang
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - ts         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * <div>{agots ts="3434323"} ago </div>
 */

function smarty_function_lang($params, &$smarty) {
    $name = explode(".tpl", $smarty->source->name)[0];
    $controller_explode = explode("/", $smarty->source->filepath);
    $controller = $controller_explode[count($controller_explode) - 2];
    $keys = array_keys($params);
    $lang = "";
    if (isset($GLOBALS['lang'])) {
        $lang = $GLOBALS['lang']['it'][$controller][$name][$keys[0]];
    }
    $value = "";
    if (Top::getLangDebug() == 1) {
        return "<b>{$keys[0]}</b>";
    } else {
        if ($lang != "") {
            $value = $lang;
        } elseif ($params[$keys[0]] != "") {
            $value = $params[$keys[0]];
        } else {
            $value = "<b>$keys[0]</b>";
        }
    }
    return $value;
}
