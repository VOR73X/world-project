<?php

/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.lang.php
 * Type:        function
 * Name:        lang
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - ts         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * <div>{agots ts="3434323"} ago </div>
 */

function smarty_function_jsfunction($params, &$smarty) {
    $id = $params['id'];
    $obj = "var preView = function(value, id){";
    $obj .= $params['preview'];
    $obj .= "return value;};";
    return $obj;
}
