<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of functions
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
function smarty_function_config($params, &$smarty) {
    $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
    foreach (explode("->", $params['var']) as $key => $value) {
        if ($key == 0) {
            $el = $config->{$value};
        } else {
            $el = $el->{$value};
        }
    }
    return $el;
}
