<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Event
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 * @PrimaryKey: id
 * @Table: event
 * @ClassReferenced: {"script":["script","id"],"scene":["scene","id"]}
 * @Field: name VARCHAR(45)
 * @Field: latitude DECIMAL(13,10) 0.0
 * @Field: longitude DECIMAL(13,10) 0.0
 * @Field: script INT(11)
 * @Field: scene INT(11)
 * @Field: visible TINYINT(1) 0
 * @Field: enable TINYINT(1) 0
 * @Field: title VARCHAR(45)
 * @Field: save TINYINT(1) 0
 * @Field: abstract TINYINT(1) 0
 * @Field: multiplayer TINYINT(1) 0
 */
class Default_Model_event extends Default_Model_Resource {

}
