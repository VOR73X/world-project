<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Database
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Default_Model_Database extends Zend_Db_Table_Abstract {

    function getList($page = 1) {
        $responde['data'] = $this->fetchAll()->toArray();
        return $responde;
    }

    function getListPaginator($page = 1, $from = [], $where = "", $order = []) {
        $elementi = $this->select()->from($this->_name, $from);

        if ($where != "") {
            $elementi = $elementi->where($where);
        }

        $elementi->order($order);

        $adapter = new Zend_Paginator_Adapter_DbSelect($elementi);
        $adapter->getCountSelect();

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);

        $responde['data'] = $paginator->getItemsByPage($page);
        $responde['paginator'] = $paginator->getPages();

        return $responde;
    }

    function getHeaders() {
        return $this->info(Zend_Db_Table_Abstract::COLS);
    }

    function ImportVideo() {
        include ("/home/jean/Scaricati/user.php");
        $query = "";
        $datecreate = date('Y-m-d H:i:s');

        if ($video) {
            foreach ($video as $key => $value) {
                $obj[$key]['title'] = $value['video_title'];
                $obj[$key]['description'] = $value['video_description'];
                $obj[$key]['create_date'] = $datecreate;
                $obj[$key]['resource_id'] = $value['video_name'];
                $obj[$key]['source_id'] = $value['video_channel'];
                $obj[$key]['source_name'] = $value['video_channel_title'];
                $obj[$key]['source_platform'] = 1;
                $obj[$key]['user_id'] = $value['video_user_id'];
            }

            foreach ($obj as $key => $value) {
                $base = "INSERT INTO `tubooster`.`video` "
                        . "(`id`, `title`, `description`, `create_date`, `resource_id`, `source_id`, `source_name`, `source_platform`, `user_id`) VALUES "
                        . "(NULL, '{$value['title']}', '{$value['description']}', '{$value['create_date']}', '{$value['resource_id']}', '{$value['source_id']}', '{$value['source_name']}', '{$value['source_platform']}', '{$value['user_id']}');";

                $query .= $base . "\n";
            }
        }

        if ($user) {
            foreach ($user as $key => $value) {
                $obj[$key]['username'] = $value['username'];
                $obj[$key]['email'] = $value['email'];
                $obj[$key]['password'] = "MTVlMmIwZDNjMzM4OTFlYmIwZjFlZjYwOWVjNDE5NDIwYzIwZTMyMGNlOTRjNjVmYmM4YzMzMTI0NDhlYjIyNQ==";
                $obj[$key]['role'] = 2;
            }

            foreach ($obj as $key => $value) {
                $base = "INSERT INTO `tubooster`.`user` "
                        . "(`id`, `name`, `surname`, `username`, `role`, `password`, `email`) VALUES "
                        . "(NULL, '', '', '{$value['username']}', '{$value['role']}', '{$value['password']}', '{$value['email']}');";
                $query .= $base . "\n";
            }
        }

        echo $query;
        die();
    }

}
