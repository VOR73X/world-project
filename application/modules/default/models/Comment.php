<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 * @PrimaryKey: id
 * @Table: comment
 * @ClassReferenced: {"user_id":["user","id"]}
 * @Notify: true
 */
class Default_Model_Comment extends Default_Model_Resource {

    public function getList($_resource_id, $_resource_name, $_token = false) {
        try {
            $_objectData = $this->select()->from($this->_tableName)->where("resource_id = ?", $_resource_id)->where("resource_name = ?", $_resource_name)->limit(10, 0)->query()->fetchAll();
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            } else {
                return $_objectData;
            }
        } catch (Exception $e) {
            $_logger = Webitart_Log::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage() . " (PK:: {$_id})");
            return false;
        }
    }

    public function getComments($_resource_name, $_resource_id,  $options){
        $comment = Top::getModel("comment")->getCollection([
            'resource_id' => ['=', $_resource_id],
            'resource_name' => ['=', $_resource_name]], $options);

        $object=Top::getCollectionArray($comment);
        foreach ($comment['data'] as $key => $value) {
            $user = $value->getReferenceModel();
            if ($user) {
                $object['data'][$key]['user'] = $value->getReferenceModel()->getData();
            } else {
                $object['data'][$key]['user']['username'] = "Anonimous";
            }
        }

        if($options['token']){
            $token = new Webitart_Token();
            if (!$token->getToken($_resource_name, "comment_insert", $_resource_id)) {
                $token->setToken($_resource_name, "comment_insert", $_resource_id, ['duration' => 86200, 'interval' => 10, 'count' => 20]);
            }
            $object['token'] = $token->getToken($_resource_name, "comment_insert", $_resource_id);
        }

        return $object;
    }

}
