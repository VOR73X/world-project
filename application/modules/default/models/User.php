<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 * @PrimaryKey: id
 * @Table: user
 */
class Default_Model_User extends Webitart_Abstract {

    public function getData(){
        $temp = parent::getData();
        unset($temp['password']);
        Top::reatriveFile($temp['image']);
        return $temp;
    }

    public function login($_identifier, $_password, $force = false) {
        $_password = hash("sha256", $_password);
        try {
            if (!$force) {
                $_objectData = $this->select()->where("username = ?", $_identifier)->where("password = ?", $_password)->query()->fetch();
            } else {
                $_objectData = $this->select()->where("username = ?", $_identifier)->query()->fetch();
            }

            if (!$_objectData) {
                return false;
            } else {
                $object = get_called_class();
                return new $object($_objectData);
            }
        } catch (Exception $e) {
            $_logger = Webitart_Log::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage() . " (PK:: {$_id})");
            return false;
        }
    }
}
