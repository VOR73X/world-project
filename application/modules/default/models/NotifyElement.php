<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 * @PrimaryKey: id
 * @Table: notify_element
 * @ClassReferenced: {"user_id":["user","id"]}
 */
class Default_Model_NotifyElement extends Webitart_Abstract {

	public function getData(){
		$temp = parent::getData();
		
		$user = $this->getReferenceModel('user_id');
		$temp['user']=$user->getData();
		return $temp;
	}

}
