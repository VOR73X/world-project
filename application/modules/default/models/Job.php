<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Jobs Actions
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class Default_Model_Job {

	public function AzioneJob($parameters){
        $user = Top::getModel("user")->load($parameters['user_id']);

        switch ($parameters['azione']) {
            case "+":
                $user->setXp($user->getXp() + $parameters['value']);
                break;
            case "-":
                $user->setXp($user->getXp() - $parameters['value']);
                break;
            case "*":
                $user->setXp($user->getXp() * $parameters['value']);
                break;
            case "/":
                $user->setXp($user->getXp() / $parameters['value']);
                break;
        }

        $user->save();
	}

}