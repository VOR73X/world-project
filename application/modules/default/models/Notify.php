<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 * @PrimaryKey: id
 * @Table: notify
 * @ClassReferenced: {"user_id":["user","id"]}
 */
class Default_Model_Notify extends Webitart_Abstract {

	public function getData(){
		$temp = parent::getData();
		$resource = Top::getModel($this->getResource_name())->load($this->getResource_id());
		if($resource){
			$temp['resource'] = $resource->getData();
		}

		$notifyElement = Top::getModel('notifyelement')->getCollection(['group_id'=>['=',$this->getId()]],['order'=>'id DESC']);
		$temp['party']= Top::getCollectionArray($notifyElement);
		$temp['text'] = $this->translate($temp);
		return $temp;
	}

	/**
	* Translate
	* @notify: Source Data model
	*/
	protected function translate($notify){
		switch($notify['type']){
			case 'view':
				return $this->template($notify,
					"<b>{name}</b> ha guardato il tuo <b>{resource}</b>: \"<i>{subject}</i>\"",
					"<b>{name}</b> hanno guardato il tuo <b>{resource}</b>: \"<i>{subject</i>\"}"
				);
			case 'likes':
				return $this->template($notify,
					"<b>{name}</b> ha messo mi Piace al tuo <b>{resource}</b>: \"<i>{subject}</i>\"",
					"<b>{name}</b> hanno messo mi Piace al tuo <b>{resource}</b>: \"<i>{subject</i>\"}"
				);
			case 'dislikes':
				return $this->template($notify,
					"<b>{name}</b> ha messo non mi Piace al tuo <b>{resource}</b>: \"<i>{subject}</i>\"",
					"<b>{name}</b> hanno messo non mi Piace al tuo <b>{resource}</b>: \"<i>{subject</i>\"}"
				);
			case 'comment':
				return $this->template($notify,
					"<b>{name}</b> ha Commentato il tuo <b>{resource}</b>: \"<i>{subject}</i>\"",
					"<b>{name}</b> hanno Commentato il tuo <b>{resource}</b>: \"<i>{subject</i>\"}"
				);	
			default:
				return "Generic Notify";
		}
		return "";
	}

	/**
	* Template
	* @tpl_single: {name} hanno guardato il tuo {resource} {subject}
	* @tpl_plural: {name} ha guardato il tuo {resource} {subject}
	*/
	protected function template($data, $tpl_single, $tpl_plural){		
		$tmp = "";
		$type = $data['type'];

		$keyRules = $type . "Rules";
		if(!$this->{$keyRules}()){
			$keyRules = "defaultRules";
		}

		$entity = $this->{$keyRules}();

		if(count($data['party']['data'])>1){
			$tmp = str_replace("{name}", $this->pluralConcat($data['party'], $entity['name']),$tpl_plural);
		}else{
			$tmp = str_replace("{name}", Top::getKey($data['party']['data'][0],$entity['name']),$tpl_single);
		}

		if(strpos($tmp, "{subject}")!==false){
			$tmp = str_replace("{subject}", ucfirst(Top::getKey($data,$entity['subject'])),$tmp);
		}
		if(strpos($tmp, "{resource}")!==false){
			$tmp = str_replace("{resource}", ucfirst(Top::getKey($data,$entity['resource'])),$tmp);
		}
		return $tmp;
	}

	/**
	* PluralConcat
	*/
	protected function pluralConcat($array, $entity, $max = 3){
		$str="";
		foreach ($array['data'] as $key => $value) {
			if($key<$max){
				$s = false;
				if($key>0 && $key<$max-1){
					$str.=", ";
				}elseif($key>0){
					$str.=" e ";
					if($array['paginator']['totalItemCount'] - $max >= 1){
						$s = true;
					}
				}
				if(!$s){
					$str.=Top::getKey($value,$entity);
				}else{
					$str.= "altre " . ($array['paginator']['totalItemCount'] -($max-1)) . " persone";
				}
			}
		}
		return $str;
	}

	protected function defaultRules(){
		return [
			'name'=>'user.username',
			'subject'=>'resource.title',
			'resource'=>'resource_name'	
		];
	}

	protected function commentRules(){
		$resource_name = parent::getData()['resource_name'];
		switch ($resource_name) {
			case 'comment':
				return [
					'name'=>'user.username',
					'subject'=>'resource.content',
					'resource'=>'commento'	
				];
				break;
			default:
				return [
					'name'=>'user.username',
					'subject'=>'resource.title',
					'resource'=>'resource_name'	
				];
				break;
		}
	}

}
