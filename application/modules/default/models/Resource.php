<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 * @PrimaryKey: id
 * @Table: view
 * @ClassReferenced: {"user_id":["user","id"]}
 */
class Default_Model_Resource extends Webitart_Abstract {

    public function load_resource($_resource_id, $_resource_name, $_user_id = null) {
        if (Top::$profile->session['id'] && $_user_id == null) {
            $_user_id = Top::$profile->session['id'];
        }
        try {
            $_objectData = $this
                    ->select()
                    ->where("resource_id = ?", $_resource_id)
                    ->where("resource_name = ?", $_resource_name)
                    ->where("user_id = ?", $_user_id);


            Top::print_d($_objectData->__toString());

            $_objectData = $_objectData
                    ->query()
                    ->fetch();

            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            } else {
                $object = get_called_class();
                return new $object($_objectData);
            }
        } catch (Exception $e) {
            $_logger = Webitart_Log::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage() . " (PK:: {$_id})");
            return false;
        }
    }

    public function getCount($_resource_name, $_resource_id) {
        try {
            $_objectData = $this->select();

            if ($this->_tableName == "view") {
                $_objectData = $_objectData->from([$this->_tableName], ["count_view" => 'COUNT(*)', 'count_tick' => 'SUM(tick)']);
            } else {
                $_objectData = $_objectData->from([$this->_tableName], ["count_view" => 'COUNT(*)']);
            }

            Top::print_d($_objectData->__toString());

            $_objectData = $_objectData->where("resource_id = ?", $_resource_id)
                    ->where("resource_name = ?", $_resource_name)
                    ->query()
                    ->fetch();


            if (!$_objectData['count_tick'])
                $_objectData['count_tick'] = 0;
            if (!$_objectData) {
                throw new Exception("Object not found", 501);
            } else {
                return $_objectData['count_view'] + $_objectData['count_tick'];
            }
        } catch (Exception $e) {
            $_logger = Webitart_Log::getInstance();
            $_logger->log(get_called_class(), Zend_Log::ERR, $e->getMessage() . " (PK:: {$_id})");
            return false;
        }
    }

    public function checkResource($_resource_name,$_resource_id) {
        if (Top::$profile->session['id']) {
            $user_id = Top::$profile->session['id'];
        } else {
            $user_id = 0;
        }

        $datecreate = date('Y-m-d H:i:s');
        $element = $this->load_resource($_resource_id,$_resource_name, $user_id);

        if (!$element) {
            $this->setResource_id($_resource_id);
            $this->setResource_name($_resource_name);
            $this->setIp($_SERVER['REMOTE_ADDR']);
            $this->setCreate_date($datecreate);
            $this->setModified_date($datecreate);
            $this->setUser_id($user_id);
            $this->save();
        } else {
            $element->setModified_date($datecreate);
            $element->setTick($element->getTick() + 1);
            $element->save();
            return true;
        }

        return false;
    }

    public function statusResource($_resource_name,$_resource_id){
        if (Top::$profile->session['id']) {
            $user_id = Top::$profile->session['id'];

            $datecreate = date('Y-m-d H:i:s');
            $element = $this->load_resource($_resource_id,$_resource_name, $user_id);

            if ($element) {
                return true;
            } else {
                return false;
            }
        } 
        else
        {
            return false;
        }
    }
}
