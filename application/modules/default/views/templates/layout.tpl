{strip}
    <!DOCTYPE html>
    <html>
        <head>
            {include file="head.tpl"}
        </head>
        <body>
            <header>
                {include file="header.tpl"}
            </header>
            {$this->layout()->content}
            <footer>
                {include file="footer.tpl"}
            </footer>
        </body>
    </html>
{/strip}