{strip}
    <ul class="pagination text-center" role="navigation" aria-label="Pagination">
        {*Previous*}    
        {if !$data.previous}
            <li class="pagination-previous disabled">
                Previous
            </li>
        {else}
            <li class="pagination-previous">
                <a href="{$baseurl}?page={$data.previous}" aria-label="Previous page">Previous</a>
            </li>
        {/if}

        {* Page in Range *}
        {foreach from=$data.pagesInRange item=$pagination}
            {if $data.current==$pagination}
                <li class="current">
                    {$pagination}
                </li>
            {else}
                <li class="fg-primary-hover">
                    <a href="{$baseurl}?page={$pagination}">{$pagination}</a>
                </li>
            {/if}
        {/foreach}

        {*Next*}
        {if !$data.next}
            <li class="pagination-next disabled">
                Next
            </li>
        {else}
            <li class="pagination-next">
                <a href="{$baseurl}?page={$data.next}" aria-label="Next page">Next</a>
            </li>
        {/if}
    </ul>
{/strip}