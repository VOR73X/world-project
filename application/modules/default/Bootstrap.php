<?php

class Default_Bootstrap extends Zend_Application_Module_Bootstrap {

    protected function _initCache(){
        if (!file_exists(Top::getAbsoluteUrl() . '/var')) {
            mkdir(Top::getAbsoluteUrl() . '/var', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/cache')) {
            mkdir(Top::getAbsoluteUrl() . '/var/cache', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/db')) {
            mkdir(Top::getAbsoluteUrl() . '/var/db', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/log')) {
            mkdir(Top::getAbsoluteUrl() . '/var/log', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/media')) {
            mkdir(Top::getAbsoluteUrl() . '/var/media', 0777, true);
        }

        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        if (!file_exists(Top::getAbsoluteUrl() . '/media')) {
            mkdir(Top::getAbsoluteUrl() . '/media', 0777, true);
        }
        $frontendOptions = array(
           'lifetime' => 86400,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => Top::getAbsoluteUrl() . 'var/cache'
        );
         
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions,$backendOptions);
         
        $manager = new Zend_Cache_Manager;
        $manager->setCache('generic', $cache);
         
        Zend_Registry::set('cacheGeneric', $manager);
    }
    
    protected  function _initCacheDB(){
        $frontendOptions = array(
           'lifetime' => 86400,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => Top::getAbsoluteUrl() . 'var/db'
        );
         
        $dbCache = Zend_Cache::factory('Core', 'File', $frontendOptions,$backendOptions);
         
        $manager = new Zend_Cache_Manager;
        $manager->setCache('database', $dbCache);
         
        Zend_Registry::set('cacheDatabase', $manager);
    }

    protected  function _initCacheGeneric(){
        $frontendOptions = array(
           'lifetime' => 600,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => Top::getAbsoluteUrl() . 'var/cache'
        );
         
        $dbCache = Zend_Cache::factory('Core', 'File', $frontendOptions,$backendOptions);
         
        $manager = new Zend_Cache_Manager;
        $manager->setCache('generic', $dbCache);
         
        Zend_Registry::set('cacheGeneric', $manager);
    }

    protected  function _initCacheHome(){
        $frontendOptions = array(
           'lifetime' => 600,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => Top::getAbsoluteUrl() . 'var/db'
        );
         
        $dbCache = Zend_Cache::factory('Core', 'File', $frontendOptions,$backendOptions);
         
        $manager = new Zend_Cache_Manager;
        $manager->setCache('database', $dbCache);
         
        Zend_Registry::set('cacheHome', $manager);
    }

    protected function _initSession(){
        Top::$token = new Zend_Session_Namespace('token');
        Top::$token->setExpirationSeconds(86400);

        Top::$jsEngine = new Zend_Session_Namespace('jsEngine');
        Top::$jsEngine->setExpirationSeconds(86400);

        if (!isset(Top::$jsEngine->object)) {
            Top::$jsEngine->object = [];
        }

        Top::$profile = new Zend_Session_Namespace('profile');
        Top::$profile->setExpirationSeconds(86400);
        if (!isset(Top::$profile->session['roleString'])) {
            Top::$profile->session['roleString'] = "guest";
        }
        Top::$profile->session['timeServer'] = time();
        Top::$jsEngine->object['user'] = Top::$profile->session;

        Top::$world = new Zend_Session_Namespace("world");
        Top::$world->setExpirationSeconds(86400);
    }

    protected function _initBase() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        Top::$jsEngine->object['env']['getbaseurl'] = Top::getBaseUrl();
        Top::$jsEngine->object['env']['getEnv'] = Top::getEnv();
        if($config->website->log->status==1){
            Top::$jsEngine->object['env']['log']['status'] = true;
        } else {
            Top::$jsEngine->object['env']['log']['status'] = false;
        }
        Top::$jsEngine->object['env']['log']['level'] = $config->website->log->level;

    }

    protected function _initLang() {
        //$GLOBALS['lang']['it']['static']['header'] = [
        //    'logout' => 'TEST2'
        //];
    }

    protected function _initAcl() {
        Top::$acl = new Webitart_Acl();

        $admin = new Zend_Acl_Role('admin');
        $moderatore = new Zend_Acl_Role('moderator');
        $partner = new Zend_Acl_Role('partner');
        $utente = new Zend_Acl_Role('user');
        $guest = new Zend_Acl_Role('guest');

        Top::$acl->addRole($guest);
        Top::$acl->addRole($utente, $guest);
        Top::$acl->addRole($partner, $utente);
        Top::$acl->addRole($moderatore, $partner);
        Top::$acl->addRole($admin, $moderatore);

        Top::$acl->add(new Zend_Acl_Resource("login"));
        Top::$acl->add(new Zend_Acl_Resource("menu"));
        Top::$acl->add(new Zend_Acl_Resource("profile"));
        Top::$acl->add(new Zend_Acl_Resource("comments"));
        Top::$acl->add(new Zend_Acl_Resource("rate"));
        Top::$acl->add(new Zend_Acl_Resource("artworks"));

        //Generic
        Top::$acl->deny("guest", "profile");
        Top::$acl->allow("user", "profile");

        //Login
        Top::$acl->allow("guest", "login");
        Top::$acl->deny("user", "login");
        
        //Comments
        Top::$acl->allow("guest", "comments");
        Top::$acl->allow("user", "comments");
        
        //Rate
        Top::$acl->allow("guest", "rate");
        Top::$acl->allow("user", "rate");
        
        //Artworks
        Top::$acl->allow("guest", "artworks");
        Top::$acl->allow("user", "artworks");
        
        //Menu
        Top::$acl->allow("user", "menu", 'l1');
        Top::$acl->allow("partner", "menu", 'l2');
        
        


//        Top::$acl->addResources(["login", 'registrazione'], "", ['page']);
//        print_r(Top::$profile->session);
//        echo Top::$acl->isAllowed(Top::$profile->session['roleString'], "login") ? 'allowed' : 'denied';
//        echo "\n";
//
//        
//        echo Top::$acl->isAllowedMe("login") ? 'allowed' : 'denied';
//        echo "\n";
//
//        echo Top::$profile->session['roleString'];
//        die();
//        Top::$acl->deny("user", new Webitart_AclModel("user", "read", 'db'), ['name', 'surname', 'eta']);
//        Top::$acl->allow("user", new Webitart_AclModel("user", "read_all", 'db'), ['name','user_nickname']);
//        
//        //video
////        Top::$acl->allow("utente", new Webitart_AclModel("video", "read_all", 'db'), ['video_name','user_nickname']);
//        Top::$acl->allow("user", new Webitart_AclModel("video", "read_all", 'db'), ['video_title','video_name','video_user_id']);
//        
//        
//        Top::$acl->allow("user", new Webitart_AclModel("video", "read_all", 'db'), ['video_title','video_name','video_user_id']);
//
//        Top::$acl->allow("moderator");
    }
}
    