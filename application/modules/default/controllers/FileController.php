<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnswersController
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class FileController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function uploadAction() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $datecreate = date('Y-m-d H:i:s');

        $adapter = new Zend_File_Transfer_Adapter_Http();

        if($adapter->isValid()){
            $generic = explode(".", $adapter->getFileName(null,false));
            $format = strtolower($generic[count($generic)-1]);
            $name = str_replace(".{$format}", "", $adapter->getFileName(null,false));
            $size = $adapter->getFileSize();

            $file = Top::getModel("file");
            $file->setName($name);
            $file->setFormat($format);
            $file->setSize($size);
            $file->setCreate_date($datecreate);
            $file->setCollection($_POST['collection']);

            $file->setUser_id(Top::$profile->session['id']);

            $file->save();

            $name_define = hash("sha256", $file->getId());

            $file->setHash($name_define);
            $file->save();

            $adapter->addFilter('Rename', array('target' => Top::getAbsoluteUrl() . "media/{$name_define}." . $format , 'overwrite' => true));
             
            $adapter->receive();
             
            $obj = $file->getData();
            Top::returnJson($obj);
        }

    }
}
