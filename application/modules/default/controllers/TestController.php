<?php

class TestController extends Zend_Controller_Action {

    public function init() {

    }

    public function indexAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        echo "ok";
    }


    public function scanAction()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json; charset='utf-8'");
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();


        $req = [
            'app' => [
                'id' => 1
            ],
            'user' => [
                'id' => 1
            ],
            'navigator' => [
                'latitude' =>  $_POST['latitude'],
                'longitude' =>  $_POST['longitude'],
                'accuracy' =>  $_POST['accuracy'],
                'altitude' =>  $_POST['altitude'],
                'altitudeAccuracy' =>  $_POST['altitudeAccuracy'],
                'heading' =>  $_POST['heading'],
                'speed' =>  $_POST['speed']
            ]

        ];

        print_r(json_encode($req));

    }

    public function worldAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        echo (json_encode(Top::$world->system['env']));
    }

    public function noteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $event = Top::getModel("event");
        $event->setup();

        $script = Top::getModel("script");
        $script->setup();

        $note = Top::getModel("note");
        $note->setup();

        $scene = Top::getModel("scene");
        $scene->setup();

        $session = Top::getModel("session");
        $session->setup();
    }

    public function resetAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        Top::$world->env = [];
    }

}