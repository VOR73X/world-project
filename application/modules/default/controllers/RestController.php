<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RestController
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class RestController extends Zend_Controller_Action {

    public function instanceAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $obj = [];

        $user = Top::getModel("user")->login($_POST['username'], $_POST['password']);
        if($user){
            Top::$profile->session = $user->getData();
            $token = new Webitart_Token();
            $token->setToken("rest", "", "", [
                'count' => 15000
            ]);

            $obj['token'] = $token->getToken("rest", "", "");
        }

        Top::returnJson($obj);
    }

    public function loadAction() {
        header('Access-Control-Allow-Origin: *');
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $obj = [];

        $tab = $this->getParam("tab");
        $pk = $this->getParam("pk");
        $ref = $this->getParam("ref");
        $sts = $this->getParam("sts");
        
        $elemento = Top::getModel($tab)->load($pk);
        if($elemento){
            $obj['data'] = $elemento->getData();
            if($sts){
                $obj['data']['statistics'] = $elemento->getStatistics();
            }

            if($ref){
                $model = $elemento->getReferenceModel();
                if ($model) {
                    $obj['data'][$key]['user'] = $model->getData();
                }
            }
            $obj['status'] = true;
        } else {
            $obj['status'] = false;
        }

        Top::returnJson($obj);
    }

    public function listAction() {
        header('Access-Control-Allow-Origin: *');
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $obj = [];

        $tab = $this->getParam("tab");
        $page = $this->getParam("page");
        $limit = $this->getParam("limit");
        $order = $this->getParam("order");
        $ref = $this->getParam("ref");
        $sts = $this->getParam("sts");

        if ($page < 1 || !isset($page)) {
            $page = 1;
        }

        if (!isset($limit)) {
            $limit = 9;
        }

        $elemento = Top::getModel($tab);
        $where = [];
        if ($this->getParam("filter")) {
            $filter = $this->ConditionExtract($this->getParam("filter"))[1];
            $elementi = explode(":|:", $this->ConditionConvert($this->getParam("filter")));
            foreach ($elementi as $key => $value) {
                $operand = $this->OperatorExtract($value)[1][0];
                if (!$operand)
                    $operand = "=";

                $explode = explode(":{$operand}:", $value);
                if (count($explode) > 0) {
                    if($operand=="like"){
                        $explode[1] = str_replace("*", "%", $explode[1]);
                    }
                    $where[$explode[0]] = [$operand, $explode[1], $filter[0]];
                }
            }
        }
        
        $object = $elemento->getCollection($where, [
            'target' => $tab,
            'page' => $page,
            'resultPage' => $limit,
            'order' => $order
        ]);

        $obj = Top::getCollectionArray($object);

        foreach ($object['data'] as $key => $value) {
            if ($sts) {
                $obj['data'][$key]['statistics'] = $value->getStatistics();
            }

            if ($ref) {
                foreach (explode(":",$ref) as $tabRef) {
                    $model = $value->getReferenceModel($tabRef);
                    if ($model) {
                        $obj['data'][$key][$tabRef."__r"] = $model->getData();
                    }
                }
                
            }
        }

        $obj['status'] = true;

        Top::returnJson($obj);
    }

    public function OperatorExtract($str) {
        $re = "/\\:([\\<\\>\\=\\!]{1,2}|like)\\:/";

        preg_match_all($re, $str, $matches);
        return $matches;
    }

    public function ConditionExtract($str) {
        $re = "/\\:(and|or)\\:/";

        preg_match_all($re, $str, $matches);
        return $matches;
    }

    public function ConditionConvert($str) {
        $re = "/\\:(and|or)\\:/";
        $subst = ":|:";

        return preg_replace($re, $subst, $str);
    }

    public function saveAction() {
        header('Access-Control-Allow-Origin: *');
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $obj = [];

        $pk = $this->getParam("pk");
        $tab = $this->getParam("tab");
        $ref = $this->getParam("ref");
        $sts = $this->getParam("sts");

        if ($pk) {
            $elemento = Top::getModel($tab)->load($pk);
        } else {
            $elemento = Top::getModel($tab);
        }
        if($elemento){
            $elemento->setDataToArray($_POST);
            $elemento->save();
            
            $obj['data'] = $elemento->getData();
            if($sts){
                $obj['data']['statistics'] = $elemento->getStatistics();
            }

            if($ref){
                foreach (explode(":",$ref) as $tabRef) {
                    $model = $elemento->getReferenceModel($tabRef);
                    if ($model) {
                        $obj['data'][$tabRef."__r"] = $model->getData();
                    }
                }
            }
            
            $obj['status'] = true;
        }else{
            $obj['status'] = false;
        }

        Top::returnJson($obj);
    }

    public function deleteAction() {
        header('Access-Control-Allow-Origin: *');
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $obj = [];

        $pk = $this->getParam("pk");
        $tab = $this->getParam("tab");
        $elemento = Top::getModel($tab)->load($pk);
        if ($elemento) {
            $elemento->delete();
            
            $obj['status'] = true;
        } else {
            $obj['status'] = false;
        }

        Top::returnJson($obj);
    }

    public function formAction(){
        header('Access-Control-Allow-Origin: *');

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $obj = [];

        $tab = $this->getParam("tab");        
        $pk = $this->getParam("pk");
        if ($this->getParam('preset')){
            $preset = $this->getParam('preset');
            $preset = str_replace("::", "/", $preset);
        }

        $elemento = Top::getModel($tab)->load($pk);
        if(!$elemento){        
            $elemento = Top::getModel($tab);
        }

        $model = $elemento->getData();

        
        $form = new Webitart_Form([
            'method'=>"post",
            'class'=>"wbt-form",
            'action' => Top::getbaseurl() . "rest/list/token/1/tab/user",
            'presetJsonToFile'=> $preset
        ]);

        $form->initModel($model);
        $obj = $form->getData();

        $obj['reference'] = $elemento->getReferenceList();

        header("Content-Type: application/json; charset='utf-8'");
        echo json_encode($obj);
    }
}
