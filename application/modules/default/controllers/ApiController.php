<?php

class ApiController extends Zend_Controller_Action {

	private $scriptStatus = [];

	public function init() {
		$token = new Webitart_Token();

		if(!$token->checkToken("rest","","",$_POST['token'])){
			//Top::returnJson(['status'=>false]);
			//die();
		}
	}

	public function indexAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
	}

	public function enterAction(){
		header('Access-Control-Allow-Origin: *');
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$object = ['status'=>false];

		$scene = Top::getModel("scene")->load($_POST['scene']);
		if($scene){
			$sceneObject = $scene->getData();
			$scriptScene = $scene->getReferenceModel('script');
			if($scriptScene){
				$sceneObject['script'] = $scriptScene->getData();
			}

			Top::$world->system = [];
			Top::$world->system['game']['scene'] = $sceneObject;

			$session = Top::getModel("session")->loadByAttribute(['scene'=>Top::$world->system['game']['scene']['id'],'user'=>Top::$profile->session['id']]);
			if($_POST['reset']){
				if($session){
					$session->delete();
					unset($session);
				}
				if($_POST['reset']>=2){
					$sessionUrl = Top::getAbsoluteUrl() . "sessions/" . $sceneObject['name'] . ".json";
					unlink($sessionUrl);
				}
			}
			if($session){
				$object=json_decode($session->getObject(),true);
			}else{				
				$events = Top::getModel("event")->getCollection(['scene'=>["=",$scene->getId()]],['resultPage'=>99999]);
				$object['events'] = $this->normalized(Top::getCollectionArray($events, false, ['script']));
				foreach ($object['events'] as $key => $value) {	
					//Valida lo script pulendo da eventuali incorretezze
					$scriptString = $value['script']['script'];
					$scriptString = str_replace(">=", "&gt;=", $scriptString);
					$scriptString = str_replace("<=", "&lt;=", $scriptString);
					$scriptString = str_replace(">>", "&gt;", $scriptString);
					$scriptString = str_replace("<<", "&lt;", $scriptString);

					$object['events'][$key]['script']['script'] = $scriptString;

				}

				$script = Top::getModel("script")->getCollection(['scene'=>["=",$scene->getId()]],['resultPage'=>99999]);
				$object['script'] = $this->normalized(Top::getCollectionArray($script));
				foreach ($object['script'] as $key => $value) {	
					//Valida lo script pulendo da eventuali incorretezze
					$scriptString = $value['script'];
					$scriptString = str_replace(">=", "&gt;=", $scriptString);
					$scriptString = str_replace("<=", "&lt;=", $scriptString);
					$scriptString = str_replace(">>", "&gt;", $scriptString);
					$scriptString = str_replace("<<", "&lt;", $scriptString);

					$object['script'][$key]['script'] = $scriptString;

				}
				$object['scene'] = $sceneObject;
			}
			$object['status'] = true;
			Top::$world->env = $object;
			Top::$world->private = [];
			Top::$world->temp = [];
			Top::$world->scriptStatus = [];
			Top::$world->responde = [];
			Top::$multiplayer = [];
		}
		Top::returnJson($object);
	}

	public function retrieveAction(){
		header('Access-Control-Allow-Origin: *');
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		Top::returnJson(Top::$world->env);
	}

	public function scanAction(){
		//https://developers.google.com/maps/articles/phpsqlsearch_v3?csw=1
		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json; charset='utf-8'");

		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		if(!Top::$world->private['cmd'])Top::$world->private['cmd'] = [];
		if(!Top::$world->private['gui'])Top::$world->private['gui'] = [];
		Top::$world->private['actions'] = [];
		Top::$world->private['errors'] = [];

		$sessionUrl = Top::getAbsoluteUrl() . "sessions/" . Top::$world->env['scene']['name'] . ".json";
		if (!file_exists(Top::getAbsoluteUrl() . "sessions")) {
		    mkdir(Top::getAbsoluteUrl() . "sessions", 0777, true);
		}
		if(!file_exists($sessionUrl)){
			$tempObj = [];
			RpgEngine_Helper::saveFile($sessionUrl, json_encode($tempObj));
		}

		Top::$multiplayer = json_decode(RpgEngine_Helper::openFile($sessionUrl),true);


		//Preparazione Responde
		$responde = [
			'app' => [
				'id' => 1
			],
			'user' => [
				'id' => 1
			],
			'navigator' => [
				'latitude' =>  $_POST['latitude'],
				'longitude' =>  $_POST['longitude'],
				'accuracy' =>  $_POST['accuracy'],
				'altitude' =>  $_POST['altitude'],
				'altitudeAccuracy' =>  $_POST['altitudeAccuracy'],
				'heading' =>  $_POST['heading'],
				'speed' =>  $_POST['speed']
			]
		];

		//Aggiornamento navigator nel Ambiente
		Top::$world->env['navigator'] = $responde['navigator'];

		//Aggiornamento local to Multiplayer
		Top::$world->env = array_replace_recursive(Top::$world->env,Top::$multiplayer);

		//Controllo se uno script è associato alla scena, se associato lo esegue
		if(Top::$world->env['scene']['script']){
			$resultScene = $this->codeExe(Top::$world->env['scene'], Top::$world->env['scene']['script']['script']);
			foreach ($resultScene as $key => $value) {
				$responde[$key]=$value;
			}
		}

		//Controllo sullo stato dello script della scena se Completed controlla gli eventi
		if($responde['status']!="pending"){
			$events = Top::$world->env['events'];
			foreach ($events as $key => $event) {
				if($event['enable']){
					$distance = RpgEngine_Helper::distance($responde['navigator']['latitude'],$responde['navigator']['longitude'], $event['latitude'], $event['longitude']);
					if($distance<20){
						if($event['script']){
							$resultEvents = $this->codeExe($event, $event['script']['script']);
							foreach ($resultEvents as $key => $value) {
								$responde[$key]=$value;
							}
							if($resultEvents['status']=="pending"){
								break;
							}
						}
					}
				}
			}
		}

		//Viene controllata la visibilità degli eventi
		$events = Top::$world->env['events'];
		Top::print_d($events);
		foreach ($events as $key => $event) {
			if($event['enable']){
				$distance = RpgEngine_Helper::distance($responde['navigator']['latitude'],$responde['navigator']['longitude'], $event['latitude'], $event['longitude']);
				$direction = RpgEngine_Helper::direction($responde['navigator']['latitude'],$responde['navigator']['longitude'], $event['latitude'], $event['longitude']);

				$eventData = $event;
				$eventData['distance'] = $distance;


				$eventData['direction'] = $direction;
				$eventData['directionString'] = RpgEngine_Helper::directionString($direction);

				if($event['visible']){
					$responde['events'][$event['name']] = $eventData;	
				}
			}
			if($event['multiplayer']){
				Top::$multiplayer['events'][$key] = $event;
			}
		}		
				
		function cmp($a,$b){
			return $a['distance']>$b['distance'];
		}

		usort($responde['events'], "cmp");

		//in caso di scan con post cmd ricontrollo la validita di tutti cmd
		//altrimenti stampo le response
		
		// if($_POST['cmd'] && $_POST['cmd'] != ''){
		// 	unset($_POST['cmd']);
		// 	$this->scanAction();
		// } else {

		//#DEBUG: Respnde Variabili di debug, dati da non inviare in PROD
		$responde['var'] = Top::$world->scriptStatus;
		$responde['private'] = Top::$world->private;
		$responde['temp'] = Top::$world->temp;
		$responde['multiplayer'] = Top::$multiplayer;
		$responde['debug'] = Top::$debug;
		$responde['debugTime'] = Top::$debugTime;
		

		//Se la responde risulta completed viene svutato lo stato delle esecuzioni
		//dello script
		if($responde['status']!="pending"){
			Top::$world->scriptStatus = [];
			Top::$world->responde = [];
		}

		//merge Multiplayer to Singleplayer
		$mergeEnvs = array_replace_recursive(Top::$world->env,Top::$multiplayer);


		//Save multiplayer session
		RpgEngine_Helper::saveFile($sessionUrl, json_encode(Top::$multiplayer));

		//Completa la responde con gli oggetti aggiuntivi
		$responde['actions'] = Top::$world->private['actions'];
		$responde['gui'] = Top::$world->private['gui'];
		$responde['cmd'] = Top::$world->private['cmd'];
		$responde['errors'] = Top::$world->private['errors'];
		$responde["environment"]=$mergeEnvs;
		$responde['game'] = Top::$world->system['game'];

		Top::$world->responde = $responde;

		//Se la responde risulta completed viene svutato lo stato delle esecuzioni
		//dello script
		if($responde['status']!="pending"){
			Top::$world->private['cmd']=[];
		}
		echo json_encode($responde);

		// }


	}

	public function codeExe($object,$code){
		$nameObject = $object['name'];

		//controlla stato dell script, se eseguito o no
		if(!Top::$world->scriptStatus[$nameObject]){
			//Viene controllato se lato Client è stato inviato un Input
			if(Top::$world->temp[$nameObject]['input']){
				foreach (Top::$world->temp[$nameObject]['input'] as $key => $value) {
					if(!$value){
						Top::$world->temp[$nameObject]['input'][$key] = $_POST['pending'];
					}
				}
			}

			//viene controllato se lato Client è stato inviato un CMD
			if($_POST['cmd'] && $_POST['cmd'] != ''){
				Top::$world->temp[$nameObject]['req_cmd'] = $_POST['cmd']; 
			}

			//viene impostato l'oggetto me, il quale fa riferimento all'oggetto chiamante
			Top::$world->temp[$nameObject]['me']=$nameObject;

			//Instazia ed esegue il codice
			$xml = simplexml_load_string($code) or die("Error: Cannot create object");
			$exe = new RpgEngine_Code($xml);
			$exeObject = [
				'env'=>Top::$world->env,
				'temp'=>Top::$world->temp[$nameObject],
				'private'=>Top::$world->private,
				'multiplayer'=>Top::$multiplayer
			];
			$returnCode = $exe->encode($exeObject);

			//Viene aggioranto l'ambiente private
			Top::$world->private =  $returnCode['private'];
			Top::print_d($returnCode['status']);

			//Controllo della risposta dello script
			switch ($returnCode['status']) {
				case 'pending':
					$responde['environmentPending'] = array_replace_recursive($returnCode['env'],$returnCode['multiplayer']);
					Top::$world->temp[$nameObject] = $returnCode['temp'];
					break;
				case 'complete':
					Top::$world->env = $returnCode['env']; 
					Top::$multiplayer = $returnCode['multiplayer']; 

					//Blocca lo script
					Top::$world->scriptStatus[$nameObject]=true;
					Top::$world->temp[$nameObject]=[];
					
					//Salva la scena se la property save è impostata a True
					if($object['save']){
						$env = Top::$world->env;
						unset($env['actions']);

						$saveSession = Top::getModel("session")->loadByAttribute([
							'scene'=>Top::$world->system['game']['scene']['id'],
							'user'=>Top::$profile->session['id']
						]);

						//Se non esiste la session viene creata una nuova 
						if(!$saveSession){
							$saveSession = Top::getModel("session");
							$saveSession->setUser(Top::$profile->session['id']);
							$saveSession->setScene(Top::$world->system['game']['scene']['id']);
						}

						$saveSession->setObject(json_encode($env));
						$saveSession->save();

					}
					break;
			}

			//Aggiorna la responde
			$responde['status']=$returnCode['status'];
		}else{
			//script già eseguito
			$responde['status']="complete";
		}

		return $responde;
		
	}

	public function normalized($data){
		foreach ($data['data'] as $key => $value) {
			$obj[$value['name']] = $value;
		}

		return $obj;
	}
}