<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnswersController
 *
 * @author Jean Paul Sanchez <j.sanchez@webitart.com>
 */
class MediaController extends Zend_Controller_Action {

    var $maxX = 100;
    var $maxY = 100;
    var $fisso = 0;
    var $latofisso = "Y";


    public function init() {
        
    }

    public function fileAction() {        
        header("Cache-Control: private, max-age=10800, pre-check=10800");
        header("Pragma: private");
        header("Expires: " . date(DATE_RFC822,strtotime(" 2 day")));
        $file = Top::getModel("file")->loadByAttribute("hash", $this->getParam("v"));
        if($file){
            $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);

            $this->_helper->viewRenderer->setNoRender(true);
            $this->_helper->layout->disableLayout();

            $writabledir = Top::getAbsoluteUrl() . 'var/media/';
            $nameFile = $this->getParam("v") . "_X" . $this->getParam('r');


            if (file_exists($writabledir . $nameFile) && $config->website->cache->image)
            {
                header("Content-type: image/".$file->getFormat());
                readfile($writabledir . $nameFile);
            }else{
                if($this->getParam("r")){
                    $this->maxX = $this->getParam('r');
                    $this->maxY = $this->getParam('r');
                }

                $urlimage = Top::getAbsoluteUrl() . "media/". $file->getHash() . "." . $file->getFormat();

                if ($file->getFormat() == "jpeg" || $file->getFormat() == "jpg") {
                    $handle_immagine = imagecreatefromjpeg($urlimage);
                } elseif ($file->getFormat() == "gif") {
                    $handle_immagine = imagecreatefromgif($urlimage);
                } elseif ($file->getFormat() == "png") {
                    $handle_immagine = imagecreatefrompng($urlimage);
                } else {

                }

                header("Content-type: image/".$file->getFormat());
                header("Last-Modified: ".gmdate("D, d M Y H:i:s", time())." GMT");

                $handle_immagine_adattata = $this->cut($handle_immagine);


                if ($file->getFormat() == "jpeg" || $file->getFormat() == "jpg") {
                    imagejpeg($handle_immagine_adattata);
                    imagejpeg($handle_immagine_adattata, $writabledir . $nameFile);
                } elseif ($file->getFormat() == "gif") {
                    imagegif($handle_immagine_adattata);
                    imagegif($handle_immagine_adattata, $writabledir . $nameFile);
                } elseif ($file->getFormat() == "png") {
                    imagepng($handle_immagine_adattata);
                    imagepng($handle_immagine_adattata, $writabledir . $nameFile);
                } else {

                }
                imagedestroy($handle_immagine_adattata);
            }
        } else {
            $this->_forward("page404", "error", "default");
        }
    }


    function cut($handle_immagine) {
       $this->originalX = imagesx($handle_immagine);
       $this->originalY = imagesy($handle_immagine);
        if ($this->fisso == 1) {
            $this->newX = $this->maxX;
            $this->newY = $this->maxY;
        } else {
            if ($this->latofisso == "XY") {
                if ($this->originalX / $this->originalY > $this->maxX / $this->maxY) {
                    $this->newX = $this->maxX;
                    $this->newY = ($this->originalY / $this->originalX) * $this->maxX;
                } else {
                    $this->newX = ($this->originalX / $this->originalY) * $this->maxY;
                    $this->newY = $this->maxY;
                }
            } elseif ($this->latofisso == "X") {
                $this->newX = $this->maxX;
                $this->newY = ($this->originalY / $this->originalX) * $this->maxX;
            } elseif ($this->latofisso == "Y") {
                $this->newX = ($this->originalX / $this->originalY) * $this->maxY;
                $this->newY = $this->maxY;
            } else {
                if ($this->originalX / $this->originalY > $this->maxX / $this->maxY) {
                    $this->newX = $this->maxX;
                    $this->newY = ($this->originalY / $this->originalX) * $this->maxX;
                } else {
                    $this->newX = ($this->originalX / $this->originalY) * $this->maxY;
                    $this->newY = $this->maxY;
                }
            }
        }
        $tmp_immagine = imagecreatetruecolor($this->newX, $this->newY);
        $handle_immagine_adattata = imagecopyresampled($tmp_immagine, $handle_immagine, 0, 0, 0, 0, $this->newX, $this->newY, $this->originalX, $this->originalY);
        return $tmp_immagine;
    }
}
