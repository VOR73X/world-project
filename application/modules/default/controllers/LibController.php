<?php

class libController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function customAction() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        $uri_clear = str_replace($config->website->subdir, "", $this->getRequest()->getRequestUri());
        $explode = explode("/", $uri_clear);

        $baseUrl = "lib/custom/webitart/". $explode[count($explode)-1];

        $isCache = $config->website->cache->javascript;
        $isMinify = $config->website->basejs->minify;

        $cacheManager =  Zend_Registry::get('cacheGeneric');
        $cache = $cacheManager->getCache('generic');

        $cacheID = 'base_js';
        if (false === ($js = $cache->load($cacheID))) {
            $path_load = Top::getAbsoluteUrl() . $baseUrl;

            $js = file_get_contents($path_load);
            $regex = '/(\\$wbt_[\\w]+)/';
            preg_match_all($regex, $js, $matches);

            foreach ($matches[0] as $key => $value) {
                $namefile = str_replace('$wbt_', "", $value);
                $urlfile = Top::getAbsoluteUrl() . "/application/modules/default/jsengine/" . $namefile . ".js";
                $temp_js = file_get_contents($urlfile);
                if (file_exists($urlfile)) {
                    $temp_js = str_replace('var $wbt_' . $namefile . ' = ', "", $temp_js);
                    $js = str_replace($value, $temp_js, $js);
                }
            }

            if($isMinify){
                $js = JsMinify::minify($js);
            }

            if ($isCache) {
                $cache->save($js, $cacheID);
            }
        }

        $vars = "Wbt=JSON.parse('" . json_encode(Top::$jsEngine->object,JSON_FORCE_OBJECT) . "');";
        $js = $vars . $js;
        echo $js;
    }
}
