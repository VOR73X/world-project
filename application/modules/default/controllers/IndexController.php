<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $seo['title'] = "Titolo";
        $seo['description'] = "Descrizione Breve";
        $seo['url'] = Top::getBaseUrl();

        $this->view->seo = $seo;
    }

}
