var $wbt_submit = function () {
    // Variable to store your files
    $(document).ready(function () {

        xhr = function ()
        {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    $(".wbt-loading").css("width", percentComplete + "%");
                    Webitart.log("Submit ... " + percentComplete + "%");
                }
            }, false);
            return xhr;
        };

        $(".wbt-form").on('submit', function (e) {
            Webitart.log("Start Submit");

            e.preventDefault();
            e.returnValue = false;

            var callback = $(this).data("callback") || 'default';
            var params =  jQuery.extend([], $(this).data("params"));

            //** Add Auto UID 
            var id = Webitart.top.getGuid();
            var defineId;
            var myId = $(this).attr('id');
            var parent = $(this).parents(".wbt-group");
            var me = $(this).data("child");
        
            if (!$(parent).attr('id')){
                parent.attr('id', id);
            } else {
                id=parent.attr('id');
            } 
            //** AUto UID
            
            var action = $(this).attr("action") || parent.data("action");
            var method = $(this).attr("method") || "post";

            var formData = new FormData(this);

            Webitart.log("Prepare to Submit", formData);
            $.ajax({
                type: method,
                xhr: xhr,
                url: action + "?ajax=1",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                async: true,
                success: function (responde) {
                    responde = JSON.stringify(responde);
                    Webitart.log("Success to Submit");
                    var group = {
                        id:id
                    };

                    jQuery.each(params, function(i, el){
                        if(typeof el == 'string'){
                            params[i] = "'" + el + "'";
                        }
                    });
                    
                    params.unshift('group');
                    params.unshift('responde');

                    
                    var fn = "Webitart.callback." + callback +  "(" + params.concat() + ");";
                    Webitart.log("Call Callback: " + fn,responde);
                    Webitart.log("Guid: " + id);
                    eval(fn);
                }
            });
        });
    });

}