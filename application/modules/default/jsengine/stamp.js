var $wbt_stamp = function (options) {

    options = options || {};
    this.id = options.id || Webitart.top.getGuid();
    this.model = options.model || "";
    this.templatePath = options.templatePath || "";
    this.data = options.data || {};
    this.childs = options.childs || [];
    this.parentID = options.parentID || "";
    this.events = options.id || [];
    this.setFunctions = typeof options.setFunctions == 'function' ? options.setFunctions : function () {};
    this.restConf = options.restConf;
    this.autoRender = typeof options.autoRender != 'undefined' ? options.autoRender : true;

    this.addChild = function (options) {
        options = options || {};
        options.recursive = options.recursive || true;
        if (options.recursive) {
            for (var i in this) {
                if (i != "childs" && i != "parentID" && i != "id") {
                    if (!options[i]) {
                        var elemento = this[i];
                        options[i] = elemento;
                    }
                }
            }
        }
        options.parentID = this.id;

        this.tempObj = new Webitart.stamp(options);
        options.init = options.init || true;
        options.init && this.tempObj.init();
        this.childs.push(this.tempObj);
    };

    this.getTemplate = function (scope) {

        /*
         * TODO
         * Inserire wbt_cache
         * 
         */
        if (!Webitart.cache.isCached(scope.templatePath, 'session')) {

            $.get(scope.templatePath, function (tpl) {
                Webitart.cache.set(scope.templatePath.toString(), tpl, 'session');
                scope.morphTemplate(tpl);
            });
        } else {
            var tpl = Webitart.cache.get(scope.templatePath, 'session');
            scope.morphTemplate(tpl);
        }

    };

    this.morphTemplate = function (tpl) {

        this.data.stamp_id = this.id;

        for (var key in this.data) {
            var reg = new RegExp('\\[\\*' + key + '+\\*\\]', 'g');
            tpl = tpl.replace(reg, this.data[key]);
        }

        this.jObject = $(tpl);
        this.setFunctions(this);

        this.autoRender && this.render();

    };

    this.render = function (options) {

        $("#" + this.parentID).append(this.jObject);

        for (var i in this.childs) {
            child = this.childs[i];
            child.init();
        }

    };

    this.init = function () {
        if (this.restConf) {
            this.rest = new Webitart.rest(this.restConf);
        }
        this.templatePath && this.getTemplate(this);

    };

    this.toggle = function (duration, callback) {
        callback = callback || function () {};
        this.jObject.toggle(duration, callback);
    };

    this.kill = function (duration) {
        this.jObject.remove();
    };

    for (var i in options.childsOptions) {
        var option = options.childsOptions[i];
        this.addChild(option);
    }
}