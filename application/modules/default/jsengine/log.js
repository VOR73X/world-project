var $wbt_log = function (key, obj, type) {
    if (Wbt.env.log.status == true) {
        console.info(key);
        if (obj && Wbt.env.log.level == 1) {
            switch (type) {
                case 'warn':
                    console.warn(obj);
                    break
                default:
                    console.log(obj);
            }
            console.info("----");
        }
    }
}