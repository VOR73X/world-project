/*
 * @param {Object} options
 */

var $wbt_rest = function (options) {

    this.options = options || {};
    this.options.formData = this.options.formData || {};
    this.options.method = this.options.method || 'POST';
    this.options.cache = typeof this.options.cache != 'undefined' ? this.options.cache : false;
    this.options.async = typeof this.options.async != 'undefined' ? this.options.async : true;
    this.options.params = this.options.params || {};

    /*
     * @param [Array] params
     */
    this.composeFilter = function(params){
        var result = "";
        var index = 0;
        for(var i in params){
            var el = params[i];
            var key = i;
            var operand = el[0];
            var value = el[1];
            var condition = el[2] || "and";

            result = result + key + ":" + operand + ":" + value;
            if(index<Object.keys(params).length-1){
                result = result + ":"+condition+":";
            }
            index ++;
        }
        return result;
    }

    if (options.filter) {
        this.options.params.filter = this.composeFilter(options.filter);
    }

    /*
     * @param {Object} options
     */
    this.composeRestUrl = function (options) {
        var url = options.url;

        if (!url) {
            url = Wbt.env.getbaseurl + 'rest/';
            url += options.type + '/tab/' + options.tab;
            for (var key in options.params) {
                url += '/' + key + '/' + options.params[key];
            }
        }

        return encodeURI(url);

    };

    this.xhr = function (){
        var xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                var percentComplete = (evt.loaded / evt.total) * 100;
                $(".wbt-loading").css("width", percentComplete + "%");
                Webitart.log("xhr ... " + percentComplete + "%");
            }
        }, false);
        return xhr;
    };

    this.invoke = function (callback) {
        
        callback = callback || function(res){}

        this.options.url = this.options.url || this.composeRestUrl(this.options);
        var cacheKey = this.options.cacheName || this.options.method + '|' + this.options.url;
        var formData = new FormData();
        for(var i in this.options.formData){
            formData.append(i,this.options.formData[i])            
        }

        if(Webitart.cache.isCached(cacheKey)){
            callback(Webitart.cache.get(cacheKey));
        } else {
            $.ajax({
                type: this.options.method,
                url: this.options.url + "?ajax=1",
                data: formData,
                contentType: false,
                processData: false,
                async: this.options.async,
                success: function(res){
                    if(this.options.cache == true){
                        Webitart.cache.set(cacheKey, res);
                    }
                    callback(res);
                }.bind(this)
            });
        }


    };


}