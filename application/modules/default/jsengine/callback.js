var $wbt_callback = function (key, obj) {

    this.default = function (responde) {
        var obj = JSON.parse(responde);
        obj.status ?  location.reload(true) : Webitart.notify(obj.notify);
    }

    this.reload = function(res){
        location.reload(true);
    }
    
    this.simple = function(res){
        var obj = JSON.parse(res);
        Webitart.notify(obj.notify);
    }

    this.upload = function(res,reacts,thumb){
        var obj = JSON.parse(res);
        Webitart.notify(obj.notify);
        for(var i in reacts){
            $(reacts[i]).val(obj.id);
        }

        for(var i in thumb){
           var t =  $(thumb[i]);
           var size =  t.data("size") || 300;
           t.css("backgroundImage", "url(" + Wbt.env.getbaseurl + "media/file/r/" + size + "/v/" +  obj.hash +")");
        }
    }

    this.registration = function (res) {
        var obj = JSON.parse(res);
        if (obj.status == true) {
            location.href = "/";
        } else {
            Webitart.notify(obj.notify);
        }
    }

    this.rate = function (res) {
        var obj = JSON.parse(res);
        if (obj.status == true) {
            Webitart.notify(obj.notify);
        }
    }

    this.comment = function (res, a) {
        var obj = JSON.parse(res);

        if (obj.status == true) {
                     
            var data = {
                id: Wbt.user.id,
                username: Wbt.user.username,
                content: 'test contenuto'
            };

            Webitart.stamp('#stamp-tpl-comment', data, '#stamp-target-comment');

            Webitart.notify(obj.notify);
        }
    }

    this.like = function (res, group) {
            console.log(res)
            console.log(group)
        var obj = JSON.parse(res);
        if (obj.status) {
            
            
        }
    }

    this.dislike = function (res, group) {
         console.log(res)
            console.log(group)
        var obj = JSON.parse(res);
        if (obj.status) {
            
            updateCount($('#dislike-count_' + id), 1, '+');
            $('#rated-status_' + id).data("rated") && updateCount($('#like-count_'+ id), 1, '-');
            $('#rated-status_' + id).data("rated", true);
            
            $('#dislike_' + id).switchClass('fg-t-color-hover', 'fg-first');
            $('#like_' + id).switchClass('fg-ok-color', 'fg-t-color-hover');
        }
    }
   
}