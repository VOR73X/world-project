var $wbt_notify = function (object) {

    if(typeof Notification !== "undefined"){
        if (!Notification) {
            Webitart.log("Notify - No native notification support");
        }
    }
    
    for (var index in object) {
        var child = object[index];

        if (Notification.permission !== "granted"){
            Notification.requestPermission();
            Webitart.log("Notify - RequestPermission");
        } else {
            if (child.system) {
                var notification = new Notification(child.title, {
                    icon: child.icon,
                    body: child.message
                });

                Webitart.log("Notify system: " + child.message,child);

                notification.onclick = function () {
                    window.open(child.onclick);
                };
            } else {
                if (typeof child !== "undefined") {
                    Webitart.log("Notify standard: " + child.message,child);
                    var id = "wbt-alert-" + Webitart.top.getGuid();
                    $(".wbt-notify-wrapper").prepend("<div id='" + id + "' class='wbt-notify'>" + child.message + "</div>");
                    eval("setTimeout(function(){$(\"#" + id + "\").fadeOut(200,function(){$(\"#" + id +"\").remove();});},5000);");
                }
            }
        }
    }
}
