var $wbt_cache = function () {

    /*
     * @param {string} key
     * @param {string} from (session|null)
     * 
     * @return {bool}
     */
    this.isCached = function (key, from) {
        switch (from) {
            case 'session':
                return sessionStorage.getItem(key) ? true : false;
                break;
            default:
                return localStorage.getItem(key) ? true : false;
        }
    };

    /*
     * @param {string} key
     * @param {string} from (session|null)
     *
     * @return {*}
     */
    this.get = function (key, from) {
        switch (from) {
            case 'session':
                return JSON.parse(sessionStorage.getItem(key));
                break;
            default:
                return JSON.parse(localStorage.getItem(key));
        }

    };

    /*
     * @param {string} key
     * @param {string} from (session|null)
     * @param {*} obj
     * 
     */
    this.set = function (key, obj, from) {
        obj = JSON.stringify(obj);
        switch (from) {
            case 'session':
                sessionStorage.setItem(key, obj);
                break;
            default:
                localStorage.setItem(key, obj);
        }

    };

    /*
     * @param {string} key
     * @param {string} from (session|null)
     */
    this.remove = function (key, from) {
        switch (from) {
            case 'session':
                sessionStorage.removeItem(key, from);
                break;
            default:
                localStorage.removeItem(key, from);
        }
    };


}