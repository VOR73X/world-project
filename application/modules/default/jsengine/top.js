var $wbt_top = function (key, obj) {

    /*
    * Generazione di Guid Inicovoci
    * 
    * @PARAM prefix : Opzionale: Aggiunge un Prefisso antecedente alla GUID generata
    * 
    */

    this.getGuid = function(prefix){
        var temp="";
        var d = new Date();
        var token = Math.floor((Math.random() * 9999) + 1000);
        if (prefix)temp = prefix + "-";

        function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
        }
        gen = temp + s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        Webitart.log("Gen Guid: " + gen);
        return gen;
    }

    /*
    * Gestione dei Tab (Show/Hide) dipendenti da un Header Esterno
    * In funzione del @INDEX la funzione attiva lo stato SHOW per l'elemento @REACT
    * In funzione del paramentro  @HEADER verra impostato lo stato ACTIVE
    * 
    * @PARAM @header : Class nel Header su cui attivare la Classe @active
    * @PARAM @active : Class name per lo style Active
    * @PARAM @react : Elementi su cui applicare (show/hide)
    *
    * Header .wbt-tab-header @header @active @react {
    *    element .header: that
    *    element
    *    element
    * }
    *
    * DOM {
    *    .tab
    *    .tab
    *    .tab
    * }
    * 
    * @PARAM action : JQuery_Element
    * @PARAM action : Index element To Show
    * 
    */

    this.magicShow = function(that,index){
        var parent = $(that).parents(".wbt-tab-header");    
        var activeClass = parent.data("active") || "secondary";
        var header = parent.data("header");
        parent.find("."+header).removeClass(activeClass).eq(index).addClass(activeClass);
        $(parent.data('react')).hide(0).eq(index).show();
    }
    
}