var $wbt_popup = function (link, options, params) {
    options = options || {};
    params = params || {};

    this.onLoad = options.onLoad || function () {};
    this.onClose = options.onClose || null;

    this.width = options.width || 500;
    this.height = options.height || 500;
    this.x = (screen.width / 2) - (this.width / 2);
    this.y = (screen.height / 2) - (this.height / 2);

    this.style = "top=" + this.y + ", left=" + this.x + ", width=" + this.width + ", height=" + this.height + ", status=no, menubar=no, toolbar=no scrollbars=no";

    this.win = window.open(link, "", this.style);

    this.win.onload = function () {
        this.win.onParent = params;
        this.onLoad();

    }.bind(this);

    this.win.onbeforeunload = function () {
        if(this.onClose){
            this.onClose();
            return false;
        }
    }.bind(this);
}