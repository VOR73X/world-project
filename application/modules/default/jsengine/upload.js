var $wbt_upload = function (key, obj) {
    
    $(document).ready(function(){
    	$('.wbt-upload').fileupload({
	        add: function (e, data) {
	        	Webitart.log("Start Upload",data);
	            var jqXHR = data.submit();
	        },

	        progress: function(e, data){
	            var status = parseInt(data.loaded / data.total * 100, 10);
	            var parent = $(data.form);
	            var inwork = jQuery.extend([], parent.data("inwork"));
	            for(var i in inwork){
	            	$(inwork[i]).text(status + "%");
	            }
	        	Webitart.log("Upload... " + status + "%", data);
	        },

	        done: function(e,data){
	        	Webitart.log("Upload Complete",data);
	        	var parent = $(data.form);
				var callback = $(parent).data("callback") || 'upload';
				var object_react = jQuery.extend([], parent.data("responde"));
				var object_thumb = jQuery.extend([], parent.data("thumb"));

	            var inwork = jQuery.extend([], parent.data("inwork"));
	            for(var i in inwork){
	            	var element = $(inwork[i]);
	            	var label = element.data("inwork-complete") || "Complete";
	            	element.text(label);
	            }

				var responde = data.result;
                responde = JSON.stringify(responde);
				var fn = "Webitart.callback." + callback +  "(responde,object_react, object_thumb);";

                eval(fn);
	        },
	        fail:function(e, data){
	            Webitart.log("Upload Fail",data);
	        }

	    });

	    function formatFileSize(bytes) {
	        if (typeof bytes !== 'number') {
	            return '';
	        }

	        if (bytes >= 1000000000) {
	            return (bytes / 1000000000).toFixed(2) + ' GB';
	        }

	        if (bytes >= 1000000) {
	            return (bytes / 1000000).toFixed(2) + ' MB';
	        }

	        return (bytes / 1000).toFixed(2) + ' KB';
	    }


    })
    
}