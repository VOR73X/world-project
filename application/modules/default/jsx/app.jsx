Home = require('./pages/home/home.jsx');
Editor = require('./pages/editor/editor.jsx');

$(document).ready(function () {
	$('.react-home').each(function(i){
		var elemento = this;
		var params = $(this).data('params') || {};
		if(elemento){
			ReactDOM.render(
				<Home params={params}/>, elemento
			)
		}
	})

	$('.react-editor').each(function(i){
		var elemento = this;
		var params = $(this).data('params') || {};
		if(elemento){
			ReactDOM.render(
				<Editor params={params}/>, elemento
			)
		}
	})

})