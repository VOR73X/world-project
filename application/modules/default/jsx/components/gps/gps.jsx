Gps = React.createClass({
	gpsId: null,
	componentDidMount: function(){
		options = {
		  enableHighAccuracy: true,
		  timeout: 500,
		  maximumAge: 1000
		};
		this.gpsId = setInterval(function(){
			this.updateGPS();
		}.bind(this), 1000)
	},
	componentWillUnmount: function(){
		clearInterval(this.gpsId);
	},
	getInitialState: function(){
		return {
			latitude: 0,
			longitude: 0,	
			altitude: 0,
			accuracy: 0,
			altitudeAccuracy: 0,
			heading: 0,
			headingString: "",
			speed: 0,
			status: ""
		}
	},
	updateGPS: function(){
		this.setState({
			latitude: World.gps.latitude,
			longitude: World.gps.longitude,
			altitude: World.gps.altitude,
			accuracy: World.gps.accuracy,
			altitudeAccuracy: World.gps.altitudeAccuracy,
			heading: World.gps.heading,
			headingString: World.gps.headingString,
			speed: World.gps.speed,
			status: World.gps.status,
			timestamp: World.gps.timestamp
		});
	},
	render: function(){
		var gps = this.state;
		return <div>
			<div>latitude: {gps.latitude}</div>
			<div>longitude: {gps.longitude}</div>
			<div>altitude: {gps.altitude}</div>
			<div>accuracy: {gps.accuracy}</div>
			<div>altitudeAccuracy: {gps.altitudeAccuracy}</div>
			<div>heading: {gps.heading}</div>
			<div>headingString: {gps.headingString}</div>
			<div>speed: {gps.speed}</div>
			<div>status: {gps.status}</div>
			<div>timestamp: {gps.timestamp}</div>
		</div>
	}
})

module.exports = Gps;