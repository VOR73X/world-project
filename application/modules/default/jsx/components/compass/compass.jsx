
Compass = React.createClass({

	componentDidMount: function(){
		var self = this;
		if(window.DeviceOrientationEvent){
			window.addEventListener('deviceorientation', function(event) {
			  self.setState({
			  	alpha: event.alpha
			  })
			}, false);
		} else {
			console.log('Browser not support DeviceOrientationEvent')
		}
	},
	getInitialState: function(){
		return {
			alpha: 0
		}
	},
	render: function(){
		var compass = this.state;
		return <div>
			<b>Compass:</b> {getDirectionString(compass.alpha)} | {compass.alpha}
		</div>
	}
})

module.exports = Compass;

