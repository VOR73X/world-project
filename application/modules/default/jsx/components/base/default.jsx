Base = React.createClass({
	getDefaultProps: function(){
		console.log('INIT: getDefaultProps');
		return {}
	},
	getInitialState: function(){
		console.log('MOUNTING: getInitialState');
		return {
			show : false
		}
	},
	componentWillMount: function(){
		console.log('MOUNTING: componentWillMount');
	},
	render: function(){
		console.log('MOUNTING : render');
		return <div props={props}>
			BASE COMPONENT
		</div>
	},
	componentDidMount: function(){
		console.log('MOUNTING: componentDidMount');
	},
	componentWillReceiveProps: function(nextProps){
		console.log('UPDATE: componentWillReceiveProps');
	},
	shouldComponentUpdate: function(){
		console.log('UPDATE: shouldComponentUpdate');
		return true;
	},
	componentWillUpdate: function(nextProps,nextState){
		console.log('UPDATE: componentWillUpdate');
	},
	componentDidUpdate: function(prevProps,prevState){
		console.log('UPDATE: componentDidUpdate');
	},
	componentWillUnmount: function(){
		console.log('UNMOUNTING: componentWillUnmount');
	}
})

module.exports = Base;

