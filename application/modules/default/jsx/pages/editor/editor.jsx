Event = require('./event/event.jsx');
Script = require('./script/script.jsx');


Editor = React.createClass({
	getDefaultProps: function(){
		console.log('INIT: getDefaultProps');
		return {}
	},
	getInitialState: function(){
		console.log('MOUNTING: getInitialState');
		return {
			show : false
		}
	},
	componentWillMount: function(){
		console.log('MOUNTING: componentWillMount');
	},
	render: function(){
		console.log('MOUNTING : render');
		return <div className="row">
			<div className="column larg-16">
				BASE COMPONENT
			</div>
		</div>
	},
	componentDidMount: function(){
		console.log('MOUNTING: componentDidMount');
	},
	componentWillReceiveProps: function(nextProps){
		console.log('UPDATE: componentWillReceiveProps');
	},
	shouldComponentUpdate: function(){
		console.log('UPDATE: shouldComponentUpdate');
		return true;
	},
	componentWillUpdate: function(nextProps,nextState){
		console.log('UPDATE: componentWillUpdate');
	},
	componentDidUpdate: function(prevProps,prevState){
		console.log('UPDATE: componentDidUpdate');
	},
	componentWillUnmount: function(){
		console.log('UNMOUNTING: componentWillUnmount');
	}
})

module.exports = Editor;

