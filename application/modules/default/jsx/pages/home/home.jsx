Gps= require('./../../components/gps/gps.jsx');
Compass = require('./../../components/compass/compass.jsx');
Events = require("./events.jsx");

Home = React.createClass({
	getInitalState:function () {
		return{
			data: {}
		}
	},
	onCheckStatus: function(){
		var formData = new FormData();
		for(var a in World.gps){
			formData.append(a,World.gps[a]);
		}

		$.ajax({
		    type: "post",
		    url: "api/scan",
		    data: formData,
		    cache: false,
		    contentType: false,
		    processData: false,
		    async: true,
		    success: function (responde) {	        
		        console.log(responde);
		        this.setState(function () {
		        	return{
		        		data:responde
		        	}
		        });

		        if(responde.events){
		        	for(var i in responde.events){
		        		var element = responde.events[i];

		        		if(element.responde){
		        			if(element.responde.messages){
		        				for(var x in element.responde.messages){
		        					var message = element.responde.messages[x];
		        					alert(message);
		        				}
		        			}
		        		}

		        	}
	        		console.log(responde.env);
		        }
		    }.bind(this)
		});
	},
	render: function(){
		var tplEvents = <div />;
		var state = this.state || {};

			console.log(this.state);
		if(state.data){
			tplEvents = <Events data={state.data} />
		}
		return <div>
			<Compass />
			<Gps />
			{tplEvents}
			<button className="button" onClick={this.onCheckStatus}>Check</button>

		</div>
	}
})

module.exports = Home;
