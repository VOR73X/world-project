Events = React.createClass({
	componentWillMount: function(){

	},
	render: function(){
		var tplEvents = [];
		
		for(var x in this.props.data.events){
			var event = this.props.data.events[x];
			tplEvents.push(<div key={x} className="callout">
				{event.name} - {event.directionString} - {event.distance}
			</div>);
		}

		return <div>
			{tplEvents}
		</div>
	}
})

module.exports = Events;