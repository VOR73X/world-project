{strip}
    <!DOCTYPE html>
    <html>
        <head>
            {include file="head.tpl"}
        </head>

        <body>
            {$this->layout()->content}
            <footer>
                {include file="footer.tpl"}
            </footer>
            
        </body>
    </html>
{/strip}
