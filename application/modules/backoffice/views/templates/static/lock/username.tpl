{strip}
    <div class="row padding-vertical-large">
        <div class="columns large-5 medium-5 small-12 large-centered medium-centered">
            <div class="title-wrapper">
                <h1>{lang title='Nuovo Username'}</h1>
                <h2>{lang subtitle='Per poter accedere a Tubooster, imposta il tuo nuovo Username'}</h2>
                <div class="line"></div>
            </div>
            <form class="wbt-form" data-abide method="post" action="{getbaseurl}options/update">
                <div class="row">
                    <div class="columns">
                        <label>{lang email='Username' g=true}
                            <input type="text" required="" name="username" required="">
                        </label>
                    </div>

                    <div class="column large-12">
                        <input type="submit" name="profile_username" class="button" value="{lang imposta='Imposta'}"> 
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        {literal}   
        
        {/literal}
    </script>
{/strip}