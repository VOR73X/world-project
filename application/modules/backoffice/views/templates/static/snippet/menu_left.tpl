{strip}
    <div class="">
        
        {if !{acl r='menu' p='l1'}}
            <div class="row">
                <div class="column">
                    <ul>
                        <li><a href="{getbaseurl}login"><i class="fi fi-torso"></i>&nbsp;{lang header='Login'}</a></li>
                        <li><a href="{getbaseurl}registration"><i class="fi fi-pencil"></i>&nbsp;{lang header='Registrati'}</a></li>
                    </ul>
                </div>
            </div>
        {else}
            {if {acl r='menu' p='l1'}}
                <div class="row">
                    <div class="column">
                        <ul>
                            <li><a href="{getbaseurl}"><i class="fi fi-home"></i> Home</a></li>
                            <li><a href="{getbaseurl}profile"><i class="fi fi-torso"></i> Profilo</a></li>
                            <li><a href="{getbaseurl}options/notifications"><i class="fi fi-web"></i> Notifiche</a></li>
                            <li><a href="{getbaseurl}options/history"><i class="fi fi-graph-pie"></i> Cronologia</a></li>
                            <li><a href="{getbaseurl}options/profile"><i class="fi fi-wrench"></i> Impostazioni</a></li>
                            <li><a href="{getbaseurl}login/close"><i class="fi fi-torso"></i>&nbsp;{lang logout='Logout'}</a></li>
                        </ul>
                    </div>
                </div>
            {/if}
        {/if}

        <div class="row">
            <div class="column">
                <b>Categorie</b>
                <ul>
                    {foreach from=$menu_category item=data}
                        <li><span><a href="{getbaseurl}category/watch/{$data.name}">{$data.label}</a></span></li>
                    {/foreach}
                </ul>

            </div>
        </div>
        <div class="row">
            <div class="column">
                <b>Altro</b>
                <ul>
                    <li><a href="{getbaseurl}artworks/">Artworks</a></li>
                </ul>
                <ul>
                    <li><a href="{getbaseurl}answers/">Answers</a></li>
                </ul>
            </div>
        </div>
    
    </div>

{/strip}