{strip}
    <title>{$seo.title} - {config var="website->name"}</title>

    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <!-- SEO -->
    <meta name="keywords" content="{$seo.name}"/>
    <meta name="description" content="{$seo.description}"/>
    <link rel="canonical" href="{$seo.url}"/>
    <meta property="og:locale" content="{$seo.lang}"/>
    <meta property="og:title" content="{$seo.title}"/>
    <meta property="og:description" content="{$seo.description}"/>
    <meta property="og:url" content="{$seo.url}"/>
    <meta property="og:site_name" content="{config var="website->name"}"/>
    <meta property="og:image" content="{$seo.image}"/>
    <meta property="og:image:width" content="{$seo.width}">
    <meta property="og:image:height" content="{$seo.height}">
    <!-- SEO -->
    <!-- IOS ICON-->
    <link rel="apple-touch-icon" href="{$seo.apple_icon}"/>
    <!-- IOS ICON-->
    <link rel="shortcut icon" href="{getBaseUrl}style/img/favicon.png" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="{getBaseThemeUrl module='backoffice'}css/foundation.min.css"/>
    <link rel="stylesheet" type="text/css" href="{getBaseUrl}lib/dependencies/foundation-icon-fonts/foundation-icons.css"/>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/motion-ui/dist/motion-ui.min.css"></script>

    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/blueimp-file-upload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/foundation-sites/dist/foundation.min.js"></script>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/motion-ui/dist/motion-ui.min.js"></script>
    <script type="text/javascript" src="{getBaseThemeUrl}webitart/base.js"></script>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/react/react.js"></script>
    <script type="text/javascript" src="{getBaseUrl}lib/dependencies/react/react-dom.min.js"></script>
    <script src="https://npmcdn.com/react-router/umd/ReactRouter.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{getBaseThemeUrl module='backoffice'}css/style.min.css"/>
    <script type="text/javascript" src="{getBaseThemeUrl module='backoffice'}react/react-app.js"></script>    

    <style>
        *:focus{
            outline: 0;
        }
    </style>

{/strip}