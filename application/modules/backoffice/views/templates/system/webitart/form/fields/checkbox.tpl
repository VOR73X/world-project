{strip}
	{if $debug==1}<pre>{$bean|print_r}</pre>{/if}
	<legend>{$bean.label}</legend>
			{foreach from=$bean.option key=key item=data}
			<label style="display: inline;">
				<input type="checkbox" {field id=$bean}{field class=$bean}{field name=$bean value="{$bean.name}[]"}{field regex=$bean wbtattr="pattern"}{field min=$bean}{field max=$bean}{field required=$bean}{field readonly=$bean}{field style=$bean}{field maxlength=$bean}{field placeholder=$bean}{field step=$bean}{field accept=$bean}{field checked=$bean}{field disabled=$bean} value="{$key}">{$data}
			</label>
			{/foreach}
		{include file='./js_preview.tpl'}
	</label>
{/strip}