{strip}
	{if $debug==1}<pre>{$bean|print_r}</pre>{/if}
	<label>{$bean.label}
		<select type="text" {$bean.datafield} {field id=$bean}{field class=$bean}{field name=$bean}{field value=$bean}{field regex=$bean wbtattr="pattern"}{field min=$bean}{field max=$bean}{field required=$bean}{field readonly=$bean}{field style=$bean}{field maxlength=$bean}{field placeholder=$bean}{field step=$bean}{field accept=$bean}{field checked=$bean}{field disabled=$bean}>
			{foreach from=$bean.option key=key item=data}
				<option value="{$key}">{$data}</option>
			{/foreach}
		</select>
		{include file='./js_preview.tpl'}
	</label>
{/strip}