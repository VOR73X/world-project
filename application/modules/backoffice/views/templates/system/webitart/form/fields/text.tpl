{strip}
	<label>{$bean.label}
		<input type="text" {$bean.datafield} {field id=$bean}{field class=$bean}{field name=$bean}{field value=$bean}{field regex=$bean wbtattr="pattern"}{field min=$bean}{field max=$bean}{field required=$bean}{field readonly=$bean}{field style=$bean}{field maxlength=$bean}{field placeholder=$bean}{field step=$bean}{field accept=$bean}{field checked=$bean}{field disabled=$bean}>
		{if $bean.help_message}
			<p class="help-text" id="{$bean.id}">{$bean.help_message}</p>
		{/if}
		{if $bean.help_error}
			<span class="form-error">{$bean.help_error}</span>
		{/if}
		{include file='./js_preview.tpl'}
	</label>
{/strip}