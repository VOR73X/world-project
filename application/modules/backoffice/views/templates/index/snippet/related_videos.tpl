{strip}
    {assign var="index_absolute" value=$index}
    <div class="row">
        <div class="columns large-12">    
            <div class="row margin-vertical-medium">
                <div class="column large-12 margin-bottom-small">
                    <div class="border square-large inline middle"></div>
                    <span class="inline middle font-small-bold">&nbsp; 
                        {if $index==0 && $index_absolute==0}
                            Sponsorizzati
                        {elseif $index==1 && $index_absolute==1}
                            Ultimi visti
                        {elseif $index==2 && $index_absolute==2}
                            Più visti
                        {elseif $index==3 && $index_absolute==3}
                            Caricati di Recente
                        {elseif $index>3 || $index_absolute>3}
                            {$data[0].user.username}
                        {/if}
                    </span>
                </div>
                <div class="column">
                    <div class="row {if $index==0}small-up-1 medium-up-3 large-up-3{else}small-up-1 medium-up-3 large-up-6{/if}">
                        {foreach from=$data item=video}
                            {include file="../../commons/flex_video.tpl"}
                        {/foreach}
                    </div>
                </div>
                <div class="column end"></div>
            </div>
            {if $index>0}
                <div class="line"></div>
            {/if}
        </div>
    </div>  
{/strip}