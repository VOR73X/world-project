<?php

class Backoffice_Bootstrap extends Zend_Application_Module_Bootstrap {
    
    protected function _initCache(){
        if (!file_exists(Top::getAbsoluteUrl() . '/var')) {
            mkdir(Top::getAbsoluteUrl() . '/var', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/cache')) {
            mkdir(Top::getAbsoluteUrl() . '/var/cache', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/db')) {
            mkdir(Top::getAbsoluteUrl() . '/var/db', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/log')) {
            mkdir(Top::getAbsoluteUrl() . '/var/log', 0777, true);
        }
        if (!file_exists(Top::getAbsoluteUrl() . '/var/media')) {
            mkdir(Top::getAbsoluteUrl() . '/var/media', 0777, true);
        }

        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        if (!file_exists(Top::getAbsoluteUrl() . '/media')) {
            mkdir(Top::getAbsoluteUrl() . '/media', 0777, true);
        }
        $frontendOptions = array(
           'lifetime' => 86400,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => Top::getAbsoluteUrl() . 'var/cache'
        );
         
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions,$backendOptions);
         
        $manager = new Zend_Cache_Manager;
        $manager->setCache('generic', $cache);
         
        Zend_Registry::set('cacheGeneric', $manager);
    }
    
    protected  function _initCacheDB(){
        $frontendOptions = array(
           'lifetime' => 86400,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => Top::getAbsoluteUrl() . 'var/db'
        );
         
        $dbCache = Zend_Cache::factory('Core', 'File', $frontendOptions,$backendOptions);
         
        $manager = new Zend_Cache_Manager;
        $manager->setCache('database', $dbCache);
         
        Zend_Registry::set('cacheDatabase', $manager);
    }
}
    