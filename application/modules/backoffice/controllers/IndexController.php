<?php

class Backoffice_IndexController extends Zend_Controller_Action {

    public function init() {

    }

    public function indexAction() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);

        $seo['title'] = "Titolo";
        $seo['description'] = "Descrizione Breve";
        $seo['url'] = Top::getBaseUrl();

        $this->view->seo = $seo;
    }

    public function homeAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $page = new Webitart_Page();
        $page->setTitle("Title");
        $page->setDescription("Descrizione");
        $page->addRow([
        	[
        		'large'=>6,
        		'list'=>[
        			'link1'=>'link1',
        			'link2'=>'link2',
        			'link3'=>'link3'
        		]
    		],
        	[
        		'large'=>6,
        		'list'=>[
        			'link1'=>'link1',
        			'link2'=>'link2',
        			'link3'=>'link3'
        		]
    		],
    	]);

	    $page->addRow([
	    	[
	    		'large'=>4,
	    		'list'=>[
	    			'link1'=>'link1',
	    			'link2'=>'link2',
	    			'link3'=>'link3'
	    		]
			],
	    	[
	    		'large'=>4,
	    		'list'=>[
	    			'link1'=>'link1',
	    			'link2'=>'link2',
	    			'link3'=>'link3'
	    		]
			],
	    	[
	    		'large'=>4,
	    		'list'=>[
	    			'link1'=>'link1',
	    			'link2'=>'link2',
	    			'link3'=>'link3'
	    		]
			],
		]);

        $page->json();
    }

}
