Page = require('./../components/page/page.jsx');

var Index = React.createClass({
	componentDidMount: function(){
		$(document).foundation();
	},
	render: function(){	
		return <div className="row">
			<Page path='index/home/' />
		</div>
	}
})

module.exports = Index;