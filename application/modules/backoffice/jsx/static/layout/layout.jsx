Menu = require('./../../components/Menu/Menu.jsx');
MenuConfig = [
  {
    label: 'Home',
    to: '/'
  }
]

var Layout = React.createClass({
  getInitialState: function(){
    return {  
      show : false
    }
  },
  hideMenu:function(event){
    this.setState(function(){
      return {
        show: false
      }
    })
  },
  toggleMenu: function(event){
    this.setState(function(){
      return {
        show: !this.state.show
      }
    })
  },
  render: function() {
    var tpl;

    if(this.state.show){
      tpl = <Menu hideMenu={this.hideMenu} config={MenuConfig}/>
    }else{
      tpl = this.props.children;
    }

    return (
      <div>
        <div className="title-bar">
            <div className="title-bar-left">
            <button className="menu-icon" type="button" onClick={this.toggleMenu}></button>
            <span className="title-bar-title">Backoffice</span>
            </div>
        </div>
        <section>
          {tpl}
        </section>
      </div>
    );
  }
});

module.exports = Layout;