Link = require('react-router').Link

Page = React.createClass({
	getDefaultProps: function(){
		return {}
	},
	getInitialState: function(){
		return {
			data : {}
		}
	},
	componentWillMount: function(){
	},
	render: function(){
		var tplPage = <div />;
		var tplRows = [];
		if (this.state.data.rows){
			for(var i in this.state.data.rows){
				var tplColums = [];
				var row = this.state.data.rows[i];

				for(var x in row){
					var column = row[x];
					var className = "column large-" + column.large;
					var list = column.list;
					var tplList = [];

					for(var z in list){
						var el = list[z];
						tplList.push(<div key={z}>
							<Link to={el}>{z}</Link>
						</div>)
					}

					tplColums.push(<div key={x} className={className}>
						{tplList}
					</div>);
				}
				tplRows.push(<div key={i}>
					<div className="row">
						{tplColums}
					</div>
					<hr />
				</div>);
			}
		}
		if (this.state.data.title){
			tplPage = <div className='row'>
				<div className="column">
					<h3>{this.state.data.title}</h3>
					<h5 className="subheader">{this.state.data.description}</h5>
					{tplRows}
				</div>
			</div>
		}
		return <div>
			{tplPage}
		</div>
	},
	componentDidMount: function(){
		var open = new Webitart.rest({
			url: Wbt.env.getbaseurl + "admin/" + this.props.path,
			cache: false,
		})

		open.invoke(function(res){
			console.log(res);
			this.setState(function(){
				return{
					data: res
				}
			})
		}.bind(this));
	},
	componentWillReceiveProps: function(nextProps){
	},
	shouldComponentUpdate: function(){
		return true;
	},
	componentWillUpdate: function(nextProps,nextState){
	},
	componentDidUpdate: function(prevProps,prevState){
	},
	componentWillUnmount: function(){
	}
})

module.exports = Page;

