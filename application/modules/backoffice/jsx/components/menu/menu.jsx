Link = require('react-router').Link

Menu = React.createClass({
	hideMenu:function(event){
		this.props.hideMenu();
	},
	getInitialState: function(){
		return {
			show : false
		}
	},
	toggleElement: function(){
		this.setState({
			show: !this.state.show
		});
	},
	render: function(){
		var body = [];
		for(var i in this.props.config){
			var el = this.props.config[i];
			body.push(<li  key={i}><Link onClick={this.hideMenu} to={el.to}>{el.label}</Link></li>)

		}
		if(this.state.show){
			tpl = <div>
				<button className="button" onClick={this.toggleElement}>Close</button>
			</div>;		
		} else {
			tpl = <div>
				<button className="button" onClick={this.toggleElement}>Open</button>
			</div>;
		}

		return <ul className="vertical menu">
			{body}
		</ul>
	}
})

module.exports = Menu;