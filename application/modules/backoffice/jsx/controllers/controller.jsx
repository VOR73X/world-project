Router = require('react-router').Router
Route = require('react-router').Route
Link = require('react-router').Link
hashHistory  = require('react-router').hashHistory 
IndexRoute  = require('react-router').IndexRoute 

Layout = require('./../static/layout/layout.jsx');
Index = require('./../static/Index.jsx');

Controller = React.createClass({
	render: function(){
		return <Router history={hashHistory}>
			<Route path="/">
				<Route component={Layout}>
					<IndexRoute component={Index} />
				</Route>	
			</Route>
		</Router>
	}
})

module.exports = Controller;