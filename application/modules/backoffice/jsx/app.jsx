Controller = require('./controllers/controller.jsx');

$(document).ready(function(){
	ReactDOM.render(
		<Controller/>,
		document.getElementById('app')
	)
});