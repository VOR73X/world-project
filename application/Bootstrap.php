<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    public static $smarty;

    public function _initRoutes() {
        $this->bootstrap(['db','FrontController']);   
        $this->_frontController = $this->getResource('FrontController');
        $router = $this->_frontController->getRouter();

        $defaultRoute = new Zend_Controller_Router_Route(':controller/:action/*',
            [
                'module' => 'default',
                'controller' => 'index',
                'action' => 'index'
            ]
        );
        $adminRoute = new Zend_Controller_Router_Route('admin/:controller/:action/*', 
            [
                'module' => 'backoffice',
                'controller' => 'index',
                'action' => 'index'
            ]
        );

        $router->addRoute('defaultRoute', $defaultRoute);
        $router->addRoute('adminRoute', $adminRoute);
    }
   
    protected function _initView() {
        Top::$smarty = new Ext_View_Smarty($this->getOption('smarty'));

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setViewSuffix('tpl');
        $viewRenderer->setView(Top::$smarty);
        $this->bootstrap('layout');

        $layout = Zend_Layout::getMvcInstance();
        $layout->setViewSuffix('tpl');
        

        return Top::$smarty;
    }


}
